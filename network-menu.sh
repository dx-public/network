#!/bin/env bash

#get current dir
cdir=`dirname $0`

#check status
[ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
if [ $changed = 1 ]; then
 #update git-repo first
 #git reset -- hard # for resetting
 echo "Updating script.."
 git -C $cdir pull
 echo "Please rerun network-menu.sh"
 exit
fi

ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

# Running a forever loop using while statement
# This loop will run untill select the exit option.
# User will be asked to select option again and again
while :
do

# creating a menu with the following options
echo "--------------"
echo "Network Menu"
echo "--------------"
echo "01.  Check Connectivity"
echo "02.  Configure Anycast"
echo "03.  Configure IPTables"
echo "04.  Cloudflare VPN"
echo "05.  ZeroTier"
echo "06.  Blocky"
echo "07.  DNSCrypt-Proxy"
echo "08.  Wireguard-Go"
echo "09.  Caddy"
echo "10.  NTP"
echo "11.  ARP"
echo "90.  Supervisor"
echo "91.  Tasker (cron)"
echo "95.  Update Hosts File"
echo -n "98.  Reboot"; [ -e /var/run/reboot-required ] && echo " (NEEDED) " || echo ""
echo "99.  Exit menu "
echo -n "Enter your menu choice [#]: "

# reading choice
read choice

# case statement is used to compare one value with the multiple cases.
case $choice in
  # Pattern 1
  1|01)  echo "You have selected the option 1"
      echo "Checking Connectivity... "
      sh $cdir/network_check.sh;;
  # Pattern 2
  2|02)  echo "You have selected the option 2"
      echo "Running script... "
      sh $cdir/network_anycast.sh;;
  # Pattern 3
  3|03)  echo "You have selected the option 3"
      echo "Running script... "
      sh $cdir/network_iptables.sh;;
  # Pattern 4
  4|04)  echo "You have selected the option 4"
      echo "Running script... "
      sh $cdir/network_cloudflared.sh;;
  # Pattern 5
  5|05)  echo "You have selected the option 5"
      echo "Running script... "
      sh $cdir/network_zerotier.sh;;
  # Pattern 6
  6|06)  echo "You have selected the option 6"
      echo "Running script... "
      sh $cdir/network_blocky.sh;;
  # Pattern 7
  7|07)  echo "You have selected the option 7"
      echo "Running script... "
      sh $cdir/network_dnscrypt.sh;;
  # Pattern 8
  8|08)  echo "You have selected the option 8"
      echo "Running script... "
      sh $cdir/network_wireguard.sh;;
  # Pattern 9
  9|09)  echo "You have selected the option 9"
      echo "Running script... "
      sh $cdir/network_caddy.sh;;
  # Pattern 10
  10)  echo "You have selected the option 10"
      echo "Running script... "
      sh $cdir/network_ntp.sh;;
  11) echo "You have selected option 11"
      echo "Running script... "
      sh $cdir/network_arp.sh;;
  90)  echo "You have selected the option 90"
      echo "Running script... "
      sh $cdir/network_supervisor.sh;;
  91)  echo "You have selected the option 91"
      echo "Running script... "
      sh $cdir/network_tasker.sh;;
  # Pattern 95
  95)  echo "You have selected the option 95"
      echo "Running script... "
      sh $cdir/network_hosts.sh;;
  # Pattern 98
  98|r)  echo "Rebooting ..."
       sudo reboot;;
  # Pattern 99
  99|q|'')  echo "Quitting ..."
       exit;;
  # Default Pattern
  *) echo "" && echo "invalid option" && echo "";;

esac
  #echo -n "Enter your menu choice [#]: "
done
