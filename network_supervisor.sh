#!/bin/env sh

echo "Installing Supervisor"

if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor/; chmod +w /etc/supervisor; fi

if [ ! -f /etc/supervisor/supervisord ]; then 
 if [ -f /etc/supervisord/supervisord ]; then
  echo "updating currently installed binary to default folder"
  ln -s /etc/supervisord/supervisord /etc/supervisor/supervisord
 else
  echo "downloading binary to directory"
  _BINARY=`curl -s https://api.github.com/repos/ochinchina/supervisord/releases/latest |  awk '/download_url/ && /Linux_64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' || echo 'https://github.com/ochinchina/supervisord/releases/download/v0.7.3/supervisord_0.7.3_Linux_64-bit.tar.gz'`
  curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t\"]+|[ \t\"]+$/,"");print}') > /tmp/supervisor.tar.gz
  CWD=$PWD
  cd /tmp
  tar xvf /tmp/supervisor.tar.gz
  cd supervisord*
  mkdir -p /etc/supervisord
  mv supervisord /etc/supervisord/.
  ln -s /etc/supervisord/supervisord /etc/supervisor/supervisord
  rm /tmp/supervisor.tar.gz
  cd $CWD
 fi
fi

#if [ -d /etc/supervisord ]; then rmdir /etc/supervisord; fi

cat << \EOF > "/etc/supervisor/supervisord.conf.sample"
;[inet_http_server]
;port=127.0.0.1:9001
;username=test1
;password=thepassword
;[supervisorctl]
;serverurl=http://127.0.0.1:9001
;[program:test]
;depends_on = B, C
;environment=VAR1="value1",VAR2="value2"
;command = /your/program args
EOF

if [ ! -f /etc/systemd/system/supervisord.service ]; then
cat << 'EOF' > /etc/systemd/system/supervisord.service
[Unit]
Description=Supervisord Go
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/bin/bash /etc/supervisor/supervisord.systemd

[Install]
WantedBy=multi-user.target
EOF
fi
echo "enable: systemctl enable supervisord"

echo "creating default arg template for supervisor.conf"
cat /dev/null > /etc/supervisor/program-default.args
echo "envFiles=.env" >> /etc/supervisor/program-default.args
awk '{gsub(".*/|.args", "", FILENAME); print "[" FILENAME "]\n" $0}' /etc/supervisor/*.args > /etc/supervisor/supervisord.conf

cat << 'EOF' > /etc/supervisor/supervisord.systemd
#!/bin/bash

#stopping/disabling services via systemd
for service in $(/bin/awk '{gsub(".*/|.args|program:|program-default", "", FILENAME); print "" FILENAME ""}' /etc/supervisor/*.args)
do
    /bin/echo "stopping $service"
    /bin/systemctl stop $service
    /bin/echo "disabling $service"
    /bin/systemctl disable $service
done

#creating dynamic conf file for supervisor
/bin/awk '{gsub(".*/|.args", "", FILENAME); print "[" FILENAME "]\n" $0}' /etc/supervisor/*.args > /etc/supervisor/supervisord.conf
/etc/supervisor/supervisord
EOF

chmod +x /etc/supervisor/supervisord.systemd