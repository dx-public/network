#!/bin/env bash

echo 'setting up dnscrypt-proxy'

if [ ! -f /etc/dnscrypt-proxy/dnscrypt-proxy ]; then 
_BINARY=`curl -s https://api.github.com/repos/DNSCrypt/dnscrypt-proxy/releases/latest | awk '/download_url/ && /linux_x86_64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' || echo 'https://github.com/DNSCrypt/dnscrypt-proxy/releases/download/2.1.4/dnscrypt-proxy-linux_x86_64-2.1.4.tar.gz'`

curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/dnscrypt-proxy.tar.gz
mkdir -p /etc/dnscrypt-proxy
cd /tmp
tar xvf /tmp/dnscrypt-proxy.tar.gz
cd linux*
cp dnscrypt-proxy /etc/dnscrypt-proxy/.
if [ ! -f /etc/dnscrypt-proxy/localhost.pem ]; then cp localhost.pem /etc/dnscrypt-proxy/localhost.pem; fi
rm -rf /tmp/dnscrypt-proxy*
rm -rf /tmp/linux*
fi

if [ ! -d '/var/log/dnscrypt-proxy' ]; then
mkdir -p /var/log/dnscrypt-proxy
touch /var/log/dnscrypt-proxy/dnscrypt-proxy.log
chmod +w /var/log/dnscrypt-proxy
fi

if [ ! -f '/etc/dnscrypt-proxy/cloaking-rules.txt' ]; then touch /etc/dnscrypt-proxy/cloaking-rules.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/query.log' ]; then touch /etc/dnscrypt-proxy/query.log; fi
if [ ! -f '/etc/dnscrypt-proxy/nx.log' ]; then touch /etc/dnscrypt-proxy/nx.log; fi
if [ ! -f '/etc/dnscrypt-proxy/blocked-names.txt' ]; then touch /etc/dnscrypt-proxy/blocked-names.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/blocked-names.log' ]; then touch /etc/dnscrypt-proxy/blocked-names.log; fi
if [ ! -f '/etc/dnscrypt-proxy/blocked-ips.txt' ]; then touch /etc/dnscrypt-proxy/blocked-ips.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/blocked-ips.log' ]; then touch /etc/dnscrypt-proxy/blocked-ips.log; fi
if [ ! -f '/etc/dnscrypt-proxy/blocked-names.txt' ]; then touch /etc/dnscrypt-proxy/blocked-names.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/allowed-names.txt' ]; then touch /etc/dnscrypt-proxy/allowed-names.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/allowed-names.log' ]; then touch /etc/dnscrypt-proxy/allowed-names.log; fi
if [ ! -f '/etc/dnscrypt-proxy/dnscrypt-proxy.toml' ]; then touch /etc/dnscrypt-proxy/dnscrypt-proxy.toml; fi
if [ ! -f '/etc/dnscrypt-proxy/whitelist.txt' ]; then touch /etc/dnscrypt-proxy/whitelist.txt; fi
if [ ! -f '/etc/dnscrypt-proxy/statichosts.txt' ]; then touch /etc/dnscrypt-proxy/statichosts.txt; fi

#if [ ! -f '/etc/dnscrypt-proxy/bootstrapconfig.toml' ]; then
cat > /etc/dnscrypt-proxy/bootstrapconfig.toml <<_EOF_
#server_names = ['cf-iv4-malware-1','cf-iv4-malware-2','cf-iv6-malware-1','cf-iv6-malware-2']
#server_names = ['custom-1-ip4-1','custom-2-ip4-2','custom-3-ip6-1','custom-4-ip6-2']
server_names = ['custom-1-ip4-1','custom-2-ip4-2']
listen_addresses = ['0.0.0.0:53']
ipv4_servers = true
ipv6_servers = true
#dnscrypt_servers = true
#doh_servers = true
#odoh_servers = true
#require_dnssec = false
#require_nolog = false
#require_nofilter = false
disabled_server_names = []
force_tcp = false
http3 = true
timeout = 5000
keepalive = 30
# edns_client_subnet = ['0.0.0.0/0', '2001:db8::/32']
# blocked_query_response = 'refused'
blocked_query_response = 'a:0.0.0.0,aaaa:[::]'
#lb_strategy = 'p2'
lb_strategy = 'first'
log_level = 2
log_file = '/var/log/dnscrypt-proxy/dnscrypt-proxy.log'
cert_refresh_delay = 240
bootstrap_resolvers = ['208.67.222.222:443', '[2620:119:35::35]:443', '208.67.220.220:5353', '[2620:119:53::53]:5353', '9.9.9.9:9953']
ignore_system_dns = true
# netprobe_timeout = 60
# netprobe_address = '127.0.0.1:53'
log_files_max_size = 10
log_files_max_age = 3
log_files_max_backups = 1
cache = true
cache_size = 4096
cache_min_ttl = 2400
cache_max_ttl = 86400
cache_neg_min_ttl = 60
cache_neg_max_ttl = 600
cloaking_rules = '/etc/dnscrypt-proxy/cloaking-rules.txt'
cloak_ttl = 600
cloak_ptr = false
[local_doh]
listen_addresses = ['0.0.0.0:2053']
cert_file = "/etc/dnscrypt-proxy/localhost.pem"
cert_key_file = "/etc/dnscrypt-proxy/localhost.pem"
[query_log]
file = '/etc/dnscrypt-proxy/query.log'
format = 'tsv'
#format = 'ltsv'
#ignored_qtypes = ['DNSKEY', 'NS']
[nx_log]
file = '/etc/dnscrypt-proxy/nx.log'
format = 'tsv'
#format = 'ltsv'
[blocked_names]
blocked_names_file = '/etc/dnscrypt-proxy/blocked-names.txt'
log_format = 'tsv'
#log_format = 'ltsv'
log_file = '/etc/dnscrypt-proxy/blocked-names.log'
[blocked_ips]
blocked_ips_file = '/etc/dnscrypt-proxy/blocked-ips.txt'
log_file = '/etc/dnscrypt-proxy/blocked-ips.log'
log_format = 'tsv'
#log_format = 'ltsv'
[allowed_names]
allowed_names_file = '/etc/dnscrypt-proxy/allowed-names.txt'
log_file = '/etc/dnscrypt-proxy/allowed-names.log'
log_format = 'tsv'
#log_format = 'ltsv'
[static]
[static.'custom-1-ip4-1']
stamp='sdns://AgMAAAAAAAAABzEuMS4xLjIABzEuMS4xLjIKL2Rucy1xdWVyeQ'
[static.'custom-2-ip4-2']
stamp='sdns://AgMAAAAAAAAABzEuMC4wLjIABzEuMC4wLjIKL2Rucy1xdWVyeQ'
[static.'custom-3-ip6-1']
stamp='sdns://AgMAAAAAAAAAFlsyNjA2OjQ3MDA6NDcwMDo6MTExMl0AFDI2MDY6NDcwMDo0NzAwOjoxMTEyCi9kbnMtcXVlcnk'
[static.'custom-4-ip6-2']
stamp='sdns://AgMAAAAAAAAAFlsyNjA2OjQ3MDA6NDcwMDo6MTAwMl0AFDI2MDY6NDcwMDo0NzAwOjoxMDAyCi9kbnMtcXVlcnk'
[static.'cf-iv4-malware-1']
stamp='sdns://AgMAAAAAAAAABzEuMS4xLjIABzEuMS4xLjIKL2Rucy1xdWVyeQ'
[static.'cf-iv4-malware-2']
stamp='sdns://AgMAAAAAAAAABzEuMC4wLjIABzEuMC4wLjIKL2Rucy1xdWVyeQ'
[static.'cf-iv6-malware-1']
stamp='sdns://AgMAAAAAAAAAFlsyNjA2OjQ3MDA6NDcwMDo6MTExMl0AFDI2MDY6NDcwMDo0NzAwOjoxMTEyCi9kbnMtcXVlcnk'
[static.'cf-iv6-malware-2']
stamp='sdns://AgMAAAAAAAAAFlsyNjA2OjQ3MDA6NDcwMDo6MTAwMl0AFDI2MDY6NDcwMDo0NzAwOjoxMDAyCi9kbnMtcXVlcnk'
_EOF_
#fi

domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
certname=$(curl "https://8.8.8.8/resolve?name=certname.ssl.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
if [ ! -z "$certname" ]; then
 if [ -f "/etc/ssl/${certname}.crt" ]; then
  echo "copying good cert to dnscrypt"
  cat /etc/ssl/${certname}.key > /etc/dnscrypt-proxy/localhost.pem
  cat /etc/ssl/${certname}.crt >> /etc/dnscrypt-proxy/localhost.pem
 fi
fi

#dummy cert for the dns server to run if not found (recommend changing it)
if [ ! -f /etc/dnscrypt-proxy/localhost.pem ]; then
cat > /etc/dnscrypt-proxy/localhost.pem <<_EOF_
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDb7g6EQhbfby97
k4oMbZTzdi2TWFBs7qK/QwgOu+L6EhNHPO1ZEU29v0APFBFJO5zyyAk9bZ9k9tPB
bCuVVI9jEUfLH3UCjEQPG6XI2w++uVh0yALvc/uurCvRHVlle/V7cAoikndc2SjE
RQUALbACIqwD5g0F77BYwcsreB4GH253/R6Q2/CJZ4jNHPjkocOJiVr3ejA0kkoN
MXpGUXWcrVVk20M2A1CeO7HAulLRcklEdoHE3v46pjp0iZK0F9LyZX1U1ql+4QL3
iQttoZ4tMg83lFHSt4G9PrpIhzXr9W4NW822faSvrIwwN/JbItUmRa7n/3+MkuJQ
IGGNDayXAgMBAAECggEBANs0fmGSocuXvYL1Pi4+9qxnCOwIpTi97Zam0BwnZwcL
Bw4FCyiwV4UdX1LoFIailT9i49rHLYzre4oZL6OKgdQjQCSTuQOOHLPWQbpdpWba
w/C5/jr+pkemMZIfJ6BAGiArPt7Qj4oKpFhj1qUj5H9sYXkNTcOx8Fm25rLv6TT9
O7wg0oCpyG+iBSbCYBp9mDMz8pfo4P3BhcFiyKCKeiAC6KuHU81dvuKeFB4XQK+X
no2NqDqe6MBkmTqjNNy+wi1COR7lu34LPiWU5Hq5PdIEqBBUMjlMI6oYlhlgNTdx
SvsqFz3Xs6kpAhJTrSiAqscPYosgaMQxo+LI26PJnikCgYEA9n0OERkm0wSBHnHY
Kx8jaxNYg93jEzVnEgI/MBTJZqEyCs9fF6Imv737VawEN/BhesZZX7bGZQfDo8AT
aiSa5upkkSGXEqTu5ytyoKFTb+dJ/qmx3+zP6dPVzDnc8WPYMoUg7vvjZkXXJgZX
+oMlMUW1wWiDNI3wP19W9Is6xssCgYEA5GqkUBEns6eTFJV0JKqbEORJJ7lx5NZe
cIx+jPpLkILG4mOKOg1TBx0wkxa9cELtsNsM+bPtu9OqRMhsfPBmsXDHhJwg0Z6G
eDTfYYPkpRhwZvl6jBZn9sLVR9wfg2hE+n0lfV3mceg336KOkwAehDU84SWZ2e0S
esqkpbHJa+UCgYA7PY0O8POSzcdWkNf6bS5vAqRIdSCpMjGGc4HKRYSuJNnJHVPm
czNK7Bcm3QPaiexzvI4oYd5G09niVjyUSx3rl7P56Y/MjFVau+d90agjAfyXtyMo
BVtnAGGnBtUiMvP4GGT06xcZMnnmCqpEbBaZQ/7N8Bdwnxh5sqlMdtX2hwKBgAhL
hyQRO2vezgyVUN50A6WdZLq4lVZGIq/bqkzcWhopZaebDc4F5doASV9OGBsXkyI1
EkePLTcA/NH6pVX0NQaEnfpG4To7k46R/PrBm3ATbyGONdEYjzX65VvytoJDKx4d
pVrkKhZA5KaOdLcJ7hHHDSrv/qJXZbBn44rQ5guxAoGBAJ6oeUsUUETakxlmIhmK
xuQmWqLf97BKt8r6Z8CqHKWK7vpG2OmgFYCQGaR7angQ8hmAOv6jM56XhoagDBoc
UoaoEyo9/uCk6NRUkUMj7Tk/5UQSiWLceVH27w+icMFhf1b7EmmNfk+APsiathO5
j4edf1AinVCPwRVVu1dtLL5P
-----END PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIIDAjCCAeoCCQCptj0+TjjIJjANBgkqhkiG9w0BAQsFADBDMREwDwYDVQQKDAhE
TlNDcnlwdDEaMBgGA1UECwwRTG9jYWwgdGVzdCBzZXJ2ZXIxEjAQBgNVBAMMCWxv
Y2FsaG9zdDAeFw0xOTExMTgxNDA2MzBaFw0zMzA3MjcxNDA2MzBaMEMxETAPBgNV
BAoMCEROU0NyeXB0MRowGAYDVQQLDBFMb2NhbCB0ZXN0IHNlcnZlcjESMBAGA1UE
AwwJbG9jYWxob3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2+4O
hEIW328ve5OKDG2U83Ytk1hQbO6iv0MIDrvi+hITRzztWRFNvb9ADxQRSTuc8sgJ
PW2fZPbTwWwrlVSPYxFHyx91AoxEDxulyNsPvrlYdMgC73P7rqwr0R1ZZXv1e3AK
IpJ3XNkoxEUFAC2wAiKsA+YNBe+wWMHLK3geBh9ud/0ekNvwiWeIzRz45KHDiYla
93owNJJKDTF6RlF1nK1VZNtDNgNQnjuxwLpS0XJJRHaBxN7+OqY6dImStBfS8mV9
VNapfuEC94kLbaGeLTIPN5RR0reBvT66SIc16/VuDVvNtn2kr6yMMDfyWyLVJkWu
5/9/jJLiUCBhjQ2slwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQA6Vz5HnGuy8jZz
5i8ipbcDMCZNdpYYnxgD53hEKOfoSv7LaF0ztD8Kmg3s5LHv9EHlkK3+G6FWRGiP
9f6IbtRITaiVQP3M13T78hpN5Qq5jgsqjR7ZcN7Etr6ZFd7G/0+mzqbyBuW/3szt
RdX/YLy1csvjbZoNNuXGWRohXjg0Mjko2tRLmARvxA/gZV5zWycv3BD2BPzyCdS9
MDMYSF0RPiL8+alfwLNqLcqMA5liHlmZa85uapQyoUI3ksKJkEgU53aD8cYhH9Yn
6mVpsrvrcRLBiHlbi24QBolhFkCSRK8bXes8XDIPuD8iYRwlrVBwOakMFQWMqNfI
IMOKJomU
-----END CERTIFICATE-----
_EOF_
fi

cat > /etc/dnscrypt-proxy/refresh-dns.sh <<_EOF_
#!/bin/sh
echo "refrshing dns"

domain=\$(hostname -f | awk -F'.' '{ \$1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')

echo "converting static hosts"
#convert to txt record
#awk '{printf "%s,",\$0} END {print ""}' /etc/dnscrypt-proxy/cloaking-rules.txt

#txt record to file
dig TXT +short statichosts.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0' > /etc/dnscrypt-proxy/cloaking-rules.txt

cat /etc/dnscrypt-proxy/statichosts.txt >> /etc/dnscrypt-proxy/cloaking-rules.txt


#grabbing bootstrap servers
bootstrap_resolvers=\$(dig TXT +short bootstrap_resolvers.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/ /, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')

if [ ! -z "\$bootstrap_resolvers" ]; then
echo "bootstrap_resolvers should be: \$bootstrap_resolvers"
fi

#grabbing custom servers
sdnss=\$(dig TXT +short sdns.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/ /, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')

if [ ! -z "\$sdnss" ]; then

i=0
for sdns in \$(printf '%s\n' "\$sdnss")
do
i=\$((i+1))

sdns="stamp='sdns://"\$sdns"'"
#echo "sdns \$i: \$sdns"

echo "current setting of custom \$i"
currentsdns=\$(awk '/static.'"'"'custom-'\$i'/{getline;print}' /etc/dnscrypt-proxy/bootstrapconfig.toml)
echo "\$currentsdns"

echo "replacing current setting: \$currentsdns with new setting: \$sdns"
awk '/static.'"'"'custom-'\$i'/{ rl = NR + 1 } NR == rl { gsub( /.*/,"'\$sdns'") } 1' /etc/dnscrypt-proxy/bootstrapconfig.toml > /etc/dnscrypt-proxy/bootstrapconfig.toml.tmp && mv /etc/dnscrypt-proxy/bootstrapconfig.toml.tmp /etc/dnscrypt-proxy/bootstrapconfig.toml

#echo "enabling server"
#awk '/static.'"'"'custom-'\$i'/{ rl = NR + 0 } NR == rl { gsub( /#/,"") } 1' /etc/dnscrypt-proxy/bootstrapconfig.toml

done

if [ -f "/etc/dnscrypt-proxy/bootstrapconfig.toml.tmp" ]; then rm /etc/dnscrypt-proxy/bootstrapconfig.toml.tmp; fi

fi

#get blocklists

lists=\$(dig TXT +short blocklists.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')

if [ ! -z "\$lists" ]; then

if [ ! -d "/etc/dnscrypt-proxy/lists" ]; then mkdir -p /etc/dnscrypt-proxy/lists; fi

#cleaning lists
#rm /etc/dnscrypt-proxy/lists/*.txt

for list in \$(printf '%s\n' \$lists)
do

echo "getting urls for \$list"
urls=\$(dig TXT +short \${list}.blocklists.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')

i=0
for url in \$(printf '%s\n' \$urls)
do
i=\$((i+1))
echo "curling \$url to /etc/dnscrypt-proxy/lists/\$list-\$i.txt"
if [ ! -f /etc/dnscrypt-proxy/lists/\$list-\$i.txt ]; then curl -s \$url > /etc/dnscrypt-proxy/lists/\$list-\$i.txt; fi
awk '{print}' /etc/dnscrypt-proxy/lists/\$list-\$i.txt | awk '// {gsub(/127\.0\.0\.1|0\.0\.0\.0|::1|^\.com|\t| |\*./, ""); print}' | awk '// {gsub(/^localhost$|^ip6-localhost$|^localhost.localdomain$|/, ""); print}' | awk '!/^#/{gsub(/#.*/,"",\$0);print}' | awk 'NF > 0' > /etc/dnscrypt-proxy/lists/\$list-\$i.txt.tmp && mv /etc/dnscrypt-proxy/lists/\$list-\$i.txt.tmp /etc/dnscrypt-proxy/lists/\$list-\$i.txt
done

done

echo "combining files"
awk '{print}' /etc/dnscrypt-proxy/lists/*.txt > /etc/dnscrypt-proxy/blocked-names.txt

#echo "sorting"
#awk '{ line[NR] = $0 } END { do { haschanged = 0; for(i=1; i < NR; i++) { if ( line[i] > line[i+1] ) { t = line[i]; line[i] = line[i+1]; line[i+1] = t; haschanged = 1 } } } while ( haschanged == 1 ) for(i=1; i <= NR; i++) { print line[i] } }' /etc/dnscrypt-proxy/blocked-names.txt > /etc/dnscrypt-proxy/blocked-names.txt.sorted 

echo "remove dups"
awk '!(\$0 in a) {a[\$0];print}' /etc/dnscrypt-proxy/blocked-names.txt > /etc/dnscrypt-proxy/blocked-names.txt.dups && mv /etc/dnscrypt-proxy/blocked-names.txt.dups /etc/dnscrypt-proxy/blocked-names.txt

fi

_EOF_

# Create systemd unit
create_systemd_config() {
# Systemd unit
cat > /etc/systemd/system/dnscrypt-proxy-bootstrap.service <<_EOF_
[Unit]
Description=DNSCrypt is a DNS proxy (bootstrap)
ConditionPathExists=/etc/dnscrypt-proxy
After=local-fs.target
[Service]
#User=${_APP_USER_NAME}
#Group=${_APP_USER_NAME}
Type=simple
WorkingDirectory=/etc/dnscrypt-proxy
ExecStart=/etc/dnscrypt-proxy/dnscrypt-proxy -config /etc/dnscrypt-proxy/bootstrapconfig.toml
Restart=on-failure
RestartSec=10
#StandardOutput=syslog
#StandardError=syslog
SyslogIdentifier=dnscrypt-proxy-bootstrap
[Install]
WantedBy=multi-user.target
_EOF_
}

chmod +x /etc/dnscrypt-proxy/refresh-dns.sh
/etc/dnscrypt-proxy/refresh-dns.sh

create_systemd_config
systemctl enable --now dnscrypt-proxy-bootstrap.service

systemctl stop dnscrypt-proxy-bootstrap.service
systemctl start dnscrypt-proxy-bootstrap.service

echo 'to run: ./etc/dnscrypt-proxy/dnscrypt-proxy or systemctl enable --now dnscrypt-proxy-bootstrap.service'

echo 'done'
