#!/bin/env sh

install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


echo "Installing Tasker (cron)"

if [ ! -d /etc/tasker ]; then mkdir -p /etc/tasker; chmod +w /etc/tasker; fi

if [ ! -f /etc/tasker/tasker ]; then 
_BINARY=`curl -s https://api.github.com/repos/adhocore/gronx/releases |  awk '/download_url/ && /linux_amd64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' | awk '{print $1;exit}' || echo 'https://github.com/adhocore/gronx/releases/download/v1.19.3/tasker_1.19.3_linux_amd64.tar.gz'`
curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/tasker.tar.gz
CWD=$PWD
cd /tmp
tar xvf /tmp/tasker.tar.gz
cd /tmp/tasker_*
mv tasker /etc/tasker/.
rm /tmp/tasker.tar.gz
rm LICENSE
cd $CWD
fi

if [ ! -z "$(pidof cron)" ]; then
kill -9 $(pidof cron)
fi

if [ ! -f /etc/tasker/crontab ]; then touch /etc/tasker/crontab; fi

#run startup after 5 minutes and before 6 minutes
if [ -f /etc/cron.hourly/runcronstartupfolder ]; then rm /etc/cron.hourly/runcronstartupfolder; fi
if [ ! -d /etc/cron.minutes ]; then mkdir -p /etc/cron.minutes; fi
cat << 'EOF' > /etc/cron.minutes/runcronstartupfolder
#!/bin/bash
#epochuptime=$(stat --printf='%Y' /proc/uptime)
#epochcurrnt=$(date +%s) >> /etc/cron.hourly/runcronstartupfolder
#echo "uptime of system: $(($epochcurrnt-$epochuptime)) seconds ago"
#(echo ' Currently:' | tr "\n" ' ' ; date +"%Y-%m-%d %k:%M:%S" ; echo '  Up Since:' | tr '\n' ' ' ; uptime -s ; echo '  Duration:' | tr '\n' ' ' ; uptime -p)
#date -d "`cut -f1 -d' ' /proc/uptime` seconds ago" -u -Iseconds
#date -d @$(( $(date +%s) - $(cut -f1 -d. /proc/uptime) ))
#date -d "`cut -f1 -d. /proc/uptime` seconds ago" -u -Iseconds
#stat --printf='%Y' /proc/1
#awk '/btime/{print $2}' /proc/stat
#awk '{print int($1/86400)"days "int($1%86400/3600)":"int(($1%3600)/60)":"int($1%60)}' /proc/uptime
#echo "uptime of sys: $(awk '{m=$1/60; h=m/60; printf "%sd %sh %sm %ss", int(h/24), int(h%24), int(m%60), int($1%60) }' /proc/uptime)"
#echo "uptime of sys: $(awk '{print int($1/86400)" days "int($1%86400/3600)" hours "int(($1%3600)/60)" minutes "int($1%60)" seconds "}' /proc/uptime)"

#if [ "$(awk '{print int($1)}' /proc/uptime)" -le "3660" ]; then
if [ "$(awk '{print int($1)}' /proc/uptime)" -ge "301" ] && [ "$(awk '{print int($1)}' /proc/uptime)" -le "360" ]; then 
 echo "uptime of sys: $(awk '{print int($1/86400)" days "int($1%86400/3600)" hours "int(($1%3600)/60)" minutes "int($1%60)" seconds "}' /proc/uptime)"
 echo "system recently started up running cron.startup"
 if [ ! -d /etc/cron.startup ]; then mkdir /etc/cron.startup; fi
 cd / && /etc/tasker/run-parts --report /etc/cron.startup
fi
if [ -d /etc/cron.startup ]; then echo "#!/bin/bash" > /etc/cron.startup/tasker; awk '{gsub(".*/|.cron.startup", "", FILENAME); print $0 " # from file: " FILENAME "\n"}' /etc/tasker/*.cron.startup >> /etc/cron.startup/tasker; chmod +x /etc/cron.startup/tasker; fi

if [ -d /etc/cron.hourly ]; then echo "#!/bin/bash" > /etc/cron.hourly/tasker; awk '{gsub(".*/|.cron.hourly", "", FILENAME); print $0 " # from file: " FILENAME "\n"}' /etc/tasker/*.cron.hourly >> /etc/cron.hourly/tasker; chmod +x /etc/cron.hourly/tasker; fi
EOF
chmod +x /etc/cron.minutes/runcronstartupfolder

if [ ! -f /etc/systemd/system/tasker.service ]; then
cat << 'EOF' > /etc/systemd/system/tasker.service
[Unit]
Description=Tasker
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/etc/tasker/tasker --file /etc/tasker/crontab

[Install]
WantedBy=multi-user.target
EOF
fi


#if [ ! -f /etc/tasker/run-parts ]; then
cat << 'EOF' > /etc/tasker/run-parts
#!/bin/sh

# alternative to run-parts ..... (?)

USAGE="\
Usage: $0 dirname\n
run all the executable inside the directory <dirname>
"

if [ ! -z $1 ] ; then arg1=$1; fi
if [ ! -z $2 ] ; then arg2=$2; fi
if [ ! -z $3 ] ; then arg3=$3; fi

if [ "$arg1" = "--report" ] ; then
arg1="$arg2"
fi

if [ "$arg2" = "test" ] ; then
        TEST=1
else
        TEST=0
fi

if [ $# = 0 ] ; then
# no arguments
        echo -e $USAGE
        exit 1
fi

if [ ! -d $arg1 ] ; then
        echo "Error, $arg1 isn't a directory."
        exit 1
fi

DIR=$arg1

#FILELIST=`ls $arg1`
# echo $FILELIST

for FILE in "$DIR"/* ; do
        FILENAME=$FILE
#            executable?  AND real file ? (not directory...)
        if [ -x $FILENAME -a -f $FILENAME ] ; then
                if [ "$(echo $FILENAME | awk -F/ '{ print $3 }')" != "cron.minutes" ]; then echo "Executing $FILENAME"; fi
                if [ $TEST = 0 ] ; then
                        #. $FILENAME
                        ("$FILENAME")
                fi
        else
                if [ "$(echo $FILENAME | awk -F/ '{ print $4 }')" != "*" ]; then echo "$FILENAME is not a file or is not executable."; fi
        fi
done

exit 0
EOF
#fi
chmod +x /etc/tasker/run-parts

if [ ! -d /etc/cron.minutes ]; then mkdir /etc/cron.minutes; fi
if [ ! "$(awk '/cron.minutes/' /etc/tasker/crontab)" ]; then
 echo "adding minutes tab" 
 echo "* * * * * cd / && /etc/tasker/run-parts --report /etc/cron.minutes" >> /etc/tasker/crontab
fi

if [ ! -d /etc/cron.hourly ]; then mkdir /etc/cron.hourly; fi
if [ ! "$(awk '/cron.hourly/' /etc/tasker/crontab)" ]; then
 echo "adding hourly tab" 
 rand=`awk -v min=20 -v max=40 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+17))}'`
 echo "$rand * * * * cd / && /etc/tasker/run-parts --report /etc/cron.hourly" >> /etc/tasker/crontab
fi

if [ ! -d /etc/cron.daily ]; then mkdir /etc/cron.daily; fi
if [ ! "$(awk '/cron.daily/' /etc/tasker/crontab)" ]; then
 echo "adding daily tab" 
 rand=`awk -v min=20 -v max=40 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+17))}'`
 randh=`awk -v min=1 -v max=5 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
 echo "$rand $randh * * * cd / && /etc/tasker/run-parts --report /etc/cron.daily" >> /etc/tasker/crontab
fi

if [ ! -d /etc/cron.weekly ]; then mkdir /etc/cron.weekly; fi
if [ ! "$(awk '/cron.weekly/' /etc/tasker/crontab)" ]; then
 echo "adding weekly tab" 
 rand=`awk -v min=20 -v max=40 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+17))}'`
 randh=`awk -v min=1 -v max=5 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
 echo "$rand $randh * * 7 cd / && /etc/tasker/run-parts --report /etc/cron.weekly" >> /etc/tasker/crontab
fi

if [ ! -d /etc/cron.monthly ]; then mkdir /etc/cron.monthly; fi
if [ ! "$(awk '/cron.monthly/' /etc/tasker/crontab)" ]; then
 echo "adding monthly tab" 
 rand=`awk -v min=20 -v max=40 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+17))}'`
 randh=`awk -v min=1 -v max=5 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
 echo "$rand $randh 1 * * cd / && /etc/tasker/run-parts --report /etc/cron.monthly" >> /etc/tasker/crontab
fi

if [ ! -d /etc/cron.yearly ]; then mkdir /etc/cron.yearly; fi
if [ ! "$(awk '/cron.yearly/' /etc/tasker/crontab)" ]; then
 echo "adding yearly tab" 
 rand=`awk -v min=20 -v max=40 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+17))}'`
 randh=`awk -v min=1 -v max=5 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
 echo "$rand $randh 1 1 * cd / && /etc/tasker/run-parts --report /etc/cron.yearly" >> /etc/tasker/crontab
fi

if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor; fi
if [ ! -f /etc/supervisor/program:tasker.args ]; then
echo "command = /etc/tasker/tasker -file /etc/tasker/crontab" > /etc/supervisor/program:tasker.args
fi

echo -n "Enable systemd tasker? (y/N) "
read ans;

case $ans in
y|Y)
systemctl stop cron
systemctl disable cron
systemctl enable --now tasker;;
n|N)
echo "manually enable: systemctl enable --now tasker"
echo "**remember to disable and stop cron (if running)**";;
*)
echo "manually enable: systemctl enable --now tasker"
echo "**remember to disable and stop cron (if running)**";;
esac
