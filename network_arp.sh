#!/bin/bash


install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


#Flush ARP table
#windows: netsh interface IP delete arpcache
#ip neigh flush all

if [ ! -d /etc/tasker ]; then mkdir /etc/tasker; fi
echo "$(realpath $0)" > /etc/tasker/arpscan.cron.hourly
chmod +x /etc/tasker/arpscan.cron.hourly

retrydownload() {
#retrydownload curl https://example.com /dev/null
prog="$1"
url="$2"
file="$3"
alturl="$url"
if [ "$(echo "$url" | awk '/\/ipfs\/|\/ipns\/|.ipfs.|.ipns./ {print $0}')" = "$url" ]; then
 echo "ipfs/ipns url found"
 cidurl="$(echo "$url" | awk -F'.' '{ sub(/http:\/\/|https:\/\//, ""); print $1}')"
 filename="$(echo "$url" | awk -F / '{print $NF}')"  # or "$(echo ${url##*/})" 
 proto="$(echo $url | awk -F'/' '{print $1"//"}')"
 alturl="$(echo ${proto}${cidurl}.ipfs.4everland.io/${filename})"
fi

cmdprog=""
if [ "$prog" = "wget" ]; then cmdprog="/bin/wget -q -O $file"; fi
if [ "$prog" = "curl" ]; then cmdprog="/bin/curl -L --doh-url https://1.1.1.1/dns-query -s -o $file"; fi
#echo $cmdprog
i=0
while [ $i -lt 20 ]
do
    i=$((i + 1))
    if [ $i -ge 10 ]; then url="$alturl"; fi
    echo "downloading from $url using $prog and saving to $file"
    $($cmdprog $url > $file)
    result=$?
    if [ "$result" -eq 0 ] && [ -s "$file" ] && [ "$(awk '{if(NR==1) {print substr($1,1,1)}}' $file)" != "<" ]; then
        break
    fi
    echo "Error: Retry Download Again... $i time(s)"
    if [ $i -eq 20 ]; then echo "Error: please download file again"; break; fi
    sleep 3
done
#if [ $i -lt 20 ]; then echo "Successful after $i trie(s)"; fi
}



if [ ! -f /etc/goarp/goarp ]; then
_BINARY=`install_package goarp || echo 'https://bafybeifrkc6znz2z3ifg6oxyzbnswxnfjldnoxw6dm5o766swp33akysbq.ipfs.w3s.link/goarp_v1.0.0_linux_amd64'`
#curl -Ls $_BINARY -o /tmp/goarp
retrydownload curl $_BINARY /tmp/goarp
mkdir -p /etc/goarp
mv /tmp/goarp /etc/goarp/.
chmod +x /etc/goarp/goarp
fi

if [ ! -f /etc/goip/goip ]; then
_BINARY=`install_package goip || echo 'https://bafybeidzspaknaotcpfgnkmdasgkubrtpzhc6uvrva7qtlovxrg3geomyu.ipfs.w3s.link/goip_v1.0.0_linux_amd64'`
#curl -Ls $_BINARY -o /tmp/goip
retrydownload curl $_BINARY /tmp/goip
mkdir -p /etc/goip
mv /tmp/goip /etc/goip/.
chmod +x /etc/goip/goip
fi

echo "Getting ARP cache"
if [ ! -d /etc/arp ]; then mkdir -p /etc/arp; fi
awk '!/0x0/ { print "?","("$1") at",$4" [ether] on",$6 }' /proc/net/arp | awk '!/00:00:00:00:00:00|HW/' > /etc/arp/arp.cache

#Get IP and post to arp cache
/etc/goip/goip | awk '/(status=up)/ && !/(name=lo)/ && !/(mac= )/ { gsub(/index=|name=|addr=|mac=|mtu=|flags=|status=/,""); print $2,$4,$3; }' | awk -F'/' '{ print $1 }' | awk '{ if($3 !~ /:/) print $0;}' | awk '{ print "?","("$3")","at",$2,"[ether] on",$1; }' >> /etc/arp/arp.cache

#get ips
ipip=`awk '/Main:/,/Local:/' /proc/net/fib_trie | awk '/host LOCAL/ { print f$1 } {f=$2}' | awk '!/^127/' | awk NF`

#get local private networks connected to system  
ipnet=`awk '/Main:/,/Local:/' /proc/net/fib_trie | awk '/link UNICAST/ { print f$1 } {f=$2}' | awk '/^10\.|^172\.(1[6-9]|2[0-9]|3[0-1])|^192./' | awk 'BEGIN{ORS=" "}1' | awk '{gsub(/ /,"\n");print}' | awk NF`

IPCIDRS=""
for ipsub in $(echo $ipnet)
do
BASE_IP=${ipsub%/*}
IP_CIDR=${ipsub#*/}

if [ $IP_CIDR -lt 22 ]; then 
IP_CIDR=24
for ip24 in $ipip
do
 if [ "$(echo $BASE_IP | awk -F. '{ print $1"."$2"." }')" = "$(echo $ip24 | awk -F. '{ print $1"."$2"." }')" ]; then BASE_IP=`echo $ip24 | awk -F. '{ print $1"."$2"."$3".0" }'`; fi
done
fi

IPCIDRS=$(echo "${IPCIDRS} ${BASE_IP}/${IP_CIDR}")
done

#IPCIDRS=$(echo "$IPCIDRS" | awk '{gsub(/^[ \t]+|[ \t]+$/,"");print}')

for ipsub in $(echo "$IPCIDRS" | awk '{gsub(/^[ \t]+|[ \t]+$/,"");print}')
do
BASE_IP=${ipsub%/*}
IP_CIDR=${ipsub#*/}

IP_MASK=$((0xFFFFFFFF << (32 - ${IP_CIDR})))
IFS=. read a b c d <<<${BASE_IP}
ip=$((($b << 16) + ($c << 8) + $d))

ipstart=$((${ip} & ${IP_MASK}))
ipend=$(((${ipstart} | ~${IP_MASK}) & 0x7FFFFFFF))

 seq ${ipstart} ${ipend} | while read i; do
    ipaddr=`echo $a.$((($i & 0xFF0000) >> 16)).$((($i & 0xFF00) >> 8)).$(($i & 0x00FF))`
    echo "running... /etc/goarp/goarp -ar ${ipaddr}/32"
    arpscan=$(/etc/goarp/goarp -ar ${ipaddr}/32 2>&1 | awk '/Online/' | awk '{ print "?","("$1") at",$3" [ether] on arpscan" }') 
    if [ ! -z "$arpscan" ]; then
     echo "$arpscan" >> /etc/arp/arp.cache
     echo "Found device: $(echo $arpscan | awk '{ print $4" on "$2 }')"
    fi
 done

done

echo "ARP cache file before modifications"
awk '{ print }' /etc/arp/arp.cache


echo "Cleaning up arp file"

#find duplicates
duplicates=$(awk '{ print $2,$4 }' /etc/arp/arp.cache | awk '{ cnts[$0] += 1 } END { for (v in cnts) print cnts[v], v }' | awk '!/^1/')
echo "There is $(echo "$duplicates" | awk 'END { print NR }') duplicate(s)"

for mac in $(echo "$duplicates" | awk '{ print $3 }')
do
echo "Removing duplicate $mac from file"
awk '!(/'$mac'/ && /arpscan/)' /etc/arp/arp.cache > /etc/arp/arp.tmp && mv /etc/arp/arp.tmp /etc/arp/arp.cache
done

#find duplicates again
duplicates=$(awk '{ print $2,$4 }' /etc/arp/arp.cache | awk '{ cnts[$0] += 1 } END { for (v in cnts) print cnts[v], v }' | awk '!/^1/')

echo "ARP cache file after modifications"
awk '{ print }' /etc/arp/arp.cache

if [ ! -z "$duplicates" ]; then echo "Please fix all of the following duplicates: $(echo $duplicates | awk '{print "\n"$2,$3}')"; fi

#json
printf "[" > /etc/arp/getArp
awk '{ gsub(/\(|\)/, ""); print "{\"mac\":\""$4"\",\"ip\":\""$2"\",\"intf\":\"eth0\",\"expired\":false,\"expires\":-1,\"permanent\":true,\"type\":\"ethernet\",\"manufacturer\":\"\",\"hostname\":\"\",\"intf_description\":\"lan\"}," }' /etc/arp/arp.cache >> /etc/arp/getArp
printf "]" >> /etc/arp/getArp
awk NF=NF RS= OFS= /etc/arp/getArp > /etc/arp/getArp.tmp
awk '{sub(/,]/,"]");print}' /etc/arp/getArp.tmp > /etc/arp/getArp
rm /etc/arp/getArp.tmp

