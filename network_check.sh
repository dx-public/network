#!/bin/bash

showinfo="$1"

cache_file()
{
  if [ "${arg5}" = "debug" ]; then echo "cache_file function was called as : $@"; fi
  #default settings
  if [ ! -z "$1" ]; then arg1="$1"; else arg1='https://example.invalid'; fi #default to null url
  if [ ! -z "$2" ]; then arg2="$2"; else arg2='/dev/null'; fi #default to null file
  if [ ! -z "$3" ]; then arg3="$3"; else arg3='4'; fi #default use ipv4
  if [ ! -z "$4" ]; then arg4="$4"; else arg4='43200'; fi #default 1day in secs
  if [ ! -z "$5" ]; then arg5="$5"; else arg5='variable'; fi #default variable (console, debug, variable, file)

  if [ "${arg5}" = "debug" ]; then echo "url: $arg1"; fi
  if [ "${arg5}" = "debug" ]; then echo "file: $arg2"; fi
  if [ "${arg5}" = "debug" ]; then echo "proto: $arg3"; fi
  if [ "${arg5}" = "debug" ]; then echo "oldsecs: $arg4"; fi
  if [ "${arg5}" = "debug" ]; then echo "output: $arg5"; fi

  if [ -f "$arg2" ]; then
    if [ "${arg5}" = "debug" ]; then echo "file exists"; fi
    if [ "$(( $(date +"%s") - $(date -r ${arg2} +%s) ))" -gt "$arg4" ]; then
     if [ ${arg5} = "debug" ] || [ ${arg5} = "file" ]; then echo "${arg2} file is older then ${arg4} seconds.. redownloading via ipv${arg3} from ${arg1}"; fi
     curlcmd=`curl -${arg3}s "${arg1}" | tee "${arg2}"`
    fi
  fi

  if [ ! -f "${arg2}" ]; then
   if [ ${arg5} = "debug" ] || [ ${arg5} = "file" ]; then echo "${arg2} file does not exist... downloading via ipv${arg3} from ${arg1}"; fi
   curlcmd=`curl -${arg3}s "${arg1}" | tee "${arg2}"`
  fi

  if [ "${arg5}" = "variable" ]; then cat ${arg2}; fi
  return
}


getnetstat() {
lport=$1
ports=""
protos="tcp udp tcp6 udp6"
for proto in $protos
do
port=$(awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2));
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
}
NR > 1 {{if(NR==2)print "";local=getIP($2);remote=getIP($3)}{print local"-"remote}}' /proc/net/$proto)
ports="$ports $port"
done
ports=$(echo "$ports" | awk 'NF > 0' | awk '!a[$0]++' | awk '/0:'$lport'-/')

if [ ! -z "$ports" ]; then
 echo "port $lport is listening..."
 return 0
else
 echo "port $lport not found..."
 return 1
fi
}
#getnetstat $1




ldns=$(cat /etc/resolv.conf | grep '^nameserver' | awk '{ print $2 }')
lipaddr=$(ip address | awk '/inet/ && !/ 127| ::1/ {gsub("/[^/]*$","",$2); print $2 }')
rootlmdomain=$(hostname -d | rev | awk -F\. '{ print $1"."$2 }' | rev)


if [ ! -z "$showinfo" ]; then
cftracev4=$(cache_file 'https://1.1.1.1/cdn-cgi/trace' '/tmp/cftracev4.txt' '4')
cftracev6=$(cache_file 'https://[2606:4700:4700::1111]/cdn-cgi/trace' '/tmp/cftracev6.txt' '6')

eipaddrv4=$(dig A @208.67.222.222 +tls myip.opendns.com +short  2>/dev/null)
if [ "$?" != 0 ]; then eipaddrv4=$(echo "$cftracev4" | awk -F= '/ip/ { print $2}'); fi
if [ ! -z "$eipaddrv4" ]; then econv4='4'; else econv6=''; fi
eipaddrv6=$(dig AAAA @2620:119:53::53 +tls myip.opendns.com +short  2>/dev/null)
if [ "$?" != 0 ]; then eipaddrv6=$(echo "$cftracev6" | awk -F= '/ip/ { print $2}'); fi
if [ ! -z "$eipaddrv6" ]; then econv6='6'; else econv6=''; fi
if echo "$lipaddr" | grep -q "$eipaddrv4"; then ntypev4='Direct'; else ntypev4='NAT or other'; fi
if echo "$lipaddr" | grep -q "$eipaddrv6"; then ntypev6='Direct'; else ntypev6='NAT or other'; fi
if [ ! -z $econv4 ]; then txteconv4='Enabled'; else txteconv4='Disabled'; fi
if [ ! -z $econv6 ]; then txteconv6='Enabled'; else txteconv6='Disabled'; fi

printf "\n"
printf "Currently configured settings:\n"
printf "DNS:\t\t\t\t $(echo $ldns | tr '\n' ' ')\n"
printf "HOSTNAME:\t\t\t $(hostname)\n"
printf "DOMAIN:\t\t\t\t $(hostname -d)\n"
printf "FQDN:\t\t\t\t $(hostname -f)\n"
printf "Local IP(s):\t\t\t $(echo $lipaddr | tr '\n' ' ')\n"
printf "External IP(s):\t\t\t $(echo $eipaddrv4 $eipaddrv6)\n"
printf "Net Type v4:\t\t\t $ntypev4\n"
printf "Net Type v6:\t\t\t $ntypev6\n"
printf "External Connection v4 (cf):\t ${txteconv4}\n"
printf "External Connection v6 (cf):\t ${txteconv6}\n"
printf "\nDNS Tests (Assumed Primary)\n"
printf '%s\n' --------------------
printf "Cloudflare (Ping): $(ping -c 2 1.1.1.1 | tail -n -1)\n"
printf "Cloudflare (53):  $(dig @1.1.1.1 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Cloudflare (853): $(dig @1.1.1.1 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Cloudflare (443): $(dig @1.1.1.1 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "Google (Ping): $(ping -c 2 8.8.8.8 | tail -n -1)\n"
printf "Google (53):  $(dig @8.8.8.8 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Google (853): $(dig @8.8.8.8 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Google (443): $(dig @8.8.8.8 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "Quad9 (Ping): $(ping -c 2 9.9.9.9 | tail -n -1)\n"
printf "Quad9 (53):  $(dig @9.9.9.9 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Quad9 (853): $(dig @9.9.9.9 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Quad9 (443): $(dig @9.9.9.9 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "Cisco (Ping): $(ping -c 2 208.67.220.220 | tail -n -1)\n"
printf "Cisco (53):  $(dig @208.67.220.220 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Cisco (853): $(dig @208.67.220.220 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Cisco (443): $(dig @208.67.220.220 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "NextDNS (Ping): $(ping -c 2 45.90.28.0 | tail -n -1)\n"
printf "NextDNS (53):  $(dig @45.90.28.0 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "NextDNS (853): $(dig @45.90.28.0 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "NextDNS (443): $(dig @45.90.28.0 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "Level3 (Ping): $(ping -c 2 4.2.2.1 | tail -n -1)\n"
printf "Level3 (53): $(dig @4.2.2.1 +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Level3 (853):$(dig @4.2.2.1 +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Level3 (443):$(dig @4.2.2.1 +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"

for sldns in $ldns
do
printf "Current DNS (Ping $sldns): $(ping -c 2 $sldns | tail -n -1)\n"
printf "Current DNS (53):  $(dig @$(echo $ldns | head -n 1) +time=5 +tries=1 ${rootlmdomain} | grep 'status:')\n"
printf "Current DNS (853): $(dig @$(echo $ldns | head -n 1) +time=5 +tries=1 +tls ${rootlmdomain} | grep 'status:')\n"
printf "Current DNS (443): $(dig @$(echo $ldns | head -n 1) +time=5 +tries=1 +https ${rootlmdomain} | grep 'status:')\n"
printf "\n"
done

#create tmp directory
if [ ! -d /tmp/scripts ]; then mkdir -p /tmp/scripts; touch /tmp/scripts/scripts.txt; fi

#ASN Check Script
cache_file 'https://raw.githubusercontent.com/nitefood/asn/master/asn' '/tmp/scripts/asn.sh' '4' '' 'file'

#NextDNS
cache_file 'https://nextdns.io/diag' '/tmp/scripts/nextdns-diag.sh' '4' '' 'file'
#sh -c 'sh -c "$(curl -s https://nextdns.io/diag)"'

#make scripts dir exec
chmod u+x /tmp/scripts/*

fi # end of block [dont run if not passed another parameter]

#echo "------------------------------------------------------------------------------------------------------------"
echo "fixing network"
#lipaddr
#ldns
if getnetstat 53 >/dev/null; then echo "local dns is available"; rnds=$lipaddr;
else echo "local dns is not available"; rnds='1.1.1.2 9.9.9.11';
fi

if [ -z $(echo "$lipaddr" | awk '! /^172\.|^10\.|^192\.168|^fe80:/') ]; then rdns='1.1.1.2 9.9.9.11'; fi

for sldns in $lipaddr
do
echo "dns check: $sldns"
getnetstat 53
done



