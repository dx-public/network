#!/bin/env sh

install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


echo "Installing Caddy"

if [ ! -d /etc/caddy ]; then mkdir -p /etc/caddy/; chmod +w /etc/caddy; fi
if [ ! -d /etc/caddy/vhosts ]; then mkdir -p /etc/caddy/vhosts; chmod +w /etc/caddy/vhosts; fi
if [ ! -d /etc/caddy/www ]; then mkdir -p /etc/caddy/www; echo "<html></html>" > /etc/caddy/www/index.html; fi
if [ ! -d /etc/caddy/logs ]; then mkdir -p /etc/caddy/logs; chmod +w /etc/caddy/logs; fi

if [ ! -f /etc/caddy/caddy ]; then 


#_BINARY=`curl -s https://api.github.com/repos/caddyserver/caddy/releases/latest |  awk '/download_url/ && /linux_amd64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' || echo 'https://github.com/caddyserver/caddy/releases/download/v2.6.4/caddy_2.6.4_linux_amd64.tar.gz'`

_BINARY=`install_package caddy || echo 'https://github.com/caddyserver/caddy/releases/download/v2.8.4/caddy_2.8.4_linux_amd64.tar.gz'`

_BINARY=$(echo $_BINARY | awk '{gsub(/["]/,""); print}')
echo "downloading caddy from $_BINARY"

if [ ! -z "$(echo $_BINARY | awk -F'/' '/tar.gz/{$1=""; $2=""; $3=""; print $NF}')" ]; then
 curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/caddy.tar.gz
 CWD=$PWD
 cd /tmp
 tar xvf /tmp/caddy.tar.gz
 mv caddy /etc/caddy/.
 rm /tmp/caddy.tar.gz
 rm README.md
 rm LICENSE
 cd $CWD
else
 curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /etc/caddy/caddy
 chmod +x /etc/caddy/caddy
fi


fi

cat << \EOF > "/etc/caddy/Caddyfile"
#https://caddyserver.com/docs/caddyfile/options
{
#	# General Options
#	debug
#	http_port    <port>
#	https_port   <port>
#	default_bind <hosts...>
#	order <dir1> first|last|[before|after <dir2>]
#	storage <module_name> {
#		<options...>
#	}
#	storage_clean_interval <duration>
#	renew_interval <duration>
#	ocsp_interval  <duration>
	admin   off 
#	{
#		origins <origins...>
#		enforce_origin
#	}
#	persist_config off
#	log [name] {
#		output  <writer_module> ...
#		format  <encoder_module> ...
#		level   <level>
#		include <namespaces...>
#		exclude <namespaces...>
#	}
#	grace_period   <duration>
#	shutdown_delay <duration>
#
#	# TLS Options
#	auto_https off|disable_redirects|ignore_loaded_certs|disable_certs
	auto_https disable_redirects
#	email <yours>
#	default_sni <name>
	local_certs
#	skip_install_trust
#	acme_ca <directory_url>
#	acme_ca_root <pem_file>
#	acme_eab <key_id> <mac_key>
#	acme_dns <provider> ...
#	on_demand_tls {
#		ask      <endpoint>
#		interval <duration>
#		burst    <n>
#	}
#	key_type ed25519|p256|p384|rsa2048|rsa4096
#	cert_issuer <name> ...
#	ocsp_stapling off
#	preferred_chains [smallest] {
#		root_common_name <common_names...>
#		any_common_name  <common_names...>
#	}
#
#	# Server Options
#	servers [<listener_address>] {
#		name <name>
#		listener_wrappers {
#			<listener_wrappers...>
#		}
#		timeouts {
#			read_body   <duration>
#			read_header <duration>
#			write       <duration>
#			idle        <duration>
#		}
#		trusted_proxies <module> ...
#		metrics
#		max_header_size <size>
#		log_credentials
#		protocols [h1|h2|h2c|h3]
#		strict_sni_host [on|insecure_off]
#	}
#
#	# PKI Options
#	pki {
#		ca [<id>] {
#			name                  <name>
#			root_cn               <name>
#			intermediate_cn       <name>
#			intermediate_lifetime <duration>
#			root {
#				format <format>
#				cert   <path>
#				key    <path>
#			}
#			intermediate {
#				format <format>
#				cert   <path>
#				key    <path>
#			}
#		}
#	}
#
#	# Event options
#	events {
#		on <event> <handler...>
#	}
}

(proxy_headers) {
	#header_up Host {host}
	header_up X-Forwarded-For {remote}
	header_up X-Forwarded-Port {server-port}
	header_up X-Real-IP {remote_host}
	header_up X-Forwarded-Proto {scheme}
	header_up Access-Control-Allow-Origin *
	header_up Access-Control-Allow-Credentials true
	header_up Access-Control-Allow-Headers Cache-Control,Content-Type
}

(lockdown_files) {
	@blocked {
		path *.htaccess *.md *.mdown *.env *.git *.ssh /README /config/* /.config/* /.profile/*
	}
	respond @blocked 404
}

(secure_headers) {
#	# Enable cross-site filter (XSS) and tell browser to block detected attacks
	header X-XSS-Protection "1; mode=block"
#	# Prevent some browsers from MIME-sniffing a response away from the declared Content-Type
	header X-Content-Type-Options "nosniff"
#	# Disallow the site to be rendered within a frame (clickjacking protection)
	header X-Frame-Options "DENY"
#	# Hide server header field
	header -Server
#	# Hide X-Powered-By field
	header -X-Powered-By
#	Refferer
	header ?Referrer-Policy "no-referrer"
#	Permissions Policy
	header ?Permissions-Policy "usb=(),payment=(),display-capture=(),camera=(),geolocation=(),microphone=()"
#	Cache Control
	header ?Cache-Control "max-age:86400, private"
}

(logging_options) {
	log {
		output file /etc/caddy/logs/access.log {
			roll_size 5mb
			roll_keep 3
			roll_keep_for 720h
		}
	}
}

(common) {
	import lockdown_files
	import secure_headers
	import logging_options
}

(default_location) {
	root /etc/caddy/www/
}

(defaulttemplate) {
root /etc/caddy/www/
header / {
#	# Enable HTTP Strict Transport Security (HSTS) to force clients to always
#	# connect via HTTPS (do not use if only testing)
#	# Strict-Transport-Security "max-age=31536000;"
#	# Enable cross-site filter (XSS) and tell browser to block detected attacks
	X-XSS-Protection "1; mode=block"
#	# Prevent some browsers from MIME-sniffing a response away from the declared Content-Type
	X-Content-Type-Options "nosniff"
#	# Disallow the site to be rendered within a frame (clickjacking protection)
	X-Frame-Options "DENY"
#	# Hide server header field
	-Server
#	# Disable caching since https
#	# The cache should not store anything about the client request or server response.
#	#Cache-Control "no-store"
#	#Pragma "no-cache"
}
# Better manage server connectivity in the face of buggy or malicious clients.
#timeouts {
#	read   10s # Maximum duration for reading the entire request, including the body.
#	header 10s # The amount of time allowed to read request headers.
#	write  10s # The maximum duration before timing out writes of the response.
#	idle   2m # The maximum time to wait for the next request (when using keep-alive).
#}
# Limit the HTTP header and body sizes to reasonable values
# Increase if legitmate traffic results in HTTP 413 errors
#limits {
#	header 4kb
#	#body   / 10mb
#}
# use gzip compression if the client supports it.
#gzip
#log / /var/log/caddy/access.log {
#	rotate_size     5       # Rotate after the log file reaches this size (in megabytes)
#	rotate_age      90      # Keep rotated files for this many days
#	rotate_keep     40      # Keep at most this many log files (older files get pruned)
#	rotate_compress         # compress rotated log files. gzip is the only format supported.
#}
#errors /var/log/caddy/error.log {
#	rotate_size     5      # Rotate after the log file reaches this size (in megabytes)
#	rotate_age      90     # Keep rotated files for this many days
#	rotate_keep     40     # Keep at most this many log files (older files get pruned)
#	rotate_compress        # compress rotated log files. gzip is the only format supported.
#}
}
import /etc/caddy/vhosts/*.caddyfile
EOF

/etc/caddy/caddy fmt --overwrite --diff /etc/caddy/Caddyfile

cat > "/etc/caddy/vhosts/example.caddyfile" <<-'EOF'
#(redirect) {
#	@http {
#		protocol http
#	}
#	redir @http https://{host}{uri}
#}

#import redirect

#(snippet) {
#  respond "Yahaha! You found {args.0}!"
#}

#a.example.com {
#	import snippet "Example A"
#}

#b.example.com {
#	import snippet "Example B"
#}

#example.com {
#	redir {
#		if {scheme} is http
#		/ https://{host}{uri}
#	}
#}

#example.com {
#	redir 302 {
#		if {path} is /
#		/ /login
#	}
#	login {
#		github client_id={$GITHUB_CLIENT_ID},client_secret={$GITHUB_CLIENT_SECRET}
#		redirect_check_referer false
#		success_url /home.html
#		logout_url /logout
#	}
#	jwt {
#		path /
#		redirect https://{host}/login
#		allow sub {$GITHUB_ALLOWED_USERS}
#	}
#}

# HTTP->HTTPS redirects
#http://xyz.example, http://api.xyz.example {
#	redir https://{host}{uri}
#}

#https://xyz.example {
#	# Custom SSL Conf
#	tls /ssl/certs/fullchain.pem /ssl/certs/key.pem
#}

#https://xyz.example {
#	# Custom SSL email
#	tls email@example.com
#}

#https://xyz.example {
#	# Custom SSL self
#	tls self_signed
#}

#example1.com {
#	root * /www/example.com
#	file_server
#}

#example2.com {
#	reverse_proxy localhost:9000
#}

#localhost {
#	reverse_proxy /api/* localhost:9001
#	file_server
#}

#example.com {
#	root * /var/www
#	reverse_proxy /api/* localhost:5000
#	file_server
#}

#example.com {
#	root * /file/path/content
#	hide homework.jpeg
#	file_server browse 
#}

#example.com {
#	root * /srv/public
#	encode gzip
#	php_fastcgi localhost:9000
#	file_server
#}

#www.example-one.com, www.example-two.com {
#	redir https://{labels.1}.{labels.0}{uri}
#}

#example-one.com, example-two.com {
#}

#www.example.com {
#	redir https://example.com{uri}
#}

#example.com {
#}

#example.com {
#	rewrite /add     /add/
#	rewrite /remove/ /remove
#}

#example.com {
#	root * /path/to/site
#	encode gzip
#	try_files {path} /index.html
#	file_server
#}

#example.com {
#	encode gzip
#	handle /api/* {
#		reverse_proxy backend:8000
#	}
#	handle {
#		root * /path/to/site
#		try_files {path} /index.html
#		file_server
#	}
#}

#localhost

#reverse_proxy localhost:9000 localhost:9001 {
#	lb_policy first
#}

#localhost {
#	respond "Hello, world!"
#}

#localhost:2016 {
#	respond "Goodbye, world!"
#}

EOF

masterdomain=$(hostname -f | awk -F. 'BEGIN{OFS=FS} {sub(".*" $1 FS,"")}1')
domains=$(curl "https://8.8.8.8/resolve?name=cfg-domains.caddy.${masterdomain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' )

echo "flush current vhosts"
rm -f /etc/caddy/vhosts/*.caddyfile

for domain in $domains
do
echo "getting caddy config for domain:$domain"
cfg=$(curl "https://8.8.8.8/resolve?name=cfg-vhost.caddy.${domain}&type=txt" 2>/dev/null | awk '/"Answer"/,EOF' | awk '// {gsub(/,"/, "\n"); print}' | awk '/data\":/{gsub("data\":","",$1); print $0}' | awk '{gsub(/\\\\/,"\\");print}' | awk '{gsub(/^[ "]+|[ "]+$/,"");print}' | awk '// {sub(/\"}]/, ""); print}')
awk 'BEGIN { print "'"$cfg"'" }' | awk '{ gsub(/\\n/, "\n"); gsub(/\\t/, "\t"); gsub(/^M/, "\n"); print }' > /etc/caddy/vhosts/$domain.caddyfile
/etc/caddy/caddy fmt --overwrite --diff /etc/caddy/vhosts/$domain.caddyfile
done



if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor; fi
if [ ! -f /etc/supervisor/program:caddy.args ]; then
echo "command = /etc/caddy/caddy run --config /etc/caddy/Caddyfile" > /etc/supervisor/program:caddy.args
fi

echo "done"
