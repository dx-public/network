#!/bin/env bash

echo "Installing ZeroTier"

curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi

echo "Sleeping for 10 secs to get online"
sleep 10

echo "Checking if there is a zerotierid..."
zerotierid=`dig TXT +short +tls zerotierid.$(hostname -d). | tr -d '"'`
if [ ! -z "$zerotierid" ]; then echo 'there is!'; zerotier-cli join $zerotierid; fi

echo "Getting Info"
zerotier-cli info -j

echo "Listing Peers"
zerotier-cli listpeers

echo "Peers"
zerotier-cli peers

echo "Getting Status"
zerotier-cli status

echo "List Network"
zerotier-cli listnetworks

echo "DONE"
