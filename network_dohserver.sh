#!/bin/env sh

install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


echo "Installing DoH Server"

if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor/; chmod +w /etc/supervisor; fi

retrydownload() {
#retrydownload curl https://example.com /dev/null
prog="$1"
url="$2"
file="$3"
alturl="$url"
if [ "$(echo "$url" | awk '/\/ipfs\/|\/ipns\/|.ipfs.|.ipns./ {print $0}')" = "$url" ]; then
 echo "ipfs/ipns url found"
 cidurl="$(echo "$url" | awk -F'.' '{ sub(/http:\/\/|https:\/\//, ""); print $1}')"
 filename="$(echo "$url" | awk -F / '{print $NF}')"  # or "$(echo ${url##*/})" 
 proto="$(echo $url | awk -F'/' '{print $1"//"}')"
 alturl="$(echo ${proto}${cidurl}.ipfs.4everland.io/${filename})"
fi

cmdprog=""
if [ "$prog" = "wget" ]; then cmdprog="/bin/wget -q -O $file"; fi
if [ "$prog" = "curl" ]; then cmdprog="/bin/curl -Ls -o $file"; fi
#echo $cmdprog
i=0
while [ $i -lt 20 ]
do
    i=$((i + 1))
    if [ $i -ge 10 ]; then url="$alturl"; fi
    echo "downloading from $url using $prog and saving to $file"
    $($cmdprog $url)
    result=$?
    if [ "$result" -eq 0 ] && [ -s "$file" ] && [ "$(awk '{if(NR==1) {print substr($1,1,1)}}' $file)" != "<" ]; then
        break
    fi
    echo "Error: Retry Download Again... $i time(s)"
    if [ $i -eq 20 ]; then echo "Error: please download file again"; break; fi
    sleep 3
done
#if [ $i -lt 20 ]; then echo "Successful after $i trie(s)"; fi
}




if [ ! -f /etc/godohserver/godohserver ]; then
_BINARY=`install_package godohserver || echo 'https://bafybeifompngl4t6b7lraeyhwgj4s7gdel7wgx2r4ks2dsiceulzlntueq.ipfs.w3s.link/gontpresponder_v2.3.4_linux_amd64'`

#curl -Ls $_BINARY -o /tmp/godohserver
retrydownload curl $_BINARY /tmp/godohserver
mkdir -p /etc/godohserver
mv /tmp/godohserver /etc/godohserver/.
chmod +x /etc/godohserver/godohserver
fi


cat > "/etc/godohserver/doh-server.conf" <<-'EOF'
# HTTP listen port
listen = [
    #"127.0.0.1:8053",
    #"[::1]:8053",

    ## To listen on both 0.0.0.0:8053 and [::]:8053, use the following line
     ":2053",
]

# Local address and port for upstream DNS
# If left empty, a local address is automatically chosen.
local_addr = ""

# TLS certification file
# If left empty, plain-text HTTP will be used.
# You are recommended to leave empty and to use a server load balancer (e.g.
# Caddy, Nginx) and set up TLS there, because this program does not do OCSP
# Stapling, which is necessary for client bootstrapping in a network
# environment with completely no traditional DNS service.
cert = ""

# TLS private key file
key = ""

# HTTP path for resolve application
path = "/resolve"

# Upstream DNS resolver
# If multiple servers are specified, a random one will be chosen each time.
# You can use "udp", "tcp" or "tcp-tls" for the type prefix.
# For "udp", UDP will first be used, and switch to TCP when the server asks to
# or the response is too large.
# For "tcp", only TCP will be used.
# For "tcp-tls", DNS-over-TLS (RFC 7858) will be used to secure the upstream connection.
upstream = [
    "udp:127.0.0.1:53",
]

# Upstream timeout
timeout = 10

# Number of tries if upstream DNS fails
tries = 3

# Enable logging
verbose = false

# Enable log IP from HTTPS-reverse proxy header: X-Forwarded-For or X-Real-IP
# Note: http uri/useragent log cannot be controlled by this config
log_guessed_client_ip = false

# By default, non global IP addresses are never forwarded to upstream servers.
# This is to prevent two things from happening:
#   1. the upstream server knowing your private LAN addresses;
#   2. the upstream server unable to provide geographically near results,
#      or even fail to provide any result.
# However, if you are deploying a split tunnel corporation network
# environment, or for any other reason you want to inhibit this
# behavior and allow local (eg RFC1918) address to be forwarded,
# change the following option to "true".
ecs_allow_non_global_ip = false

# If ECS is added to the request, let the full IP address or
# cap it to 24 or 128 mask. This option is to be used only on private
# networks where knwoledge of the terminal endpoint may be required for
# security purposes (eg. DNS Firewalling). Not a good option on the
# internet where IP address may be used to identify the user and
# not only the approximate location.
ecs_use_precise_ip = false

# If DOH is used for a controlled network, it is possible to enable
# the client TLS certificate validation with a specific certificate
# authority used to sign any client one. Disabled by default.
# tls_client_auth = true
# tls_client_auth_ca = "root-ca-public.crt"

EOF


if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor; fi
if [ ! -f /etc/supervisor/program:godohserver.args ]; then
echo "command = /etc/godohserver/godohserver -conf /etc/godohserver/doh-server.conf " > /etc/supervisor/program:godohserver.args
fi

echo "Done Installing DoH Server... please use supervisor or run manually"
