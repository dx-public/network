#!/bin/env sh

whichalt(){
file=$1
found=0
#pathdir="$PATH"
#if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"; fi
pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"
for p in $(echo "$pathdir" | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found" = 0 ]; then echo "$file not found"; return 1; fi
}

dohparse(){
domain=$1
type=$2
if [ -z "$type" ]; then type='A'; fi 
curl=$(whichalt curl)
data=$($curl "https://8.8.8.8/resolve?type=$type&name=$domain" 2>/dev/null)
if [ -z "$data" ]; then data=$($curl "https://[2001:4860:4860::8888]/resolve?type=$type&name=$domain" 2>/dev/null); fi
parse=$(echo $data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print $1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[$0]++')
if [ "$type"=='A' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type"=='AAAA' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[$0]++' | awk '/./'; fi
#if [ ! -z "$parse" ]; then echo $parse; fi
}


install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


echo "Installing Wireguard"

#net.ipv4.ip_forward=1
#net.ipv6.conf.all.forwarding=1
#sysctl -p

if [ ! -d /etc/wireguard ]; then mkdir -p /etc/wireguard; chmod +w /etc/wireguard; fi
if [ ! -d /var/log/wireguard ]; then mkdir -p /var/log/wireguard; chmod +w /var/log/wireguard; fi

if [ ! `which wireguard-go` ]; then 
_BINARY=`curl -s https://api.github.com/repos/P3TERX/wireguard-go-builder/releases/latest |  awk '/download_url/ && /linux-amd64/ && ! /sha/ { print $2 }' || echo 'https://github.com/P3TERX/wireguard-go-builder/releases/download/0.0.20230223/wireguard-go-linux-amd64.tar.gz'`
curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/wireguard-go.tar.gz
tar xvf /tmp/wireguard-go.tar.gz
cp wireguard-go /etc/wireguard/.
chmod +x /etc/wireguard/wireguard-go
#curl -s https://raw.githubusercontent.com/P3TERX/script/master/wireguard-go.sh | bash
touch /etc/wireguard/wg0.conf
chmod 640 /etc/wireguard/wg0.conf
fi

#if [ `which wireguard-vanity-keygen` ]; then 
#_BINARY=`curl -s https://api.github.com/repos/axllent/wireguard-vanity-keygen/releases/latest |  awk '/download_url/ && /linux_amd64/ && ! /sha/ { print $2 }' || echo 'https://github.com/axllent/wireguard-vanity-keygen/releases/download/0.0.4/wireguard-vanity-keygen_0.0.4_linux_amd64.bz2'`
#curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/wireguard-vanity-keygen.bz2
#bzip2 -d /tmp/wireguard-vanity-keygen.bz2
#cp wireguard-vanity-keygen /usr/bin/.
#fi

if [ ! `which gowgkeygen` ]; then 
_BINARY=`install_package gowgkeygen || echo 'https://bafybeidkk5omcni2xjyjpadixvg7fzy7xi3dpmum54n7exnzlod4eywiw4.ipfs.w3s.link/gowgkeygen_v0.0.4_linux_amd64'`
curl -sL $(echo $_BINARY | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}') > /tmp/gowgkeygen
cp /tmp/gowgkeygen /etc/wireguard/.
chmod +x /etc/wireguard/gowgkeygen
fi



#if [ ! -f '/etc/wireguard/wg0.conf' ]; then


randomhex=$(awk -v len=1 'BEGIN { srand('$RANDOM'); while(len--) { n=int(rand()*16); printf("%c", n+(n>9 ? 55 : 48));};}')
randomsd=$(hostname -f | awk 'BEGIN{ FS=""}{for (i=0; i<=1; ++i) {v=(int (rand()*6)+2)}; print v }')

masterdomain=$(hostname -f | awk -F. 'BEGIN{OFS=FS} {sub(".*" $1 FS,"")}1')
networkid=$(curl -s "https://8.8.8.8/resolve?name=wireguard-networkid.${masterdomain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')

if [ -z "$networkid" ]; then 
networkid="255"
fi

if [ -f '/etc/wireguard/networkhostendip' ]; then
echo "host id is: $(awk '{print}' /etc/wireguard/networkhostendip)"
fi

if [ ! -f '/etc/wireguard/networkhostendip' ]; then
hostid=$(echo $randomsd | tee /etc/wireguard/networkhostendip)
fi

if [ ! -f '/etc/wireguard/privpublic.pair' ]; then
#$(gowgkeygen $(hostname -f | awk '$0 > 0 { print substr($0,1,1)}') | awk '/private/' > /etc/wireguard/privpublic.pair)
hostnamenum=$( awk -F. 'BEGIN { OFS="." } { gsub(/[a-zA-Z]/,"",$1); print $1}' /etc/hostname)
echo "Generating key for server number $hostnamenum"
$(/etc/wireguard/gowgkeygen $( echo "$hostnamenum" ) | awk '/private/' > /etc/wireguard/privpublic.pair)
fi

hostid=$(awk '{print}' /etc/wireguard/networkhostendip)
#openssl rand -base64 32
preshared=$(curl -s "https://8.8.8.8/resolve?name=wireguard-psk.${masterdomain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
privatekey=$(awk '{print $2}' /etc/wireguard/privpublic.pair)
peerpublic=$(awk '{print $4}' /etc/wireguard/privpublic.pair | tee /etc/wireguard/publickey.txt)

echo "Public Key for this instance is: $peerpublic"

cat > /etc/wireguard/wg0.conf <<_EOF_
[Interface]
Address    = 10.$networkid.0.$hostid/32, fd00:$networkid::$hostid/48
PrivateKey = $privatekey
#PublicKey = $peerpublic
ListenPort = 51820
#Table     = off

#[Peer]
#PublicKey    = <public key of peer>
#PresharedKey = <PSK of all clients> \$(openssl rand -base64 32)
#AllowedIPs   = 0.0.0.0/0, ::/0

#Endpoint     = <Endpoint DNS or IP of peer>:51820
#PersistentKeepalive = 30

_EOF_
#fi

endpointdnses=$(curl -s "https://8.8.8.8/resolve?name=wireguard-networks.${masterdomain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')

for endpointdns in ${endpointdnses}
do
echo "creating peer $endpointdns"
#echo "getting peer public key...$(peerpublic=$(curl -s https://${endpointdns}/wg-publickey); echo $peerpublic)"

echo "[Peer]" >> /etc/wireguard/wg0.conf
echo "PublicKey = $peerpublic" >> /etc/wireguard/wg0.conf
echo "PresharedKey = $preshared" >> /etc/wireguard/wg0.conf
echo "#AllowedIPs = 0.0.0.0/0, ::/0" >> /etc/wireguard/wg0.conf
echo "" >> /etc/wireguard/wg0.conf
echo "Endpoint = $endpointdns" >> /etc/wireguard/wg0.conf
echo "PersistentKeepalive = 30" >> /etc/wireguard/wg0.conf
echo "" >> /etc/wireguard/wg0.conf
done

if [ "$(whichalt iptables)" = "iptables not found" ]; then
echo "iptables not found installing iptables"
apt update
apt -y install iptables
fi

if [ "$(whichalt ip)" = "ip not found" ]; then
echo "ip not found installing iproute2"
apt update
apt -y install iproute2
fi

if [ "$(whichalt wg-quick)" = "wg-quick not found" ]; then
echo "wg-quick not found installing wireguard-tools"
apt update
apt -y install wireguard-tools
fi

if [ "$(whichalt birdc)" = "birdc not found" ]; then
echo "birdc not found installing bird2"
apt update
apt -y install bird2
mkdir /run/bird
fi