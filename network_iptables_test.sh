#!/bin/env bash

echo "Configuring IPTables"

#echo "$(awk '{gsub(/\r/, ""); print}' network_iptables_test.sh)" > network_iptables_test.sh

whichalt(){
file=$1
found=0
#pathdir="$PATH"
#if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"; fi
pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"
for p in $(echo "$pathdir" | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found" = 0 ]; then echo "$file not found"; return 1; fi
}

dohparse(){
domain=$1
type=$2
if [ -z "$type" ]; then type='A'; fi
curl=$(whichalt curl)
data=""
data=$($curl "https://8.8.8.8/resolve?type=$type&name=$domain" 2>/dev/null)
if [ -z "$data" ]; then data=$($curl "https://[2001:4860:4860::8888]/resolve?type=$type&name=$domain" 2>/dev/null); fi
parse=$(echo $data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print $1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[$0]++')
if [ "$type" = 'A' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type" = 'AAAA' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type" = 'TXT' ]; then echo "$parse"; fi
#if [ ! -z "$parse" ]; then echo $parse; fi
}

check_installed(){
pkg=$1
if [ -z "$pkg" ]; then echo "please pass pkg"; exit; fi
checkpkg="$(whichalt $pkg)"

#if whichalt $pkg >/dev/null; then
if [ "$checkpkg" = "$pkg not found" ]; then
 echo "installing $pkg"
 apt -y install $pkg
else
 echo "$pkg exists"
fi

if [ -z "$checkpkg" ]; then echo "please install $pkg manually"; exit; fi

}


cache_file()
{
  if [ "${arg4}" = "debug" ]; then echo "cache_file function was called as : $@"; fi
  #default settings
  if [ ! -z "$1" ]; then arg1="$1"; else arg1='example.invalid:443'; fi #default to null url
  if [ ! -z "$2" ]; then arg2="$2"; else arg2='/dev/null'; fi #default to null file
  if [ ! -z "$3" ]; then arg3="$3"; else arg3='43200'; fi #default 1day in secs 11days is 950400
  if [ ! -z "$4" ]; then arg4="$4"; else arg4='variable'; fi #default variable (console, debug, variable, file)

  if [ "${arg4}" = "debug" ]; then 
   echo "url: $arg1";
   echo "file: $arg2"; 
   echo "oldsecs: $arg3";
   echo "output: $arg4"; 
  fi

  if [ -z "$(echo "$arg1" | awk '/^[h|H][t|T][t|T][p|P]/ {print}')" ]; then echo ''; return; fi
  
  #not a valid http(s) url

  emptyfile=''
  directoryarg2=$(echo $arg2 | awk -F'/[^/]*$' '{print $1}')

  if [ -f "$arg2" ] && [ -s "$arg2" ]; then
    emptyfile=no
    if [ "${arg4}" = "debug" ]; then echo "file exists and has data"; fi
    if [ "$(( $(date +"%s") - $(date -r ${arg2} +%s) ))" -gt "$arg3" ]; then
     if [ ${arg5} = "debug" ] || [ ${arg5} = "file" ]; then echo "${arg2} file is older then ${arg3} seconds.. redownloading from ${arg1}"; fi
      #check if online return if offline
      curlbin=$(whichalt curl)
      if ! whichalt curl >/dev/null; then apt install -y curl; fi
      onlineserver=$($curlbin -sI $arg1)
      if [ -z "$onlineserver" ]; then return; fi
      rm ${arg2}
      curlcmd=`$(whichalt curl) "${arg1}" > ${arg2}`
    fi
  fi

  if [ ! -f "${arg2}" ] || [ -z ${emptyfile} ]; then
   if [ ${arg4} = "debug" ] || [ ${arg4} = "file" ]; then echo "${arg2} file does not exist or empty... downloading from ${arg1}"; fi
    #check if online return if offline
    curlbin=$(whichalt curl)
    if ! whichalt curl >/dev/null; then apt install -y curl; fi
    onlineserver=$($curlbin -sI $arg1)
    if [ -z "$onlineserver" ]; then return; fi
    curlcmd=`$(whichalt curl) "${arg1}" > ${arg2}`
  fi

  if grep -i -q -m 1 '^@' "${arg2}" 2>/dev/null; then arg5='file'; else arg4='variable'; fi

  if [ "${arg4}" = "variable" ]; then cat ${arg2}; fi
  if [ "${arg4}" = "file"     ]; then echo "Binary saved to ${arg2}"; fi
  return
}




check_installed iptables
iptables=$(whichalt iptables)

check_installed ip6tables
ip6tables=$(whichalt ip6tables)

check_installed ipset
ipset=$(whichalt ipset)

check_installed curl
curl=$(whichalt curl)

check_installed jq
jq=$(whichalt jq)

echo "load netfilter"
modprobe br_netfilter

echo "fix sysctl"
sysctl net.ipv4.conf.all.rp_filter=1
sysctl -n net.ipv4.conf.all.rp_filter

# Accept all traffic first to avoid ssh lockdown  via iptables firewall rules #
$iptables -P INPUT ACCEPT
$iptables -P FORWARD ACCEPT
$iptables -P OUTPUT ACCEPT

#Flush Chain(s)
echo "Flushing Chains"
$iptables -F INPUT
$iptables -F OUTPUT

#Setting up defaults
echo "Setting up Defaults"
$iptables -A INPUT -i lo -j ACCEPT
$iptables -A OUTPUT -o lo -j ACCEPT

#Docker 
$iptables -A INPUT -i docker0 -j ACCEPT
$iptables -A OUTPUT -o docker0 -j ACCEPT

#Accept valid traffic
echo "Accept valid traffic"
$iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
$iptables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

echo "Block Portscans"
$iptables -A INPUT -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP
$iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
$iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
$iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
$iptables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP

echo "Block INVALIDS"
# Drop all invalid packets
$iptables -A INPUT -m state --state INVALID -j DROP
#$iptables -A FORWARD -m state --state INVALID -j DROP
$iptables -A OUTPUT -m state --state INVALID -j DROP

# Stop smurf attacks
echo "Stop Smurf attacks"
$iptables -A INPUT -p icmp -m icmp --icmp-type address-mask-request -j DROP
$iptables -A INPUT -p icmp -m icmp --icmp-type timestamp-request -j DROP

# Reject spoofed packets
echo "Reject spoofed packets"
#$iptables -A INPUT -s 169.254.0.0/16 -j DROP
$iptables -A INPUT -s 224.0.0.0/4 -j DROP
$iptables -A INPUT -d 224.0.0.0/4 -j DROP
$iptables -A INPUT -s 240.0.0.0/5 -j DROP
$iptables -A INPUT -d 240.0.0.0/5 -j DROP
$iptables -A INPUT -s 0.0.0.0/8 -j DROP
$iptables -A INPUT -d 0.0.0.0/8 -j DROP
$iptables -A INPUT -d 239.255.255.0/24 -j DROP
$iptables -A INPUT -d 255.255.255.255 -j DROP


echo "Removing v4 IPSets"
$ipset list -o save | awk '/family inet / { print $2 }' | xargs -n 1 $ipset destroy

## Check that a set exists in bash:
#$ipset -L dynamic_v4_ip_denied >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv4 set does not exist."
#$ipset -N dynamic_v4_ip_denied nethash timeout 2147483
#$iptables -I INPUT -m set --match-set dynamic_v4_ip_denied src -j DROP
#fi

## Check that a set exists in bash:
#$ipset -L dynamic_v4_ip_accept >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv4 set does not exist."
#$ipset -N dynamic_v4_ip_accept nethash timeout 2147483
#$iptables -I INPUT -m set --match-set dynamic_v4_ip_accept src -j ACCEPT
#fi

## Check that a set exists in bash:
#$ipset -L dynamic_v4_ssh_accept >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv4 set does not exist."
#$ipset -N dynamic_v4_ssh_accept nethash timeout 2147483
#$iptables -I INPUT -m set --match-set dynamic_v4_ssh_accept src -p tcp --dport 22 -j ACCEPT
#fi

create_ipset() 
{
if [ -z $2 ]; then echo "please enter ipset name"; return; fi
if [ "$3" = "ipv4" ]; then family="family inet"; else family="family inet6"; fi
if [ ! -z $4 ]; then timeout="timeout $4"; else timeout=""; fi
ipset=$1
$ipset -L $2 >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "ipset $2 set does not exist. creating $2"
$ipset -N $2 nethash $timeout $family
fi
}

create_iptables()
{
  if [ -z $2 ]; then echo "please enter iptables name"; return; fi
  iptables=$1
  $iptables -L $2 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
   echo "creating iptables name: $2"
   $iptables -N $2
  fi
  $iptables -F $2
}

create_ip6tables()
{
  if [ -z $2 ]; then echo "please enter ip6tables name"; return; fi
  ip6tables=$1
  $ip6tables -L $2 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
   echo "creating ip6tables name: $2"
   $ip6tables -N $2
  fi
  $ip6tables -F $2
}

##Add Private
#$ipset -A dynamic_v4_ip_accept 10.0.0.0/8
#$ipset -A dynamic_v4_ip_accept 172.16.0.0/12
#$ipset -A dynamic_v4_ip_accept 192.168.0.0/16

#ALL IPV4
echo "Setting up IPv4 ALL IPs and Ports Protection"
create_ipset $ipset allallipv4safe ipv4
create_iptables $iptables allallipv4safe
$iptables -I INPUT -m set --match-set allallipv4safe src -m conntrack --ctstate NEW -j allallipv4safe
$iptables -A allallipv4safe -m state --state NEW -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name allallipv4limit -j ACCEPT
$iptables -A allallipv4safe -m state --state NEW -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name allallipv4limit -j DROP
$iptables -A allallipv4safe -j DROP
create_ipset $ipset allallipv4safetemp ipv4 2147483
create_iptables $iptables allallipv4safetemp
$iptables -I INPUT -m set --match-set allallipv4safetemp src -m conntrack --ctstate NEW -j allallipv4safetemp
$iptables -A allallipv4safetemp -m state --state NEW -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name allallipv4limit -j ACCEPT
$iptables -A allallipv4safetemp -m state --state NEW -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name allallipv4limit -j DROP
$iptables -A allallipv4safetemp -j DROP
create_ipset $ipset allallipv4block ipv4
create_iptables $iptables allallipv4block
$iptables -I INPUT -m set --match-set allallipv4block src -j allallipv4block
$iptables -A allallipv4block -j DROP
create_ipset $ipset allallipv4blocktemp ipv4 2147483
create_iptables $iptables allallipv4blocktemp
$iptables -I INPUT -m set --match-set allallipv4blocktemp src -j allallipv4blocktemp
$iptables -A allallipv4blocktemp -j DROP
#$ipset -A allallipv4safe 10.0.0.0/8
#$ipset -A allallipv4safe 172.16.0.0/12
#$ipset -A allallipv4safe 192.168.0.0/16



#ICMP
echo "Setting up IPv4 ICMP Protection"
create_ipset $ipset 8icmpipv4safe ipv4
create_iptables $iptables 8icmpipv4safe
$iptables -I INPUT -m set --match-set 8icmpipv4safe src -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j 8icmpipv4safe
$iptables -A 8icmpipv4safe -m state --state NEW -m icmp -p icmp --icmp-type 8 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name icmplimit -j ACCEPT
$iptables -A 8icmpipv4safe -m state --state NEW -m icmp -p icmp --icmp-type 8 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name icmplimit -j DROP
$iptables -A 8icmpipv4safe -j DROP
create_ipset $ipset 8icmpipv4safetemp ipv4 2147483
create_iptables $iptables 8icmpipv4safetemp
$iptables -I INPUT -m set --match-set 8icmpipv4safetemp src -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j 8icmpipv4safetemp
$iptables -A 8icmpipv4safetemp -m state --state NEW -m icmp -p icmp --icmp-type 8 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name icmplimit -j ACCEPT
$iptables -A 8icmpipv4safetemp -m state --state NEW -m icmp -p icmp --icmp-type 8 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name icmplimit -j DROP
$iptables -A 8icmpipv4safetemp -j DROP
create_ipset $ipset 8icmpipv4block ipv4
create_iptables $iptables 8icmpipv4block
$iptables -I INPUT -m set --match-set 8icmpipv4block src -p icmp --icmp-type 8 -j 8icmpipv4block
$iptables -A 8icmpipv4block -j DROP
create_ipset $ipset 8icmpipv4blocktemp ipv4 2147483
create_iptables $iptables 8icmpipv4blocktemp
$iptables -I INPUT -m set --match-set 8icmpipv4blocktemp src -p icmp --icmp-type 8 -j 8icmpipv4blocktemp
$iptables -A 8icmpipv4blocktemp -j DROP
$ipset -A 8icmpipv4safe 10.0.0.0/8
$ipset -A 8icmpipv4safe 172.16.0.0/12
$ipset -A 8icmpipv4safe 192.168.0.0/16
$ipset -A 8icmpipv4safe 0.0.0.0/1
$ipset -A 8icmpipv4safe 128.0.0.0/1

#SSH
echo "Setting up IPv4 SSH Protection"
create_ipset $ipset 22tcpipv4safe ipv4
create_iptables $iptables 22tcpipv4safe
$iptables -I INPUT -m set --match-set 22tcpipv4safe src -p tcp --dport 22 -m conntrack --ctstate NEW -j 22tcpipv4safe
$iptables -A 22tcpipv4safe -p tcp --dport 22 -m state --state NEW -m recent --name SSH4 --update --seconds 86500 --hitcount 1 -j DROP
$iptables -A 22tcpipv4safe -p tcp --dport 22 -m state --state NEW -m recent --set --name SSH4 -j ACCEPT
$iptables -A 22tcpipv4safe -j DROP
create_ipset $ipset 22tcpipv4safetemp ipv4 2147483
create_iptables $iptables 22tcpipv4safetemp
$iptables -I INPUT -m set --match-set 22tcpipv4safetemp src -p tcp --dport 22 -m conntrack --ctstate NEW -j 22tcpipv4safetemp
$iptables -A 22tcpipv4safetemp -p tcp --dport 22 -m state --state NEW -m recent --name SSH4 --update --seconds 86500 --hitcount 1 -j DROP
$iptables -A 22tcpipv4safetemp -p tcp --dport 22 -m state --state NEW -m recent --set --name SSH4 -j ACCEPT
$iptables -A 22tcpipv4safetemp -j DROP
create_ipset $ipset 22tcpipv4block ipv4
create_iptables $iptables 22tcpipv4block
$iptables -I INPUT -m set --match-set 22tcpipv4block src -p tcp --dport 22 -j 22tcpipv4block
$iptables -A 22tcpipv4block -j DROP
create_ipset $ipset 22tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 22tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 22tcpipv4blocktemp src -p tcp --dport 22 -j 22tcpipv4blocktemp
$iptables -A 22tcpipv4blocktemp -j DROP
$ipset -A 22tcpipv4safe 10.0.0.0/8
$ipset -A 22tcpipv4safe 172.16.0.0/12
$ipset -A 22tcpipv4safe 192.168.0.0/16

#SMTP
echo "Setting up IPv4 SMTP Protection"
create_ipset $ipset 25tcpipv4safe ipv4
create_iptables $iptables 25tcpipv4safe
$iptables -I INPUT -m set --match-set 25tcpipv4safe src -p tcp --dport 25 -m conntrack --ctstate NEW -j 25tcpipv4safe
$iptables -A 25tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptcplimit -j ACCEPT
$iptables -A 25tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptcplimit -j DROP
$iptables -A 25tcpipv4safe -j DROP
create_ipset $ipset 25tcpipv4safetemp ipv4 2147483
create_iptables $iptables 25tcpipv4safetemp
$iptables -I INPUT -m set --match-set 25tcpipv4safetemp src -p tcp --dport 25 -m conntrack --ctstate NEW -j 25tcpipv4safetemp
$iptables -A 25tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptcplimit -j ACCEPT
$iptables -A 25tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptcplimit -j DROP
$iptables -A 25tcpipv4safetemp -j DROP
create_ipset $ipset 25tcpipv4block ipv4
create_iptables $iptables 25tcpipv4block
$iptables -I INPUT -m set --match-set 25tcpipv4block src -p tcp --dport 25 -j 25tcpipv4block
$iptables -A 25tcpipv4block -j DROP
create_ipset $ipset 25tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 25tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 25tcpipv4blocktemp src -p tcp --dport 25 -j 25tcpipv4blocktemp
$iptables -A 25tcpipv4blocktemp -j DROP
$ipset -A 25tcpipv4safe 10.0.0.0/8
$ipset -A 25tcpipv4safe 172.16.0.0/12
$ipset -A 25tcpipv4safe 192.168.0.0/16

#DNS
echo "Setting up IPv4 DNS Protection"
create_ipset $ipset 53tcpipv4safe ipv4
create_iptables $iptables 53tcpipv4safe
$iptables -I INPUT -m set --match-set 53tcpipv4safe src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53tcpipv4safe
$iptables -A 53tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j ACCEPT
$iptables -A 53tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j DROP
$iptables -A 53tcpipv4safe -j DROP
create_ipset $ipset 53tcpipv4safetemp ipv4 2147483
create_iptables $iptables 53tcpipv4safetemp
$iptables -I INPUT -m set --match-set 53tcpipv4safetemp src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53tcpipv4safetemp
$iptables -A 53tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j ACCEPT
$iptables -A 53tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j DROP
$iptables -A 53tcpipv4safetemp -j DROP
create_ipset $ipset 53tcpipv4block ipv4
create_iptables $iptables 53tcpipv4block
$iptables -I INPUT -m set --match-set 53tcpipv4block src -p tcp --dport 53 -j 53tcpipv4block
$iptables -A 53tcpipv4block -j DROP
create_ipset $ipset 53tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 53tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 53tcpipv4blocktemp src -p tcp --dport 53 -j 53tcpipv4blocktemp
$iptables -A 53tcpipv4blocktemp -j DROP
$ipset -A 53tcpipv4safe 10.0.0.0/8
$ipset -A 53tcpipv4safe 172.16.0.0/12
$ipset -A 53tcpipv4safe 192.168.0.0/16

create_ipset $ipset 53udpipv4safe ipv4
create_iptables $iptables 53udpipv4safe
$iptables -I INPUT -m set --match-set 53udpipv4safe src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53udpipv4safe
$iptables -A 53udpipv4safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j ACCEPT
$iptables -A 53udpipv4safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j DROP
$iptables -A 53udpipv4safe -j DROP
create_ipset $ipset 53udpipv4safetemp ipv4 2147483
create_iptables $iptables 53udpipv4safetemp
$iptables -I INPUT -m set --match-set 53udpipv4safetemp src -p udp --dport 53 -m conntrack --ctstate NEW -j 53udpipv4safetemp
$iptables -A 53udpipv4safetemp -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j ACCEPT
$iptables -A 53udpipv4safetemp -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j DROP
$iptables -A 53udpipv4safetemp -j DROP
create_ipset $ipset 53udpipv4block ipv4
create_iptables $iptables 53udpipv4block
$iptables -I INPUT -m set --match-set 53udpipv4block src -p tcp --dport 53 -j 53udpipv4block
$iptables -A 53udpipv4block -j DROP
create_ipset $ipset 53udpipv4blocktemp ipv4 2147483
create_iptables $iptables 53udpipv4blocktemp
$iptables -I INPUT -m set --match-set 53udpipv4blocktemp src -p tcp --dport 53 -j 53udpipv4blocktemp
$iptables -A 53udpipv4blocktemp -j DROP
$ipset -A 53udpipv4safe 10.0.0.0/8
$ipset -A 53udpipv4safe 172.16.0.0/12
$ipset -A 53udpipv4safe 192.168.0.0/16

#Web HTTP
echo "Setting up IPv4 WEB HTTP Protection"
create_ipset $ipset 80tcpipv4safe ipv4
create_iptables $iptables 80tcpipv4safe
$iptables -I INPUT -m set --match-set 80tcpipv4safe src -p tcp --dport 80 -m conntrack --ctstate NEW -j 80tcpipv4safe
$iptables -A 80tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httptcplimit -j ACCEPT
$iptables -A 80tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httptcplimit -j DROP
$iptables -A 80tcpipv4safe -j DROP
create_ipset $ipset 80tcpipv4safetemp ipv4 2147483
create_iptables $iptables 80tcpipv4safetemp
$iptables -I INPUT -m set --match-set 80tcpipv4safetemp src -p tcp --dport 80 -m conntrack --ctstate NEW -j 80tcpipv4safetemp
$iptables -A 80tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httptcplimit -j ACCEPT
$iptables -A 80tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httptcplimit -j DROP
$iptables -A 80tcpipv4safetemp -j DROP
create_ipset $ipset 80tcpipv4block ipv4
create_iptables $iptables 80tcpipv4block
$iptables -I INPUT -m set --match-set 80tcpipv4block src -p tcp --dport 80 -j 80tcpipv4block
$iptables -A 80tcpipv4block -j DROP
create_ipset $ipset 80tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 80tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 80tcpipv4blocktemp src -p tcp --dport 80 -j 80tcpipv4blocktemp
$iptables -A 80tcpipv4blocktemp -j DROP
$ipset -A 80tcpipv4safe 10.0.0.0/8
$ipset -A 80tcpipv4safe 172.16.0.0/12
$ipset -A 80tcpipv4safe 192.168.0.0/16

#NTP
echo "Setting up IPv4 NTP Protection"
create_ipset $ipset 123udpipv4safe ipv4
create_iptables $iptables 123udpipv4safe
$iptables -I INPUT -m set --match-set 123udpipv4safe src -p udp --dport 123 -m conntrack --ctstate NEW -j 123udpipv4safe
$iptables -A 123udpipv4safe -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name ntpudplimit -j ACCEPT
$iptables -A 123udpipv4safe -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name ntpudplimit -j DROP
$iptables -A 123udpipv4safe -j DROP
create_ipset $ipset 123udpipv4safetemp ipv4 2147483
create_iptables $iptables 123udpipv4safetemp
$iptables -I INPUT -m set --match-set 123udpipv4safetemp src -p udp --dport 123 -m conntrack --ctstate NEW -j 123udpipv4safetemp
$iptables -A 123udpipv4safetemp -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name ntpudplimit -j ACCEPT
$iptables -A 123udpipv4safetemp -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name ntpudplimit -j DROP
$iptables -A 123udpipv4safetemp -j DROP
create_ipset $ipset 123udpipv4block ipv4
create_iptables $iptables 123udpipv4block
$iptables -I INPUT -m set --match-set 123udpipv4block src -p udp --dport 123 -j 123udpipv4block
$iptables -A 123udpipv4block -j DROP
create_ipset $ipset 123udpipv4blocktemp ipv4 2147483
create_iptables $iptables 123udpipv4blocktemp
$iptables -I INPUT -m set --match-set 123udpipv4blocktemp src -p udp --dport 80 -j 123udpipv4blocktemp
$iptables -A 123udpipv4blocktemp -j DROP
$ipset -A 123udpipv4safe 10.0.0.0/8
$ipset -A 123udpipv4safe 172.16.0.0/12
$ipset -A 123udpipv4safe 192.168.0.0/16

#BGP
echo "Setting up IPv4 BGP Protection"
create_ipset $ipset 179tcpipv4safe ipv4
create_iptables $iptables 179tcpipv4safe
$iptables -I INPUT -m set --match-set 179tcpipv4safe src -p tcp --dport 179 -m conntrack --ctstate NEW -j 179tcpipv4safe
$iptables -A 179tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name bgptcplimit -j ACCEPT
$iptables -A 179tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name bgptcplimit -j DROP
$iptables -A 179tcpipv4safe -j DROP
create_ipset $ipset 179tcpipv4safetemp ipv4 2147483
create_iptables $iptables 179tcpipv4safetemp
$iptables -I INPUT -m set --match-set 179tcpipv4safetemp src -p tcp --dport 179 -m conntrack --ctstate NEW -j 123udpipv4safetemp
$iptables -A 179tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name bgptcplimit -j ACCEPT
$iptables -A 179tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name bgptcplimit -j DROP
$iptables -A 179tcpipv4safetemp -j DROP
create_ipset $ipset 179tcpipv4block ipv4
create_iptables $iptables 179tcpipv4block
$iptables -I INPUT -m set --match-set 179tcpipv4block src -p udp --dport 123 -j 179tcpipv4block
$iptables -A 179tcpipv4block -j DROP
create_ipset $ipset 179tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 179tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 179tcpipv4blocktemp src -p tcp --dport 80 -j 179tcpipv4blocktemp
$iptables -A 179tcpipv4blocktemp -j DROP
$ipset -A 179tcpipv4safe 10.0.0.0/8
$ipset -A 179tcpipv4safe 172.16.0.0/12
$ipset -A 179tcpipv4safe 192.168.0.0/16
$ipset -A 179tcpipv4safe 169.254.0.0/16

#HTTPS
echo "Setting up IPv4 WEB HTTPS Protection"
create_ipset $ipset 443tcpipv4safe ipv4
create_iptables $iptables 443tcpipv4safe
$iptables -I INPUT -m set --match-set 443tcpipv4safe src -p tcp --dport 443 -m conntrack --ctstate NEW -j 443tcpipv4safe
$iptables -A 443tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j ACCEPT
$iptables -A 443tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j DROP
$iptables -A 443tcpipv4safe -j DROP
create_ipset $ipset 443tcpipv4safetemp ipv4 2147483
create_iptables $iptables 443tcpipv4safetemp
$iptables -I INPUT -m set --match-set 443tcpipv4safetemp src -p tcp --dport 443 -m conntrack --ctstate NEW -j 443tcpipv4safetemp
$iptables -A 443tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j ACCEPT
$iptables -A 443tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j DROP
$iptables -A 443tcpipv4safetemp -j DROP
create_ipset $ipset 443tcpipv4block ipv4
create_iptables $iptables 443tcpipv4block
$iptables -I INPUT -m set --match-set 443tcpipv4block src -p tcp --dport 443 -j 443tcpipv4block
$iptables -A 443tcpipv4block -j DROP
create_ipset $ipset 443tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 443tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 443tcpipv4blocktemp src -p tcp --dport 443 -j 443tcpipv4blocktemp
$iptables -A 443tcpipv4blocktemp -j DROP
$ipset -A 443tcpipv4safe 10.0.0.0/8
$ipset -A 443tcpipv4safe 172.16.0.0/12
$ipset -A 443tcpipv4safe 192.168.0.0/16

#HTTPS-ALT
echo "Setting up IPv4 WEB HTTPS-ALT Protection"
create_ipset $ipset 8443tcpipv4safe ipv4
create_iptables $iptables 8443tcpipv4safe
$iptables -I INPUT -m set --match-set 8443tcpipv4safe src -p tcp --dport 8443 -m conntrack --ctstate NEW -j 8443tcpipv4safe
$iptables -A 8443tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpsalttcplimit -j ACCEPT
$iptables -A 8443tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpsalttcplimit -j DROP
$iptables -A 8443tcpipv4safe -j DROP
create_ipset $ipset 8443tcpipv4safetemp ipv4 2147483
create_iptables $iptables 8443tcpipv4safetemp
$iptables -I INPUT -m set --match-set 8443tcpipv4safetemp src -p tcp --dport 8443 -m conntrack --ctstate NEW -j 8443tcpipv4safetemp
$iptables -A 8443tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpsalttcplimit -j ACCEPT
$iptables -A 8443tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpsalttcplimit -j DROP
$iptables -A 8443tcpipv4safetemp -j DROP
create_ipset $ipset 8443tcpipv4block ipv4
create_iptables $iptables 8443tcpipv4block
$iptables -I INPUT -m set --match-set 8443tcpipv4block src -p tcp --dport 8443 -j 8443tcpipv4block
$iptables -A 8443tcpipv4block -j DROP
create_ipset $ipset 8443tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 8443tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 8443tcpipv4blocktemp src -p tcp --dport 8443 -j 8443tcpipv4blocktemp
$iptables -A 8443tcpipv4blocktemp -j DROP
$ipset -A 8443tcpipv4safe 10.0.0.0/8
$ipset -A 8443tcpipv4safe 172.16.0.0/12
$ipset -A 8443tcpipv4safe 192.168.0.0/16


#SMTPS
echo "Setting up IPv4 SMTPS Protection"
create_ipset $ipset 587tcpipv4safe ipv4
create_iptables $iptables 587tcpipv4safe
$iptables -I INPUT -m set --match-set 587tcpipv4safe src -p tcp --dport 587 -m conntrack --ctstate NEW -j 587tcpipv4safe
$iptables -A 587tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtpstcplimit -j ACCEPT
$iptables -A 587tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtpstcplimit -j DROP
$iptables -A 587tcpipv4safe -j DROP
create_ipset $ipset 587tcpipv4safetemp ipv4 2147483
create_iptables $iptables 587tcpipv4safetemp
$iptables -I INPUT -m set --match-set 587tcpipv4safetemp src -p tcp --dport 587 -m conntrack --ctstate NEW -j 587tcpipv4safetemp
$iptables -A 587tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtpstcplimit -j ACCEPT
$iptables -A 587tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtpstcplimit -j DROP
$iptables -A 587tcpipv4safetemp -j DROP
create_ipset $ipset 587tcpipv4block ipv4
create_iptables $iptables 587tcpipv4block
$iptables -I INPUT -m set --match-set 587tcpipv4block src -p tcp --dport 587 -j 587tcpipv4block
$iptables -A 587tcpipv4block -j DROP
create_ipset $ipset 587tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 587tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 587tcpipv4blocktemp src -p tcp --dport 587 -j 587tcpipv4blocktemp
$iptables -A 587tcpipv4blocktemp -j DROP
$ipset -A 587tcpipv4safe 10.0.0.0/8
$ipset -A 587tcpipv4safe 172.16.0.0/12
$ipset -A 587tcpipv4safe 192.168.0.0/16

#SMTPTLS
echo "Setting up IPv4 SMTP TLS Protection"
create_ipset $ipset 465tcpipv4safe ipv4
create_iptables $iptables 465tcpipv4safe
$iptables -I INPUT -m set --match-set 465tcpipv4safe src -p tcp --dport 465 -m conntrack --ctstate NEW -j 465tcpipv4safe
$iptables -A 465tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptlstcplimit -j ACCEPT
$iptables -A 465tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name smtptlstcplimit -j DROP
$iptables -A 465tcpipv4safe -j DROP
create_ipset $ipset 465tcpipv4safetemp ipv4 2147483
create_iptables $iptables 465tcpipv4safetemp
$iptables -I INPUT -m set --match-set 465tcpipv4safetemp src -p tcp --dport 465 -m conntrack --ctstate NEW -j 465tcpipv4safetemp
$iptables -A 465tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j ACCEPT
$iptables -A 465tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name httpstcplimit -j DROP
$iptables -A 465tcpipv4safetemp -j DROP
create_ipset $ipset 465tcpipv4block ipv4
create_iptables $iptables 465tcpipv4block
$iptables -I INPUT -m set --match-set 465tcpipv4block src -p tcp --dport 465 -j 465tcpipv4block
$iptables -A 465tcpipv4block -j DROP
create_ipset $ipset 465tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 465tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 465tcpipv4blocktemp src -p tcp --dport 465 -j 465tcpipv4blocktemp
$iptables -A 465tcpipv4blocktemp -j DROP
$ipset -A 465tcpipv4safe 10.0.0.0/8
$ipset -A 465tcpipv4safe 172.16.0.0/12
$ipset -A 465tcpipv4safe 192.168.0.0/16

#IMAP
echo "Setting up IPv4 IMAP TLS Protection"
create_ipset $ipset 993tcpipv4safe ipv4
create_iptables $iptables 993tcpipv4safe
$iptables -I INPUT -m set --match-set 993tcpipv4safe src -p tcp --dport 993 -m conntrack --ctstate NEW -j 993tcpipv4safe
$iptables -A 993tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name imapstcplimit -j ACCEPT
$iptables -A 993tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name imapstcplimit -j DROP
$iptables -A 993tcpipv4safe -j DROP
create_ipset $ipset 993tcpipv4safetemp ipv4 2147483
create_iptables $iptables 993tcpipv4safetemp
$iptables -I INPUT -m set --match-set 993tcpipv4safetemp src -p tcp --dport 993 -m conntrack --ctstate NEW -j 993tcpipv4safetemp
$iptables -A 993tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name imapstcplimit -j ACCEPT
$iptables -A 993tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name imapstcplimit -j DROP
$iptables -A 993tcpipv4safetemp -j DROP
create_ipset $ipset 993tcpipv4block ipv4
create_iptables $iptables 993tcpipv4block
$iptables -I INPUT -m set --match-set 993tcpipv4block src -p tcp --dport 993 -j 993tcpipv4block
$iptables -A 993tcpipv4block -j DROP
create_ipset $ipset 993tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 993tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 993tcpipv4blocktemp src -p tcp --dport 993 -j 993tcpipv4blocktemp
$iptables -A 993tcpipv4blocktemp -j DROP
$ipset -A 993tcpipv4safe 10.0.0.0/8
$ipset -A 993tcpipv4safe 172.16.0.0/12
$ipset -A 993tcpipv4safe 192.168.0.0/16


#MISC
echo "Setting up IPv4 Misc Protection"
create_ipset $ipset 4500udpipv4safe ipv4
create_iptables $iptables 4500udpipv4safe
$iptables -I INPUT -m set --match-set 4500udpipv4safe src -p udp --dport 4500 -m conntrack --ctstate NEW -j 4500udpipv4safe
$iptables -A 4500udpipv4safe -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name miscudplimit -j ACCEPT
$iptables -A 4500udpipv4safe -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name miscudplimit -j DROP
$iptables -A 4500udpipv4safe -j DROP
create_ipset $ipset 4500udpipv4safetemp ipv4 2147483
create_iptables $iptables 4500udpipv4safetemp
$iptables -I INPUT -m set --match-set 4500udpipv4safetemp src -p udp --dport 4500 -m conntrack --ctstate NEW -j 4500udpipv4safetemp
$iptables -A 4500udpipv4safetemp -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name miscudplimit -j ACCEPT
$iptables -A 4500udpipv4safetemp -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name miscudplimit -j DROP
$iptables -A 4500udpipv4safetemp -j DROP
create_ipset $ipset 4500udpipv4block ipv4
create_iptables $iptables 4500udpipv4block
$iptables -I INPUT -m set --match-set 4500udpipv4block src -p tcp --dport 4500 -j 4500udpipv4block
$iptables -A 4500udpipv4block -j DROP
create_ipset $ipset 4500udpipv4blocktemp ipv4 2147483
create_iptables $iptables 4500udpipv4blocktemp
$iptables -I INPUT -m set --match-set 4500udpipv4blocktemp src -p tcp --dport 4500 -j 4500udpipv4blocktemp
$iptables -A 4500udpipv4blocktemp -j DROP
$ipset -A 4500udpipv4safe 10.0.0.0/8
$ipset -A 4500udpipv4safe 172.16.0.0/12
$ipset -A 4500udpipv4safe 192.168.0.0/16

#Secure DoT
#create_iptables $iptables DOT-RATE-LIMIT
#$iptables -A DOT-RATE-LIMIT -m hashlimit --hashlimit-mode srcip --hashlimit-upto 30/sec --hashlimit-burst 20 --hashlimit-name dot_conn_rate_limit --jump ACCEPT
#$iptables -A DOT-RATE-LIMIT -m limit --limit 1/sec --jump LOG --log-prefix "IPTables-Ratelimited: "
#$iptables -A DOT-RATE-LIMIT -j DROP
#$iptables -I INPUT -p tcp --dport 853 -m conntrack --ctstate NEW -j DOT-RATE-LIMIT

echo "Setting up IPv4 DOT"
create_ipset $ipset 853tcpipv4safe ipv4
create_iptables $iptables 853tcpipv4safe
$iptables -I INPUT -m set --match-set 853tcpipv4safe src -p tcp --dport 853 -m conntrack --ctstate NEW -j 853tcpipv4safe
$iptables -A 853tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name tlsdnstcplimit -j ACCEPT
$iptables -A 853tcpipv4safe -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name tlsdnstcplimit -j DROP
$iptables -A 853tcpipv4safe -j DROP
create_ipset $ipset 853tcpipv4safetemp ipv4 2147483
create_iptables $iptables 853tcpipv4safetemp
$iptables -I INPUT -m set --match-set 853tcpipv4safetemp src -p tcp --dport 853 -m conntrack --ctstate NEW -j 853tcpipv4safetemp
$iptables -A 853tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name tlsdnstcplimit -j ACCEPT
$iptables -A 853tcpipv4safetemp -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name tlsdnstcplimit -j DROP
$iptables -A 853tcpipv4safetemp -j DROP
create_ipset $ipset 853tcpipv4block ipv4
create_iptables $iptables 853tcpipv4block
$iptables -I INPUT -m set --match-set 853tcpipv4block src -p tcp --dport 853 -j 853tcpipv4block
$iptables -A 853tcpipv4block -j DROP
create_ipset $ipset 853tcpipv4blocktemp ipv4 2147483
create_iptables $iptables 853tcpipv4blocktemp
$iptables -I INPUT -m set --match-set 853tcpipv4blocktemp src -p tcp --dport 853 -j 853tcpipv4blocktemp
$iptables -A 853tcpipv4blocktemp -j DROP
$ipset -A 853tcpipv4safe 0.0.0.0/1
$ipset -A 853tcpipv4safe 128.0.0.0/1


echo "Setting up IPv4 Lists"
#create_iptables $iptables UPTIMEROBOT
#$iptables -A UPTIMEROBOT -p tcp -m multiport --dports 80,443,853,8443 -j ACCEPT
#$iptables -A UPTIMEROBOT -p icmp -m icmp --icmp-type 8 -j ACCEPT

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
echo "0.0.0.0/1" > /etc/configs/iptables/all-ipv4.raw
echo "128.0.0.0/1" >> /etc/configs/iptables/all-ipv4.raw
echo "$(awk '{print}' /etc/configs/iptables/all-ipv4.raw | awk '{gsub(/\r/, ""); print}' | awk '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/')" > /etc/configs/iptables/all-ipv4.txt

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
echo "10.0.0.0/8" > /etc/configs/iptables/local-ipv4.raw
echo "172.16.0.0/12" >> /etc/configs/iptables/local-ipv4.raw
echo "192.168.0.0/16" >> /etc/configs/iptables/local-ipv4.raw
echo "$(awk '{print}' /etc/configs/iptables/local-ipv4.raw | awk '{gsub(/\r/, ""); print}' | awk '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/')" > /etc/configs/iptables/local-ipv4.txt


if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
##cache_file "https://uptimerobot.com/inc/files/ips/IPv4.txt" /etc/configs/iptables/uptimerobot-ips-v4.raw 950400
cache_file "https://uptimerobot.com/inc/files/ips/IPv4andIPv6.txt" /etc/configs/iptables/uptimerobot-ipv4.raw 950400
echo "$(awk '{print}' /etc/configs/iptables/uptimerobot-ipv4.raw | awk '{gsub(/\r/, ""); print}' | awk '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/')" > /etc/configs/iptables/uptimerobot-ipv4.txt
#for x in $(cat /etc/configs/iptables/uptimerobot-ipv4.txt); do $iptables -A INPUT -s $(echo "${x}/32") -j UPTIMEROBOT ; done

#create_iptables $iptables CLOUDFLARE
#$iptables -A CLOUDFLARE -p tcp -m multiport --dports 80,443,8443 -j ACCEPT

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
cache_file "https://api.cloudflare.com/client/v4/ips" /etc/configs/iptables/cloudflare-ipv4.raw 950400
echo "$(awk '{print}' /etc/configs/iptables/cloudflare-ipv4.raw | $jq -r '.result.ipv4_cidrs | .[]' | awk '{gsub(/\r/, ""); print}' )" > /etc/configs/iptables/cloudflare-ipv4.txt
#for x in $(cat /etc/configs/iptables/cloudflare-ipv4.txt); do $iptables -A INPUT -s $(echo ${x}) -j CLOUDFLARE ; done

#domain wide rules
domainsuffix="$(hostname -d)"
dohparse cfg-iptables.$domainsuffix TXT > /etc/configs/iptables/rules-iptables.txt
for x in $(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-iptables.txt)
do
ipsetlist=$(echo "$x" | awk -F- '{print $1$2$3$4}')
ipsetfile=$(echo "$x" | awk -F- '{print $5"-"$3""}' )
echo "ipsetlist: $ipsetlist domain file: ${ipsetfile}.txt"
if [ -f /etc/configs/iptables/${ipsetfile}.txt ]; then 
    echo "${ipsetfile}.txt domain file found and importing ips";
else
    dnsipsetfile="$ipsetfile.cfg-iptables.$domainsuffix"
    echo "file not found getting domain custom list via dns: $dnsipsetfile"
    urlhostdownload=$(dohparse $dnsipsetfile TXT)
    cache_file "$urlhostdownload" /etc/configs/iptables/${ipsetfile}.raw 950400
    echo "$(awk '{print}' /etc/configs/iptables/${ipsetfile}.raw | awk '{gsub(/\r/, ""); print}' | awk '{gsub(/"|,|[|]|{|}| /, "\n"); print}' | awk '/[0-9]/' | awk '!/[a-zA-Z]/' | awk '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/')" > /etc/configs/iptables/${ipsetfile}.txt
fi
  for y in $(awk '{print}' /etc/configs/iptables/${ipsetfile}.txt)
  do
      echo "importing ip: $y to ipset ${ipsetlist}.txt"
      $ipset -A $ipsetlist $y 2>/dev/null
  done
done

#host wide rules
hostsuffix="$(hostname -f)"
dohparse cfg-iptables.$hostsuffix TXT > /etc/configs/iptables/rules-host-iptables.txt
for x in $(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-host-iptables.txt)
do
ipsetlist=$(echo "$x" | awk -F- '{print $1$2$3$4}')
ipsetfile=$(echo "$x" | awk -F- '{print $5"-"$3""}') #($5)cloudflare-($3)ipv4 | ($5)custom-($3)ipv4
echo "ipsetlist: $ipsetlist host file: ${ipsetfile}.txt"
if [ -f /etc/configs/iptables/${ipsetfile}.txt ]; then 
    echo "${ipsetfile}.txt host file found and importing ips";
else
    dnsipsetfile="$ipsetfile.cfg-iptables.$hostsuffix"
    echo "file not found getting custom host list via dns: $dnsipsetfile"
    urlhostdownload=$(dohparse $dnsipsetfile TXT)
    cache_file "$urlhostdownload" /etc/configs/iptables/${ipsetfile}.raw 950400
    echo "$(awk '{print}' /etc/configs/iptables/${ipsetfile}.raw | awk '{gsub(/\r/, ""); print}' | awk '{gsub(/"|,|[|]|{|}| /, "\n"); print}' | awk '/[0-9]/' | awk '!/[a-zA-Z]/' | awk '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/')" > /etc/configs/iptables/${ipsetfile}.txt
fi
  for y in $(awk '{print}' /etc/configs/iptables/${ipsetfile}.txt)
  do
      echo "importing ip: $y to ipset ${ipsetlist}.txt"
      $ipset -A $ipsetlist $y 2>/dev/null
  done
done


$iptables -A INPUT -j LOG --log-prefix "iptablesinputdrop" --log-level 6
$iptables -A INPUT -j DROP
echo "Done setting up IPv4 Lists"


echo "Flushing all the ipv6 tables"
# Accept all traffic first to avoid ssh lockdown via iptables firewall rules #
$ip6tables -P INPUT ACCEPT
$ip6tables -P FORWARD ACCEPT
$ip6tables -P OUTPUT ACCEPT

# Flush All Iptables Chains/Firewall rules #
$ip6tables -F INPUT
$ip6tables -F OUTPUT

echo "Removing v6 IPSets"
$ipset list -o save | awk '/family inet6 / { print $2 }' | xargs -n 1 $ipset destroy

ipn6=0
opn6=0
fwn6=0

#Fixes
#Accept loopback interface
#$ip6tables -I INPUT $(($ipn6 + 1)) -i lo -j ACCEPT

#Defaults
$ip6tables -I INPUT $(($ipn6 + 1))  -i lo -j ACCEPT
$ip6tables -I OUTPUT $(($ipn6 + 1))  -o lo -j ACCEPT

$ip6tables -I INPUT $(($ipn6 + 1))  -i docker0 -j ACCEPT
$ip6tables -I OUTPUT $(($ipn6 + 1))  -o docker0 -j ACCEPT

#NEW Related
$ip6tables -I INPUT $(($ipn6 + 1)) -m state --state RELATED,ESTABLISHED -j ACCEPT
$ip6tables -I OUTPUT $(($opn6 + 1)) -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

##Accept IPv6 Traffic via ICMP
#$ip6tables -I INPUT $(($ipn6 + 1)) -p ipv6-icmp -j ACCEPT

#Accept DHCPv6 from LAN only
$ip6tables -I INPUT $(($ipn6 + 1)) -m udp -p udp -s fe80::/10 --sport 547 -d fe80::/10 --dport 546 -j ACCEPT

#Accept MULTICAST mDNS for service discovery
#$ip6tables -I INPUT $(($ipn6 + 1)) -p udp -d ff02::fb --dport 5353 -j ACCEPT

#Accept MULTICAST UPnP for service discovery
#$ip6tables -I INPUT $(($ipn6 + 1)) -p udp -d ff02::f --dport 1900 -j ACCEPT

echo "Block Portscans"
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags ALL NONE -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags ACK,FIN FIN -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags ACK,PSH PSH -j DROP
$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --tcp-flags ACK,URG URG -j DROP

echo "Block INVALIDS"
# Drop all invalid packets
$ip6tables -I INPUT $(($ipn6 + 1)) -m state --state INVALID -j DROP
#$ip6tables -I FORWARD $(($fwn6 + 1)) -m state --state INVALID -j DROP
$ip6tables -I OUTPUT $(($opn6 + 1)) -m state --state INVALID -j DROP

#echo "Setting up Dynamic IPv6 Lists"
## Check that a set exists in bash:
#$ipset -L dynamic_v6_ssh_accept >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv6 set does not exist."
#$ipset -N dynamic_v6_ssh_accept nethash timeout 2147483 family inet6
#$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set dynamic_v6_ssh_accept src -p tcp --dport 22 -j ACCEPT
#fi

## Check that a set exists in bash:
#$ipset -L dynamic_v6_ip_accept >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv6 set does not exist."
#$ipset -N dynamic_v6_ip_accept nethash timeout 2147483 family inet6
#$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set dynamic_v6_ip_accept src -j ACCEPT
#fi

## Check that a set exists in bash:
#$ipset -L dynamic_v6_ip_denied >/dev/null 2>&1
#if [ $? -ne 0 ]; then
#echo "Block ipv6 set does not exist."
#$ipset -N dynamic_v6_ip_denied nethash timeout 2147483 family inet6
#$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set dynamic_v6_ip_denied src -j DROP
#fi

#ALL ipv6
echo "Setting up ipv6 ALL IPs and Ports Protection"
create_ipset $ipset allallipv6safe ipv6
create_iptables $ip6tables allallipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set allallipv6safe src -m conntrack --ctstate NEW -j allallipv6safe
$ip6tables -A allallipv6safe -m state --state NEW -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name allallipv6limit -j ACCEPT
$ip6tables -A allallipv6safe -m state --state NEW -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name allallipv6limit -j DROP
$ip6tables -A allallipv6safe -j DROP
create_ipset $ipset allallipv6safetemp ipv6 2147483
create_iptables $ip6tables allallipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set allallipv6safetemp src -m conntrack --ctstate NEW -j allallipv6safetemp
$ip6tables -A allallipv6safetemp -m state --state NEW -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name allallipv6limit -j ACCEPT
$ip6tables -A allallipv6safetemp -m state --state NEW -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name allallipv6limit -j DROP
$ip6tables -A allallipv6safetemp -j DROP
#moved to bottom because it is above safelist in the iptables
create_ipset $ipset allallipv6block ipv6
create_iptables $ip6tables allallipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set allallipv6block src -j allallipv6block
$ip6tables -A allallipv6block -j DROP
create_ipset $ipset allallipv6blocktemp ipv6 2147483
create_iptables $ip6tables allallipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set allallipv6blocktemp src -j allallipv6blocktemp
$ip6tables -A allallipv6blocktemp -j DROP
#$ipset -A allallipv6safe ::/1
#$ipset -A allallipv6safe fe80::/10



#ICMP
echo "Setting up ipv6 ICMP Protection"
create_ipset $ipset 8icmpipv6safe ipv6
create_iptables $ip6tables 8icmpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8icmpipv6safe src -p ipv6-icmp -j 8icmpipv6safe
#$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8icmpipv6safe src -p icmpv6 -m conntrack --ctstate NEW -j 8icmpipv6safe
# ok icmp codes for INPUT (rfc4890, 4.4.1 and 4.4.2)
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type destination-unreachable -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type packet-too-big -j ACCEPT
# codes 0 and 1
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type time-exceeded -j ACCEPT
# codes 0-2
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type parameter-problem -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type echo-request -m limit --limit 900/min -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type echo-reply -m limit --limit 900/min -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type router-solicitation -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type router-advertisement -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type neighbor-solicitation -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type neighbor-advertisement -m hl --hl-eq 255 -j ACCEPT
# IND solicitation
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 141 -m hl --hl-eq 255 -j ACCEPT
# IND advertisement
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 142 -m hl --hl-eq 255 -j ACCEPT
# MLD query
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 130 -s fe80::/10 -j ACCEPT
# MLD report
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 131 -s fe80::/10 -j ACCEPT
# MLD done
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 132 -s fe80::/10 -j ACCEPT
# MLD report v2
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 143 -s fe80::/10 -j ACCEPT
# SEND certificate path solicitation
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 148 -m hl --hl-eq 255 -j ACCEPT
# SEND certificate path advertisement
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 149 -m hl --hl-eq 255 -j ACCEPT
# MR advertisement
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 151 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# MR solicitation
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 152 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# MR termination
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 153 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# Home Agent Address Discovery Reques
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 144 -j ACCEPT
# Home Agent Address Discovery Reply
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 145 -j ACCEPT
# Mobile Prefix Solicitation
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 146 -j ACCEPT
# Mobile Prefix Advertisement
$ip6tables -A 8icmpipv6safe -p icmpv6 --icmpv6-type 147 -j ACCEPT
$ip6tables -A 8icmpipv6safe -j DROP
create_ipset $ipset 8icmpipv6safetemp ipv6 2147483
create_iptables $ip6tables 8icmpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8icmpipv6safetemp src -p ipv6-icmp --icmpv6-type 128 -m conntrack --ctstate NEW -j 8icmpipv6safetemp
# ok icmp codes for INPUT (rfc4890, 4.4.1 and 4.4.2)
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type destination-unreachable -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type packet-too-big -j ACCEPT
# codes 0 and 1
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type time-exceeded -j ACCEPT
# codes 0-2
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type parameter-problem -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type echo-request -m limit --limit 900/min -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type echo-reply -m limit --limit 900/min -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type router-solicitation -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type router-advertisement -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type neighbor-solicitation -m hl --hl-eq 255 -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type neighbor-advertisement -m hl --hl-eq 255 -j ACCEPT
# IND solicitation
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 141 -m hl --hl-eq 255 -j ACCEPT
# IND advertisement
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 142 -m hl --hl-eq 255 -j ACCEPT
# MLD query
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 130 -s fe80::/10 -j ACCEPT
# MLD report
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 131 -s fe80::/10 -j ACCEPT
# MLD done
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 132 -s fe80::/10 -j ACCEPT
# MLD report v2
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 143 -s fe80::/10 -j ACCEPT
# SEND certificate path solicitation
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 148 -m hl --hl-eq 255 -j ACCEPT
# SEND certificate path advertisement
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 149 -m hl --hl-eq 255 -j ACCEPT
# MR advertisement
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 151 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# MR solicitation
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 152 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# MR termination
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 153 -s fe80::/10 -m hl --hl-eq 1 -j ACCEPT
# Home Agent Address Discovery Reques
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 144 -j ACCEPT
# Home Agent Address Discovery Reply
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 145 -j ACCEPT
# Mobile Prefix Solicitation
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 146 -j ACCEPT
# Mobile Prefix Advertisement
$ip6tables -A 8icmpipv6safetemp -p icmpv6 --icmpv6-type 147 -j ACCEPT
$ip6tables -A 8icmpipv6safetemp -j DROP
#moved to bottom because it is above safelist in the iptables
create_ipset $ipset 8icmpipv6block ipv6
create_iptables $ip6tables 8icmpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8icmpipv6block src -p ipv6-icmp -j 8icmpipv6block
$ip6tables -A 8icmpipv6block -j DROP
create_ipset $ipset 8icmpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 8icmpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8icmpipv6blocktemp src -p ipv6-icmp -j 8icmpipv6blocktemp
$ip6tables -A 8icmpipv6blocktemp -j DROP

$ipset -A 8icmpipv6safe fe80::/10
$ipset -A 8icmpipv6safe ::/1

#SSH
echo "Setting up ipv6 SSH Protection"
create_ipset $ipset 22tcpipv6safe ipv6
create_iptables $ip6tables 22tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 22tcpipv6safe src -p tcp --dport 22 -m conntrack --ctstate NEW -j 22tcpipv6safe
$ip6tables -A 22tcpipv6safe -p tcp --dport 22 -m state --state NEW -m recent --name SSH6 --update --seconds 86500 --hitcount 1 -j DROP
$ip6tables -A 22tcpipv6safe -p tcp --dport 22 -m state --state NEW -m recent --set --name SSH6 -j ACCEPT
$ip6tables -A 22tcpipv6safe -j DROP
create_ipset $ipset 22tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 22tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 22tcpipv6safetemp src -p tcp --dport 22 -m conntrack --ctstate NEW -j 22tcpipv6safetemp
$ip6tables -A 22tcpipv6safetemp -p tcp --dport 22 -m state --state NEW -m recent --name SSH6 --update --seconds 86500 --hitcount 1 -j DROP
$ip6tables -A 22tcpipv6safetemp -p tcp --dport 22 -m state --state NEW -m recent --set --name SSH6 -j ACCEPT
$ip6tables -A 22tcpipv6safetemp -j DROP
create_ipset $ipset 22tcpipv6block ipv6
create_iptables $ip6tables 22tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 22tcpipv6block src -p tcp --dport 22 -j 22tcpipv6block
$ip6tables -A 22tcpipv6block -j DROP
create_ipset $ipset 22tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 22tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 22tcpipv6blocktemp src -p tcp --dport 22 -j 22tcpipv6blocktemp
$ip6tables -A 22tcpipv6blocktemp -j DROP
#$ipset -A 22tcpipv6safe fe80::/10


#SMTP
echo "Setting up ipv6 SMTP Protection"
create_ipset $ipset 25tcpipv6safe ipv6
create_iptables $ip6tables 25tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 25tcpipv6safe src -p tcp --dport 25 -m conntrack --ctstate NEW -j 25tcpipv6safe
$ip6tables -A 25tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptcplimit -j ACCEPT
$ip6tables -A 25tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptcplimit -j DROP
$ip6tables -A 25tcpipv6safe -j DROP
create_ipset $ipset 25tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 25tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 25tcpipv6safetemp src -p tcp --dport 25 -m conntrack --ctstate NEW -j 25tcpipv6safetemp
$ip6tables -A 25tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptcplimit -j ACCEPT
$ip6tables -A 25tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 25 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptcplimit -j DROP
$ip6tables -A 25tcpipv6safetemp -j DROP
create_ipset $ipset 25tcpipv6block ipv6
create_iptables $ip6tables 25tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 25tcpipv6block src -p tcp --dport 25 -j 25tcpipv6block
$ip6tables -A 25tcpipv6block -j DROP
create_ipset $ipset 25tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 25tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 25tcpipv6blocktemp src -p tcp --dport 25 -j 25tcpipv6blocktemp
$ip6tables -A 25tcpipv6blocktemp -j DROP
$ipset -A 25tcpipv6safe fe80::/10

#DNS
echo "Setting up ipv6 DNS Protection"
create_ipset $ipset 53tcpipv6safe ipv6
create_iptables $ip6tables 53tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53tcpipv6safe src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53tcpipv6safe
$ip6tables -A 53tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j ACCEPT
$ip6tables -A 53tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j DROP
$ip6tables -A 53tcpipv6safe -j DROP
create_ipset $ipset 53tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 53tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53tcpipv6safetemp src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53tcpipv6safetemp
$ip6tables -A 53tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j ACCEPT
$ip6tables -A 53tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j DROP
$ip6tables -A 53tcpipv6safetemp -j DROP
create_ipset $ipset 53tcpipv6block ipv6
create_iptables $ip6tables 53tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53tcpipv6block src -p tcp --dport 53 -j 53tcpipv6block
$ip6tables -A 53tcpipv6block -j DROP
create_ipset $ipset 53tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 53tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53tcpipv6blocktemp src -p tcp --dport 53 -j 53tcpipv6blocktemp
$ip6tables -A 53tcpipv6blocktemp -j DROP
$ipset -A 53tcpipv6safe fe80::/10

create_ipset $ipset 53udpipv6safe ipv6
create_iptables $ip6tables 53udpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53udpipv6safe src -p tcp --dport 53 -m conntrack --ctstate NEW -j 53udpipv6safe
$ip6tables -A 53udpipv6safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j ACCEPT
$ip6tables -A 53udpipv6safe -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j DROP
$ip6tables -A 53udpipv6safe -j DROP
create_ipset $ipset 53udpipv6safetemp ipv6 2147483
create_iptables $ip6tables 53udpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53udpipv6safetemp src -p udp --dport 53 -m conntrack --ctstate NEW -j 53udpipv6safetemp
$ip6tables -A 53udpipv6safetemp -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j ACCEPT
$ip6tables -A 53udpipv6safetemp -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j DROP
$ip6tables -A 53udpipv6safetemp -j DROP
create_ipset $ipset 53udpipv6block ipv6
create_iptables $ip6tables 53udpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53udpipv6block src -p tcp --dport 53 -j 53udpipv6block
$ip6tables -A 53udpipv6block -j DROP
create_ipset $ipset 53udpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 53udpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 53udpipv6blocktemp src -p tcp --dport 53 -j 53udpipv6blocktemp
$ip6tables -A 53udpipv6blocktemp -j DROP
$ipset -A 53udpipv6safe fe80::/10


#Web HTTP
echo "Setting up ipv6 WEB HTTP Protection"
create_ipset $ipset 80tcpipv6safe ipv6
create_iptables $ip6tables 80tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 80tcpipv6safe src -p tcp --dport 80 -m conntrack --ctstate NEW -j 80tcpipv6safe
$ip6tables -A 80tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httptcplimit -j ACCEPT
$ip6tables -A 80tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httptcplimit -j DROP
$ip6tables -A 80tcpipv6safe -j DROP
create_ipset $ipset 80tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 80tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 80tcpipv6safetemp src -p tcp --dport 80 -m conntrack --ctstate NEW -j 80tcpipv6safetemp
$ip6tables -A 80tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httptcplimit -j ACCEPT
$ip6tables -A 80tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 80 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httptcplimit -j DROP
$ip6tables -A 80tcpipv6safetemp -j DROP
create_ipset $ipset 80tcpipv6block ipv6
create_iptables $ip6tables 80tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 80tcpipv6block src -p tcp --dport 80 -j 80tcpipv6block
$ip6tables -A 80tcpipv6block -j DROP
create_ipset $ipset 80tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 80tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 80tcpipv6blocktemp src -p tcp --dport 80 -j 80tcpipv6blocktemp
$ip6tables -A 80tcpipv6blocktemp -j DROP
$ipset -A 80tcpipv6safe fe80::/10

#NTP
echo "Setting up ipv6 NTP Protection"
create_ipset $ipset 123udpipv6safe ipv6
create_iptables $ip6tables 123udpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 123udpipv6safe src -p udp --dport 123 -m conntrack --ctstate NEW -j 123udpipv6safe
$ip6tables -A 123udpipv6safe -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name ntpudplimit -j ACCEPT
$ip6tables -A 123udpipv6safe -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name ntpudplimit -j DROP
$ip6tables -A 123udpipv6safe -j DROP
create_ipset $ipset 123udpipv6safetemp ipv6 2147483
create_iptables $ip6tables 123udpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 123udpipv6safetemp src -p udp --dport 123 -m conntrack --ctstate NEW -j 123udpipv6safetemp
$ip6tables -A 123udpipv6safetemp -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name ntpudplimit -j ACCEPT
$ip6tables -A 123udpipv6safetemp -m state --state NEW -m udp -p udp --dport 123 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name ntpudplimit -j DROP
$ip6tables -A 123udpipv6safetemp -j DROP
create_ipset $ipset 123udpipv6block ipv6
create_iptables $ip6tables 123udpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 123udpipv6block src -p udp --dport 123 -j 123udpipv6block
$ip6tables -A 123udpipv6block -j DROP
create_ipset $ipset 123udpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 123udpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 123udpipv6blocktemp src -p udp --dport 123 -j 123udpipv6blocktemp
$ip6tables -A 123udpipv6blocktemp -j DROP
$ipset -A 123udpipv6safe fe80::/10

#BGP
echo "Setting up ipv6 BGP Protection"
create_ipset $ipset 179tcpipv6safe ipv6
create_iptables $ip6tables 179tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 179tcpipv6safe src -p tcp --dport 179 -m conntrack --ctstate NEW -j 179tcpipv6safe
$ip6tables -A 179tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name bgptcplimit -j ACCEPT
$ip6tables -A 179tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name bgptcplimit -j DROP
$ip6tables -A 179tcpipv6safe -j DROP
create_ipset $ipset 179tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 179tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 179tcpipv6safetemp src -p tcp --dport 179 -m conntrack --ctstate NEW -j 179tcpipv6safetemp
$ip6tables -A 179tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name bgptcplimit -j ACCEPT
$ip6tables -A 179tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 179 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name bgptcplimit -j DROP
$ip6tables -A 179tcpipv6safetemp -j DROP
create_ipset $ipset 179tcpipv6block ipv6
create_iptables $ip6tables 179tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 179tcpipv6block src -p tcp --dport 179 -j 179tcpipv6block
$ip6tables -A 179tcpipv6block -j DROP
create_ipset $ipset 179tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 179tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 179tcpipv6blocktemp src -p tcp --dport 179 -j 179tcpipv6blocktemp
$ip6tables -A 179tcpipv6blocktemp -j DROP
$ipset -A 179tcpipv6safe fe80::/10
$ipset -A 179tcpipv6safe ::/1

#HTTPS
echo "Setting up ipv6 WEB HTTPS Protection"
create_ipset $ipset 443tcpipv6safe ipv6
create_iptables $ip6tables 443tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 443tcpipv6safe src -p tcp --dport 443 -m conntrack --ctstate NEW -j 443tcpipv6safe
$ip6tables -A 443tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpstcplimit -j ACCEPT
$ip6tables -A 443tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpstcplimit -j DROP
$ip6tables -A 443tcpipv6safe -j DROP
create_ipset $ipset 443tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 443tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 443tcpipv6safetemp src -p tcp --dport 443 -m conntrack --ctstate NEW -j 443tcpipv6safetemp
$ip6tables -A 443tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpstcplimit -j ACCEPT
$ip6tables -A 443tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpstcplimit -j DROP
$ip6tables -A 443tcpipv6safetemp -j DROP
create_ipset $ipset 443tcpipv6block ipv6
create_iptables $ip6tables 443tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 443tcpipv6block src -p tcp --dport 443 -j 443tcpipv6block
$ip6tables -A 443tcpipv6block -j DROP
create_ipset $ipset 443tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 443tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 443tcpipv6blocktemp src -p tcp --dport 443 -j 443tcpipv6blocktemp
$ip6tables -A 443tcpipv6blocktemp -j DROP
$ipset -A 443tcpipv6safe fe80::/10

#HTTPS-ALT
echo "Setting up ipv6 WEB HTTPS-ALT Protection"
create_ipset $ipset 8443tcpipv6safe ipv6
create_iptables $ip6tables 8443tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8443tcpipv6safe src -p tcp --dport 8443 -m conntrack --ctstate NEW -j 8443tcpipv6safe
$ip6tables -A 8443tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpsalttcplimit -j ACCEPT
$ip6tables -A 8443tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpsalttcplimit -j DROP
$ip6tables -A 8443tcpipv6safe -j DROP
create_ipset $ipset 8443tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 8443tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8443tcpipv6safetemp src -p tcp --dport 8443 -m conntrack --ctstate NEW -j 8443tcpipv6safetemp
$ip6tables -A 8443tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpsalttcplimit -j ACCEPT
$ip6tables -A 8443tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 8443 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name httpsalttcplimit -j DROP
$ip6tables -A 8443tcpipv6safetemp -j DROP
create_ipset $ipset 8443tcpipv6block ipv6
create_iptables $ip6tables 8443tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8443tcpipv6block src -p tcp --dport 8443 -j 8443tcpipv6block
$ip6tables -A 8443tcpipv6block -j DROP
create_ipset $ipset 8443tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 8443tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 8443tcpipv6blocktemp src -p tcp --dport 8443 -j 8443tcpipv6blocktemp
$ip6tables -A 8443tcpipv6blocktemp -j DROP
$ipset -A 8443tcpipv6safe fe80::/10

#SMTPS
echo "Setting up ipv6 WEB HTTPS Protection"
create_ipset $ipset 587tcpipv6safe ipv6
create_iptables $ip6tables 587tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 587tcpipv6safe src -p tcp --dport 587 -m conntrack --ctstate NEW -j 587tcpipv6safe
$ip6tables -A 587tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtpstcplimit -j ACCEPT
$ip6tables -A 587tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtpstcplimit -j DROP
$ip6tables -A 587tcpipv6safe -j DROP
create_ipset $ipset 587tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 587tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 587tcpipv6safetemp src -p tcp --dport 587 -m conntrack --ctstate NEW -j 587tcpipv6safetemp
$ip6tables -A 587tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtpstcplimit -j ACCEPT
$ip6tables -A 587tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 587 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtpstcplimit -j DROP
$ip6tables -A 587tcpipv6safetemp -j DROP
create_ipset $ipset 587tcpipv6block ipv6
create_iptables $ip6tables 587tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 587tcpipv6block src -p tcp --dport 587 -j 587tcpipv6block
$ip6tables -A 587tcpipv6block -j DROP
create_ipset $ipset 587tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 587tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 587tcpipv6blocktemp src -p tcp --dport 587 -j 587tcpipv6blocktemp
$ip6tables -A 587tcpipv6blocktemp -j DROP
$ipset -A 587tcpipv6safe fe80::/10


#SMTP TLS
echo "Setting up ipv6 SMTP TLS Protection"
create_ipset $ipset 465tcpipv6safe ipv6
create_iptables $ip6tables 465tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 465tcpipv6safe src -p tcp --dport 465 -m conntrack --ctstate NEW -j 465tcpipv6safe
$ip6tables -A 465tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptlstcplimit -j ACCEPT
$ip6tables -A 465tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptlstcplimit -j DROP
$ip6tables -A 465tcpipv6safe -j DROP
create_ipset $ipset 465tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 465tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 465tcpipv6safetemp src -p tcp --dport 465 -m conntrack --ctstate NEW -j 465tcpipv6safetemp
$ip6tables -A 465tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptlstcplimit -j ACCEPT
$ip6tables -A 465tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 465 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name smtptlstcplimit -j DROP
$ip6tables -A 465tcpipv6safetemp -j DROP
create_ipset $ipset 465tcpipv6block ipv6
create_iptables $ip6tables 465tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 465tcpipv6block src -p tcp --dport 465 -j 465tcpipv6block
$ip6tables -A 465tcpipv6block -j DROP
create_ipset $ipset 465tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 465tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 465tcpipv6blocktemp src -p tcp --dport 465 -j 465tcpipv6blocktemp
$ip6tables -A 465tcpipv6blocktemp -j DROP
$ipset -A 465tcpipv6safe fe80::/10

#IMAPS
echo "Setting up ipv6 IMAPS Protection"
create_ipset $ipset 993tcpipv6safe ipv6
create_iptables $ip6tables 993tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 993tcpipv6safe src -p tcp --dport 993 -m conntrack --ctstate NEW -j 993tcpipv6safe
$ip6tables -A 993tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name imapstcplimit -j ACCEPT
$ip6tables -A 993tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name imapstcplimit -j DROP
$ip6tables -A 993tcpipv6safe -j DROP
create_ipset $ipset 993tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 993tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 993tcpipv6safetemp src -p tcp --dport 993 -m conntrack --ctstate NEW -j 993tcpipv6safetemp
$ip6tables -A 993tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name imapstcplimit -j ACCEPT
$ip6tables -A 993tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 993 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name imapstcplimit -j DROP
$ip6tables -A 993tcpipv6safetemp -j DROP
create_ipset $ipset 993tcpipv6block ipv6
create_iptables $ip6tables 993tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 993tcpipv6block src -p tcp --dport 993 -j 993tcpipv6block
$ip6tables -A 993tcpipv6block -j DROP
create_ipset $ipset 993tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 993tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 993tcpipv6blocktemp src -p tcp --dport 993 -j 993tcpipv6blocktemp
$ip6tables -A 993tcpipv6blocktemp -j DROP
$ipset -A 993tcpipv6safe fe80::/10

#MISC
echo "Setting up ipv6 Misc Protection"
create_ipset $ipset 4500udpipv6safe ipv6
create_iptables $ip6tables 4500udpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 4500udpipv6safe src -p udp --dport 4500 -m conntrack --ctstate NEW -j 4500udpipv6safe
$ip6tables -A 4500udpipv6safe -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name miscudplimit -j ACCEPT
$ip6tables -A 4500udpipv6safe -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name miscudplimit -j DROP
$ip6tables -A 4500udpipv6safe -j DROP
create_ipset $ipset 4500udpipv6safetemp ipv6 2147483
create_iptables $ip6tables 4500udpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 4500udpipv6safetemp src -p udp --dport 4500 -m conntrack --ctstate NEW -j 8443tcpipv6safetemp
$ip6tables -A 4500udpipv6safetemp -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name miscudplimit -j ACCEPT
$ip6tables -A 4500udpipv6safetemp -m state --state NEW -m udp -p udp --dport 4500 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name miscudplimit -j DROP
$ip6tables -A 4500udpipv6safetemp -j DROP
create_ipset $ipset 4500udpipv6block ipv6
create_iptables $ip6tables 4500udpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 4500udpipv6block src -p udp --dport 4500 -j 4500udpipv6block
$ip6tables -A 4500udpipv6block -j DROP
create_ipset $ipset 4500udpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 4500udpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 4500udpipv6blocktemp src -p udp --dport 4500 -j 4500udpipv6blocktemp
$ip6tables -A 4500udpipv6blocktemp -j DROP
$ipset -A 4500udpipv6safe fe80::/10

#Secure DoT
#create_iptables $ip6tables DOT-RATE-LIMIT
#$ip6tables -A DOT-RATE-LIMIT -m hashlimit --hashlimit-mode srcip --hashlimit-upto 30/sec --hashlimit-burst 20 --hashlimit-name dot_conn_rate_limit --jump ACCEPT
#$ip6tables -A DOT-RATE-LIMIT -m limit --limit 1/sec --jump LOG --log-prefix "IPTables-Ratelimited: "
#$ip6tables -A DOT-RATE-LIMIT -j DROP
#$ip6tables -I INPUT $(($ipn6 + 1)) -p tcp --dport 853 -m conntrack --ctstate NEW -j DOT-RATE-LIMIT

echo "Setting up ipv6 DOT"
create_ipset $ipset 853tcpipv6safe ipv6
create_iptables $ip6tables 853tcpipv6safe
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 853tcpipv6safe src -p tcp --dport 853 -m conntrack --ctstate NEW -j 853tcpipv6safe
$ip6tables -A 853tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name tlsdnstcplimit -j ACCEPT
$ip6tables -A 853tcpipv6safe -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name tlsdnstcplimit -j DROP
$ip6tables -A 853tcpipv6safe -j DROP
create_ipset $ipset 853tcpipv6safetemp ipv6 2147483
create_iptables $ip6tables 853tcpipv6safetemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 853tcpipv6safetemp src -p tcp --dport 853 -m conntrack --ctstate NEW -j 853tcpipv6safetemp
$ip6tables -A 853tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name tlsdnstcplimit -j ACCEPT
$ip6tables -A 853tcpipv6safetemp -m state --state NEW -m tcp -p tcp --dport 853 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name tlsdnstcplimit -j DROP
$ip6tables -A 853tcpipv6safetemp -j DROP
create_ipset $ipset 853tcpipv6block ipv6
create_iptables $ip6tables 853tcpipv6block
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 853tcpipv6block src -p tcp --dport 853 -j 853tcpipv6block
$ip6tables -A 853tcpipv6block -j DROP
create_ipset $ipset 853tcpipv6blocktemp ipv6 2147483
create_iptables $ip6tables 853tcpipv6blocktemp
$ip6tables -I INPUT $(($ipn6 + 1)) -m set --match-set 853tcpipv6blocktemp src -p tcp --dport 853 -j 853tcpipv6blocktemp
$ip6tables -A 853tcpipv6blocktemp -j DROP
$ipset -A 853tcpipv6safe ::/1





echo "Setting up IPv6 Lists"
#create_ip6tables $ip6tables UPTIMEROBOT
#$ip6tables -A UPTIMEROBOT -p tcp -m multiport --dports 80,443,853,8443 -j ACCEPT
#$ip6tables -A UPTIMEROBOT -p icmpv6 --icmpv6-type echo-request -j ACCEPT

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
echo "::/1" > /etc/configs/iptables/all-ipv6.raw
echo "$(awk '{print}' /etc/configs/iptables/all-ipv6.raw | awk 'gsub(/\r/, "") {print}' | awk '{gsub(/\r/, ""); print}' | awk '/:/')" > /etc/configs/iptables/all-ipv6.txt

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
echo "fe80::/10" > /etc/configs/iptables/local-ipv6.raw
echo "$(awk '{print}' /etc/configs/iptables/all-ipv6.raw | awk 'gsub(/\r/, "") {print}' | awk '{gsub(/\r/, ""); print}' | awk '/:/')" > /etc/configs/iptables/all-ipv6.txt

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
##cache_file "https://uptimerobot.com/inc/files/ips/IPv6.txt" /etc/configs/iptables/uptimerobot-ipv6.raw 950400
cache_file "https://uptimerobot.com/inc/files/ips/IPv4andIPv6.txt" /etc/configs/iptables/uptimerobot-ipv6.raw 950400
echo "$(awk '{print}' /etc/configs/iptables/uptimerobot-ipv6.raw | awk 'gsub(/\r/, "") {print}' | awk '{gsub(/\r/, ""); print}' | awk '/:/')" > /etc/configs/iptables/uptimerobot-ipv6.txt
#for x in $(cat /etc/configs/iptables/uptimerobot-ipv6.txt); do $ip6tables -A INPUT -s $(echo ${x}/128) -j UPTIMEROBOT ; done

#create_ip6tables $ip6tables CLOUDFLARE
#$ip6tables -A CLOUDFLARE -p tcp -m multiport --dports 80,443,8443 -j ACCEPT

if [ ! -d /etc/configs/iptables ]; then mkdir -p /etc/configs/iptables; fi
cache_file "https://api.cloudflare.com/client/v4/ips" /etc/configs/iptables/cloudflare-ipv6.raw 950400
echo "$(awk '{print}' /etc/configs/iptables/cloudflare-ipv6.raw | $jq -r '.result.ipv6_cidrs | .[]' | awk '{gsub(/\r/, ""); print}')" > /etc/configs/iptables/cloudflare-ipv6.txt
#for x in $(cat /etc/configs/iptables/cloudflare-ipv6.txt); do $ip6tables -A INPUT -s $(echo ${x}) -j CLOUDFLARE ; done

#domain wide ip6tables
domainsuffix="$(hostname -d)"
dohparse cfg-ip6tables.$domainsuffix TXT > /etc/configs/iptables/rules-ip6tables.txt
for x in $(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-ip6tables.txt)
do
ipsetlist=$(echo "$x" | awk -F- '{print $1$2$3$4}')
ipsetfile=$(echo "$x" | awk -F- '{print $5"-"$3""}' )
echo "ipsetlist: $ipsetlist domain file: ${ipsetfile}.txt"
if [ -f /etc/configs/iptables/${ipsetfile}.txt ]; then 
    echo "${ipsetfile}.txt domain file found and importing ips";
else
    dnsipsetfile="$ipsetfile.cfg-ip6tables.$hostsuffix"
    echo "file not found getting domain custom list via dns: $dnsipsetfile"
    urlhostdownload=$(dohparse $dnsipsetfile TXT)
    cache_file "$urlhostdownload" /etc/configs/iptables/${ipsetfile}.raw 950400
    echo "$(awk '{print}' /etc/configs/iptables/${ipsetfile}.raw | awk '{gsub(/\r/, ""); print}' | awk '{gsub(/"|,|[|]|{|}| /, "\n"); print}' | awk '/[a-fA-F0-9]/' | awk '!/[g-zG-Z]/' | awk '/^[a-fA-F0-9]{0,4}\:/')" > /etc/configs/iptables/${ipsetfile}.txt
fi
  for y in $(awk '{print}' /etc/configs/iptables/${ipsetfile}.txt)
  do
      echo "importing ip: $y to ipset ${ipsetlist}"
      $ipset -A $ipsetlist $y 2>/dev/null
  done
done


#host wide ip6tables
hostsuffix="$(hostname -f)"
dohparse cfg-ip6tables.$hostsuffix TXT > /etc/configs/iptables/rules-host-ip6tables.txt
for x in $(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-host-ip6tables.txt)
do
ipsetlist=$(echo "$x" | awk -F- '{print $1$2$3$4}')
ipsetfile=$(echo "$x" | awk -F- '{print $5"-"$3""}' )
echo "ipsetlist: $ipsetlist host file: ${ipsetfile}.txt"
if [ -f /etc/configs/iptables/${ipsetfile}.txt ]; then 
    echo "${ipsetfile}.txt host file found and importing ips";
else
    dnsipsetfile="$ipsetfile.cfg-ip6tables.$hostsuffix"
    echo "file not found getting host custom list via dns: $dnsipsetfile"
    urlhostdownload=$(dohparse $dnsipsetfile TXT)
    cache_file "$urlhostdownload" /etc/configs/iptables/${ipsetfile}.raw 950400
    echo "$(awk '{print}' /etc/configs/iptables/${ipsetfile}.raw | awk '{gsub(/\r/, ""); print}' | awk '{gsub(/"|,|[|]|{|}| /, "\n"); print}' | awk '/[a-fA-F0-9]/' | awk '!/[g-zG-Z]/' | awk '/^[a-fA-F0-9]{0,4}\:/')" > /etc/configs/iptables/${ipsetfile}.txt
fi
  for y in $(awk '{print}' /etc/configs/iptables/${ipsetfile}.txt)
  do
      echo "importing ip: $y to ipset ${ipsetlist}"
      $ipset -A $ipsetlist $y 2>/dev/null
  done
done


#echo "Setting up IPv6 DNS protection"
#$ip6tables -A INPUT -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j ACCEPT
#$ip6tables -A INPUT -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j DROP
#$ip6tables -A INPUT -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j ACCEPT
#$ip6tables -A INPUT -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j DROP

#echo "Setting up IPv6 DOT protection"
#create_ip6tables $ip6tables DOT-RATE-LIMIT
#$ip6tables -A DOT-RATE-LIMIT -m hashlimit --hashlimit-mode srcip --hashlimit-upto 30/sec --hashlimit-burst 20 --hashlimit-name #dot_conn_rate_limit --jump ACCEPT
#$ip6tables -A DOT-RATE-LIMIT -m limit --limit 1/sec --jump LOG --log-prefix "IPTables-Ratelimited: "
#$ip6tables -A DOT-RATE-LIMIT -j DROP
#$ip6tables -I INPUT -p tcp --dport 853 -m conntrack --ctstate NEW -j DOT-RATE-LIMIT

#for x in $(cat /etc/configs/iptables/cloudflare-ips-v6.txt); do echo "x: $x"; done
#echo "ip6tables $ip6tables"
#echo "iptab: $(whichalt ip6tables)"

echo "Drop all the rest ipv6 input traffic" #Set on the bottom ip6tables
$ip6tables -A INPUT -j LOG --log-prefix "ip6tablesinputdrop" --log-level 6
$ip6tables -A INPUT -j DROP

#exit

#Add Trusted
mkdir -p /etc/cron.hourly
cat > /etc/cron.hourly/update-ipset <<EOF
#!/bin/env bash

whichalt(){
file=\$1
found=0
#pathdir="\$PATH"
#if [ -z "\$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/\$1/\$1"; fi
pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/\$1/\$1"
for p in \$(echo \$pathdir | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "\${p}/\${file}" ]; then found=1; echo "\${p}/\${file}"; return 0; fi
 done
if [ "\$found" = 0 ]; then echo "\$file not found"; return 1; fi
}


dohparse(){
domain=\$1
type=\$2
if [ -z "\$type" ]; then type='A'; fi
curl=\$(whichalt curl)
data=\$(\$curl "https://8.8.8.8/resolve?type=\$type&name=\$domain" 2>/dev/null)
if [ -z "\$data" ]; then data=\$(\$curl "https://[2001:4860:4860::8888]/resolve?type=\$type&name=\$domain" 2>/dev/null); fi
parse=\$(echo \$data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print \$1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",\$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[\$0]++')
if [ "\$type" = 'A' ]; then echo \$parse | awk '{printf "%s ", \$0}' | awk 'gsub(/ /," \n") { print }' | awk '{match(\$0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr(\$0,RSTART,RLENGTH); print ip}' | awk '!seen[\$0]++' | awk '/./'; fi
if [ "\$type" = 'AAAA' ]; then echo \$parse | awk '{printf "%s ", \$0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[\$0]++' | awk '/./'; fi
if [ "\$type" = 'TXT' ]; then echo "\$parse"; fi
#if [ ! -z "\$parse" ]; then echo \$parse; fi
}

ipset=\$(whichalt ipset)

echo "Querying Trusted IPs"
TrustedDir='/etc/configs/iptables'
if [ ! -d "\$TrustedDir" ]; then mkdir -p \$TrustedDir; fi
TrustedIPv4file='trusted-ipv4.txt'
TrustedIPv6file='trusted-ipv6.txt'
prisrvsuffix="a.\$(hostname -d)"
cat /dev/null > \$TrustedDir/\$TrustedIPv4file
cat /dev/null > \$TrustedDir/\$TrustedIPv6file
for seqnum in \`seq 1 40\`
do
        lookupsrv="\${seqnum}\${prisrvsuffix}"
        dohparse \$lookupsrv A | awk 'NR<2{ print }' | awk '!/^0.0.0.0$/ { print }' | awk '!/^127.255.255.255$/ { print }' |awk '/./' >> \$TrustedDir/\$TrustedIPv4file
        dohparse \$lookupsrv AAAA | awk 'NR<2{ print }' | awk '!/^::$/ { print }' | awk '!/^::ffff:127.255.255.255$/ { print }' | awk '!/^::ffff:7fff:ffff$/ { print }' | awk '/./' >> \$TrustedDir/\$TrustedIPv6file
        sleep 1
done
#Add Local Subnet
echo "Adding Local Subnet"
ip -o -f inet addr show |  awk '!/docker0/ { print }' | awk '/scope global/ {print \$4}' | awk '/^10\.|^172\.(1[6-9]|2[0-9]|3[0-1])\.|^192\.168\./ { print }' >> \$TrustedDir/\$TrustedIPv4file
ip -o -f inet6 addr show |  awk '!/docker0/ { print }' | awk '/scope global/ {print \$4}' >> \$TrustedDir/\$TrustedIPv6file

# Using Awk to remove empty spaces and duplicates
echo "\$(awk '!seen[\$0]++' \$TrustedDir/\$TrustedIPv4file | awk '/./')" > \$TrustedDir/\$TrustedIPv4file
echo "\$(awk '!seen[\$0]++' \$TrustedDir/\$TrustedIPv6file | awk '/./')" > \$TrustedDir/\$TrustedIPv6file

# Add to ipset

#cat \$TrustedDir/\$TrustedIPv4file | while read line; do \$ipset -D -! dynamic_v4_ip_accept \$line; \$ipset -A dynamic_v4_ip_accept \$line timeout 5100; done
#cat \$TrustedDir/\$TrustedIPv6file | while read line; do \$ipset -D -! dynamic_v6_ip_accept \$line; \$ipset -A dynamic_v6_ip_accept \$line timeout 5100; done

if [ ! -f /etc/configs/iptables/rules-iptables.txt ]; then
domainsuffix="\$(hostname -d)"
dohparse cfg-iptables.\$domainsuffix TXT > /etc/configs/iptables/rules-iptables.txt
fi

ipset="\$(whichalt ipset)"
if [ -f /etc/configs/iptables/rules-iptables.txt ]; then
  for x in \$(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-iptables.txt | awk '/temp/')
  do
  ipsetlist=\$(echo "\$x" | awk -F- '{print \$1\$2\$3\$4}')
  ipsetfile=\$(echo "\$x" | awk -F- '{print \$5"-"\$3".txt"}' )
  #echo "ipsetlist: \$ipsetlist file: \$ipsetfile"
  if [ -f /etc/configs/iptables/\$ipsetfile ]; then
      #echo "\$ipsetfile file found and importing ips";
      for y in \$(awk '{print}' /etc/configs/iptables/\$ipsetfile)
      do
          #echo "importing ip: \$y to ipset \$ipsetlist"
          \$ipset -D \$ipsetlist \$y 2>/dev/null
          \$ipset -A \$ipsetlist \$y timeout 5100 2>/dev/null
      done
  fi
  done
fi

#Add ipv6
if [ ! -f /etc/configs/iptables/rules-ip6tables.txt ]; then
domainsuffix="\$(hostname -d)"
dohparse cfg-ip6tables.\$domainsuffix TXT > /etc/configs/iptables/rules-ip6tables.txt
fi

for x in \$(awk '{gsub(/_/,"\n"); print}' /etc/configs/iptables/rules-ip6tables.txt | awk '/temp/')
do
ipsetlist=\$(echo "\$x" | awk -F- '{print \$1\$2\$3\$4}')
ipsetfile=\$(echo "\$x" | awk -F- '{print \$5"-"\$3".txt"}' )
#echo "ipsetlist: \$ipsetlist file: \$ipsetfile"
if [ -f /etc/configs/iptables/\$ipsetfile ]; then
    #echo "\$ipsetfile file found and importing ips";
    for y in \$(awk '{print}' /etc/configs/iptables/\$ipsetfile)
    do
        #echo "importing ip: \$y to ipset \$ipsetlist"
        \$ipset -D \$ipsetlist \$y 2>/dev/null
        \$ipset -A \$ipsetlist \$y timeout 5100 2>/dev/null
    done
fi
done

EOF
chmod 755 /etc/cron.hourly/update-ipset
sh /etc/cron.hourly/update-ipset

#run daily
mkdir -p /etc/cron.daily
cfile=`realpath $0`
cat > /etc/cron.daily/update-iptables <<EOF
#!/bin/env bash
echo "3" | sh $cfile
EOF
chmod 755 /etc/cron.daily/update-iptables

#put in startup
mkdir -p /etc/cron.startup
cfile=`realpath $0`
cat > /etc/cron.startup/update-iptables <<EOF
#!/bin/env bash
echo "3" | sh $cfile
EOF
chmod 755 /etc/cron.startup/update-iptables

#cleanup tmp
mkdir -p /etc/cron.weekly
cfile=`realpath $0`
cat > /etc/cron.weekly/cleanup-tmp <<EOF
#!/bin/env bash
cd /tmp
rm *.txt
EOF
chmod 755 /etc/cron.weekly/cleanup-tmp

#create startup directory
if [ -z "$(awk '/@reboot/' /etc/crontab)" ]; then echo '@reboot root cd / && run-parts --report /etc/cron.startup' >> /etc/crontab; fi