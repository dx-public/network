#!/bin/env sh

install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then
  
  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$(curl -s "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$(curl -s "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  curl "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo $_BINARY
fi
return 0
}


echo "Installing NTP Server"

if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor/; chmod +w /etc/supervisor; fi

retrydownload() {
#retrydownload curl https://example.com /dev/null
prog="$1"
url="$2"
file="$3"
alturl="$url"
if [ "$(echo "$url" | awk '/\/ipfs\/|\/ipns\/|.ipfs.|.ipns./ {print $0}')" = "$url" ]; then
 echo "ipfs/ipns url found"
 cidurl="$(echo "$url" | awk -F'.' '{ sub(/http:\/\/|https:\/\//, ""); print $1}')"
 filename="$(echo "$url" | awk -F / '{print $NF}')"  # or "$(echo ${url##*/})" 
 proto="$(echo $url | awk -F'/' '{print $1"//"}')"
 alturl="$(echo ${proto}${cidurl}.ipfs.4everland.io/${filename})"
fi

cmdprog=""
if [ "$prog" = "wget" ]; then cmdprog="/bin/wget -q -O $file"; fi
if [ "$prog" = "curl" ]; then cmdprog="/bin/curl -L --doh-url https://1.1.1.1/dns-query -s -o $file"; fi
#echo $cmdprog
i=0
while [ $i -lt 20 ]
do
    i=$((i + 1))
    if [ $i -ge 10 ]; then url="$alturl"; fi
    echo "downloading from $url using $prog and saving to $file"
    $($cmdprog $url > $file)
    result=$?
    if [ "$result" -eq 0 ] && [ -s "$file" ] && [ "$(awk '{if(NR==1) {print substr($1,1,1)}}' $file)" != "<" ]; then
        break
    fi
    echo "Error: Retry Download Again... $i time(s)"
    if [ $i -eq 20 ]; then echo "Error: please download file again"; break; fi
    sleep 3
done
#if [ $i -lt 20 ]; then echo "Successful after $i trie(s)"; fi
}


error_code=-1 && /etc/gontpresolver/gontpresolver --version 2>/dev/null || error_code=$?
if [ ! -f /etc/gontpresolver/gontpresolver ] || [ "$error_code" -gt 100 ]; then
_BINARY=`install_package gontpresponder || echo 'https://bafybeibu5qyiru6s3iruowavcfmzgmmjoerolghitsmplle7vhprbz622i.ipfs.w3s.link/gontpresponder_v2023.05.29.1_linux_amd64'`

#curl -Ls $_BINARY -o /tmp/gontpresolver
retrydownload curl $_BINARY /tmp/gontpresolver 
mkdir -p /etc/gontpresolver
mv /tmp/gontpresolver /etc/gontpresolver/.
chmod +x /etc/gontpresolver/gontpresolver
fi

error_code=-1 && /etc/goip/goip --version 2>/dev/null || error_code=$?
if [ ! -f /etc/goip/goip ] || [ "$error_code" -gt 100 ]; then
_BINARY=`install_package goip || echo 'https://bafybeidzspaknaotcpfgnkmdasgkubrtpzhc6uvrva7qtlovxrg3geomyu.ipfs.w3s.link/goip_v1.0.0_linux_amd64'`

#curl -Ls $_BINARY -o /tmp/goip
retrydownload curl $_BINARY /tmp/goip
mkdir -p /etc/goip
mv /tmp/goip /etc/goip/.
chmod +x /etc/goip/goip
fi

echo "-----------------"
echo "Checking if online"
echo "-----------------"
if curl -sI --fail https://nist.time.gov -o /dev/null; then
 echo "ONLINE... continuing..."

 echo "-----------------"
 echo "Sync Time"
 echo "-----------------"
 
 #date -s "`curl --connect-timeout 4.1 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g'`"
 date -s "`curl --connect-timeout 4.1 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print $2}'`"
 if [ $(which hwclock) ]; then
 hwclock -w
 fi
 if [ $(which timdatectl) ]; then
  timedatectl set-ntp false
  timedatectl set-local-rtc 0
  timedatectl
 fi
else
 echo "OFFLINE... clock not updated"
 #exit
fi

chmod +x /etc/gontpresolver/gontpresolver
chmod +x /etc/goip/goip

#grabing all local ips
/etc/goip/goip | awk '! /Total|lo/' | awk -F'/' '{ $2=""; print}' | awk '// {gsub(/addr=|name=/, ""); print $3}' | awk '! /^fe80/' | awk '/:/' > /etc/goip/ntp.ips
/etc/goip/goip | awk '! /Total/' | awk -F'/' '{ $2=""; print}' | awk '// {gsub(/addr=|name=/, ""); print $3}' | awk '/\./' >> /etc/goip/ntp.ips

#grabbing virtual ips via dns query
domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
dnsrecord=$(curl -s "https://8.8.8.8/resolve?name=ntp-ips.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}') 
if [ ! -z "$dnsrecord" ]; then
echo "$dnsrecord" | awk '// {gsub(/ /, "\n"); print}' >> /etc/goip/ntp.ips
fi

#sort, remove spaces and filter duplicates
awk '{ line[NR] = $0 } END { do { haschanged = 0; for(i=1; i < NR; i++) { if ( line[i] > line[i+1] ) { t = line[i]; line[i] = line[i+1]; line[i+1] = t; haschanged = 1 } } } while ( haschanged == 1 ); for(i=1; i <= NR; i++) { print line[i] } }' /etc/goip/ntp.ips > /etc/goip/ntp.ips.temp && mv /etc/goip/ntp.ips.temp /etc/goip/ntp.ips
awk '{$1=$1;print}' /etc/goip/ntp.ips > /etc/goip/ntp.ips.temp && mv /etc/goip/ntp.ips.temp /etc/goip/ntp.ips
awk '!($0 in a) {a[$0];print}' /etc/goip/ntp.ips > /etc/goip/ntp.ips.temp && mv /etc/goip/ntp.ips.temp /etc/goip/ntp.ips

ipline=""
for ip in $(awk -F '\n' '{print}' /etc/goip/ntp.ips); do
ipline=$(echo "$ipline -ip $ip")
#echo "line: $ip"
done
#echo "ipline: $ipline\n\n"


if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor; fi
if [ ! -f /etc/supervisor/program:gontpresolver.args ]; then
echo "command = /etc/gontpresolver/gontpresolver $ipline" > /etc/supervisor/program:gontpresolver.args
fi


echo "Finished Installing NTP Server"
