#!/bin/sh

echo "Configuring SSL for services"

masterdomain=$(hostname -f | awk -F. 'BEGIN{OFS=FS} {sub(".*" $1 FS,"")}1')
certserver=$(curl "https://8.8.8.8/resolve?name=certserver.ssl.${masterdomain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' )

if [ ! -z "$certserver" ]; then
echo "certserver found"
if [ ! -d /etc/ssl ]; then mkdir -p /etc/ssl; fi

echo "Downloading $certserver"
curl -sL "$certserver" > /etc/ssl/certs.txt

echo "changing to directory"
cwd=$PWD
cd /etc/ssl

echo "Extracting cert file into multiple certs"
awk '{if ( $1 == "FILE_NAME:" ){FileName = $NF ;print $0 > FileName".txt";next};{print $0 >> FileName".txt"}}' /etc/ssl/certs.txt
if [ -f /etc/ssl/.txt ]; then rm /etc/ssl/.txt; fi

echo "removing cert file"
if [ -f /etc/ssl/certs.txt ]; then rm /etc/ssl/certs.txt; fi

echo "removing txt extension"
for f in $(find /etc/ssl -type f -name '*.txt'); do echo "file: $f"; cat ${f} | tail -n +2 > ${f%.txt}; rm ${f}; done

echo "changing directory back to $cwd"
cd $cwd

fi
