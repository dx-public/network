#!/bin/env bash

echo "Configuring IPTables"

whichalt(){
file=$1
found=0
pathdir=$PATH
if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"; fi 
for p in $(echo $pathdir | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found"==0 ]; then echo "$file not found"; return 1; fi
}

dohparse(){
domain=$1
type=$2
if [ -z "$type" ]; then type='A'; fi 
curl=$(whichalt curl)
data=$($curl "https://8.8.8.8/resolve?type=$type&name=$domain" 2>/dev/null)
parse=$(echo $data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print $1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[$0]++')
if [ "$type"=='A' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type"=='AAAA' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[$0]++' | awk '/./'; fi
#if [ ! -z "$parse" ]; then echo $parse; fi
}

check_installed(){
pkg=$1
if [ -z $pkg ]; then echo "please pass pkg"; exit; fi

if whichalt $pkg >/dev/null; then
 echo "$pkg exist"
else
 echo "installing $pkg"
 apt -y install $pkg
fi
checkpkg=$(whichalt $pkg)
if [ -z "$checkpkg" ]; then echo "please install $pkg manually"; exit; fi

}

check_installed iptables
iptables=$(whichalt iptables)

check_installed ip6tables
ip6tables=$(whichalt ip6tables)

check_installed ipset
ipset=$(whichalt ipset)

check_installed curl
curl=$(whichalt curl)

check_installed jq
jq=$(whichalt jq)

echo "load netfilter"
modprobe br_netfilter

echo "fix sysctl"
sysctl net.ipv4.conf.all.rp_filter=1
sysctl -n net.ipv4.conf.all.rp_filter

#if [ ! -d /etc/tasker ]; then mkdir /etc/tasker; fi
#echo "/etc/cron.hourly/update-ipset" > /etc/tasker/iptables.cron.hourly
#echo "/etc/cron.daily/update-iptables" > /etc/tasker/iptables.cron.daily
#chmod +x /etc/tasker/iptables.cron.hourly
#chmod +x /etc/tasker/iptables.cron.daily

#Disable iptables-restore
#sudo systemctl disable iptables-restore.service
#sudo systemctl stop iptables-restore.service
#sudo systemctl disable ip6tables-restore.service
#sudo systemctl stop ip6tables-restore.service

# Accept all traffic first to avoid ssh lockdown  via iptables firewall rules #
$iptables -P INPUT ACCEPT
$iptables -P FORWARD ACCEPT
$iptables -P OUTPUT ACCEPT

#Flush Chain(s)
$iptables -F INPUT
$iptables -F OUTPUT

#Setting up defaults
echo "Setting up Defaults"
$iptables -A INPUT -i lo -j ACCEPT
$iptables -A OUTPUT -o lo -j ACCEPT

#Allow valid traffic
$iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
$iptables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

echo "Block Portscans"
$iptables -A INPUT -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP
$iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
$iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
$iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
$iptables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
$iptables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP

echo "Block INVALIDS"
# Drop all invalid packets
$iptables -A INPUT -m state --state INVALID -j DROP
#$iptables -A FORWARD -m state --state INVALID -j DROP
$iptables -A OUTPUT -m state --state INVALID -j DROP

# Stop smurf attacks
$iptables -A INPUT -p icmp -m icmp --icmp-type address-mask-request -j DROP
$iptables -A INPUT -p icmp -m icmp --icmp-type timestamp-request -j DROP

# Reject spoofed packets
#$iptables -A INPUT -s 169.254.0.0/16 -j DROP
$iptables -A INPUT -s 224.0.0.0/4 -j DROP
$iptables -A INPUT -d 224.0.0.0/4 -j DROP
$iptables -A INPUT -s 240.0.0.0/5 -j DROP
$iptables -A INPUT -d 240.0.0.0/5 -j DROP
$iptables -A INPUT -s 0.0.0.0/8 -j DROP
$iptables -A INPUT -d 0.0.0.0/8 -j DROP
$iptables -A INPUT -d 239.255.255.0/24 -j DROP
$iptables -A INPUT -d 255.255.255.255 -j DROP


echo "Removing v4 IPSets"
$ipset list -o save | awk '/family inet / { print $2 }' | xargs -n 1 $ipset destroy

# Check that a set exists in bash:
$ipset -L dynamic_v4_ip_denied >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv4 set does not exist."
$ipset -N dynamic_v4_ip_denied nethash timeout 2147483
$iptables -I INPUT -m set --match-set dynamic_v4_ip_denied src -j DROP
fi

# Check that a set exists in bash:
$ipset -L dynamic_v4_ip_allowed >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv4 set does not exist."
$ipset -N dynamic_v4_ip_allowed nethash timeout 2147483
$iptables -I INPUT -m set --match-set dynamic_v4_ip_allowed src -j ACCEPT
fi

# Check that a set exists in bash:
$ipset -L dynamic_v4_ssh_allowed >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv4 set does not exist."
$ipset -N dynamic_v4_ssh_allowed nethash timeout 2147483
$iptables -I INPUT -m set --match-set dynamic_v4_ssh_allowed src -p tcp --dport 22 -j ACCEPT
fi


#Add Private
$ipset -A dynamic_v4_ip_allowed 10.0.0.0/8
$ipset -A dynamic_v4_ip_allowed 172.16.0.0/12
$ipset -A dynamic_v4_ip_allowed 192.168.0.0/16


#DNS
echo "Setting up IPv4 DNS Protection"
$iptables -N DNS53
$iptables -F DNS53

$iptables -A DNS53 -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j ACCEPT
$iptables -A DNS53 -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnsudplimit -j DROP
$iptables -A DNS53 -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j ACCEPT
$iptables -A DNS53 -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 32 --hashlimit-name dnstcplimit -j DROP
$iptables -A DNS53 -j DROP
$iptables -I INPUT -m set --match-set dynamic_v4_ip_allowed src -p tcp --dport 53 -m conntrack --ctstate NEW -j DNS53
$iptables -I INPUT -m set --match-set dynamic_v4_ip_allowed src -p udp --dport 53 -m conntrack --ctstate NEW -j DNS53


#Secure DoT
$iptables -N DOT-RATE-LIMIT
$iptables -F DOT-RATE-LIMIT
$iptables -A DOT-RATE-LIMIT -m hashlimit --hashlimit-mode srcip --hashlimit-upto 30/sec --hashlimit-burst 20 --hashlimit-name dot_conn_rate_limit --jump ACCEPT
$iptables -A DOT-RATE-LIMIT -m limit --limit 1/sec --jump LOG --log-prefix "IPTables-Ratelimited: "
$iptables -A DOT-RATE-LIMIT -j DROP
$iptables -I INPUT -p tcp --dport 853 -m conntrack --ctstate NEW -j DOT-RATE-LIMIT


echo "Setting up IPv4 Lists"
$iptables -N UPTIMEROBOT
$iptables -F UPTIMEROBOT
$iptables -A UPTIMEROBOT -p tcp -m multiport --dports 80,443,853,8443 -j ACCEPT
$iptables -A UPTIMEROBOT -p icmp -m icmp --icmp-type 8 -j ACCEPT
#curl -sS https://uptimerobot.com/inc/files/ips/IPv4.txt | wc --lines
if [ ! -f /configs/uptimerobot-ips-v4.txt ]; then
mkdir -p /configs
$curl -sS "https://uptimerobot.com/inc/files/ips/IPv4.txt" | awk 'gsub(/\r/, "") {print}' > /configs/uptimerobot-ips-v4.txt
fi
for x in $(cat /configs/uptimerobot-ips-v4.txt); do $iptables -A INPUT -s `echo ${x}/32` -j UPTIMEROBOT ; done
#for x in $(curl https://uptimerobot.com/inc/files/ips/IPv4.txt); do $iptables -A INPUT -s `echo ${x} | tr -d '\r'`/32 -j UPTIMEROBOT ; done

$iptables -N CLOUDFLARE
$iptables -F CLOUDFLARE
$iptables -A CLOUDFLARE -p tcp -m multiport --dports 80,443,8443 -j ACCEPT
if [ ! -f /configs/cloudflare-ips-v4.txt ]; then
mkdir -p /configs
$curl -sS "https://api.cloudflare.com/client/v4/ips" | $jq -r '.result.ipv4_cidrs | .[]' > /configs/cloudflare-ips-v4.txt
fi
for x in $(cat /configs/cloudflare-ips-v4.txt); do $iptables -A INPUT -s `echo ${x} | awk 'gsub(/\r/, "") {print}'` -j CLOUDFLARE ; done
#for x in $($curl https://www.cloudflare.com/ips-v4); do $iptables -A INPUT -s `echo ${x} | tr -d '\r'` -j CLOUDFLARE ; done


$iptables -A INPUT -j DROP
echo "Done setting up IPv4 Lists"


echo "Flushing all the ipv6 tables"
# Accept all traffic first to avoid ssh lockdown via iptables firewall rules #
$ip6tables -P INPUT ACCEPT
$ip6tables -P FORWARD ACCEPT
$ip6tables -P OUTPUT ACCEPT

# Flush All Iptables Chains/Firewall rules #
$ip6tables -F INPUT
$ip6tables -F OUTPUT

#NEW Related
$ip6tables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
$ip6tables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#Allow IPv6 Traffic via ICMP
$ip6tables -A INPUT -p ipv6-icmp -j ACCEPT

# Must allow loopback interface
$ip6tables -A INPUT -i lo -j ACCEPT

# Allow DHCPv6 from LAN only
$ip6tables -A INPUT -m state --state NEW -m udp -p udp -s fe80::/10 --dport 546 -j ACCEPT

echo "Removing v6 IPSets"
$ipset list -o save | awk '/family inet6 / { print $2 }' | xargs -n 1 $ipset destroy

echo "Setting up Dynamic IPv6 Lists"
# Check that a set exists in bash:
$ipset -L dynamic_v6_ssh_allowed >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv6 set does not exist."
$ipset -N dynamic_v6_ssh_allowed nethash timeout 2147483 family inet6
$ip6tables -I INPUT -m set --match-set dynamic_v6_ssh_allowed src -p tcp --dport 22 -j ACCEPT
fi

# Check that a set exists in bash:
$ipset -L dynamic_v6_ip_allowed >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv6 set does not exist."
$ipset -N dynamic_v6_ip_allowed nethash timeout 2147483 family inet6
$ip6tables -I INPUT -m set --match-set dynamic_v6_ip_allowed src -j ACCEPT
fi

# Check that a set exists in bash:
$ipset -L dynamic_v6_ip_denied >/dev/null 2>&1
if [ $? -ne 0 ]; then
echo "Block ipv6 set does not exist."
$ipset -N dynamic_v6_ip_denied nethash timeout 2147483 family inet6
$ip6tables -I INPUT -m set --match-set dynamic_v6_ip_denied src -j DROP
fi


echo "Block Portscans"
$ip6tables -A INPUT -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
$ip6tables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP


echo "Block INVALIDS"
# Drop all invalid packets
$ip6tables -A INPUT -m state --state INVALID -j DROP
#$ip6tables -A FORWARD -m state --state INVALID -j DROP
$ip6tables -A OUTPUT -m state --state INVALID -j DROP

echo "Setting up IPv6 DNS protection"
$ip6tables -A INPUT -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j ACCEPT
$ip6tables -A INPUT -m state --state NEW -m udp -p udp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnsudplimit -j DROP
$ip6tables -A INPUT -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-upto 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j ACCEPT
$ip6tables -A INPUT -m state --state NEW -m tcp -p tcp --dport 53 -m hashlimit --hashlimit-above 200/second --hashlimit-burst 225 --hashlimit-mode srcip --hashlimit-srcmask 128 --hashlimit-name dnstcplimit -j DROP

echo "Setting up IPv6 DOT protection"
$ip6tables -N DOT-RATE-LIMIT
$ip6tables -F DOT-RATE-LIMIT
$ip6tables -A DOT-RATE-LIMIT -m hashlimit --hashlimit-mode srcip --hashlimit-upto 30/sec --hashlimit-burst 20 --hashlimit-name dot_conn_rate_limit --jump ACCEPT
$ip6tables -A DOT-RATE-LIMIT -m limit --limit 1/sec --jump LOG --log-prefix "IPTables-Ratelimited: "
$ip6tables -A DOT-RATE-LIMIT -j DROP
$ip6tables -I INPUT -p tcp --dport 853 -m conntrack --ctstate NEW -j DOT-RATE-LIMIT

echo "Setting up IPv6 Lists"
$ip6tables -N UPTIMEROBOT
$ip6tables -F UPTIMEROBOT
$ip6tables -A UPTIMEROBOT -p tcp -m multiport --dports 80,443,853,8443 -j ACCEPT
$ip6tables -A UPTIMEROBOT -p icmpv6 --icmpv6-type echo-request -j ACCEPT
if [ ! -f /configs/uptimerobot-ips-v6.txt ]; then
mkdir -p /configs
$curl -sS "https://uptimerobot.com/inc/files/ips/IPv6.txt" > /configs/uptimerobot-ips-v6.txt
fi
for x in $(cat /configs/uptimerobot-ips-v6.txt); do $ip6tables -A INPUT -s `echo ${x}/128 | awk 'gsub(/\r/, "") {print}'` -j UPTIMEROBOT ; done
#for x in $(curl https://uptimerobot.com/inc/files/ips/IPv6.txt); do $ip6tables -A INPUT -s `echo ${x} | tr -d '\r'`/128 -j UPTIMEROBOT ; done

$ip6tables -N CLOUDFLARE
$ip6tables -F CLOUDFLARE
$ip6tables -A CLOUDFLARE -p tcp -m multiport --dports 80,443,8443 -j ACCEPT
if [ ! -f /configs/cloudflare-ips-v6.txt ]; then
mkdir -p /configs
$curl -sS "https://api.cloudflare.com/client/v4/ips" | $jq -r '.result.ipv6_cidrs | .[]' > /configs/cloudflare-ips-v6.txt
fi
for x in $(cat /configs/cloudflare-ips-v6.txt); do $ip6tables -A INPUT -s `echo ${x} | awk 'gsub(/\r/, "") {print}'` -j CLOUDFLARE ; done
#for x in $($curl https://www.cloudflare.com/ips-v6); do $ip6tables -A INPUT -s `echo ${x} | tr -d '\r'` -j CLOUDFLARE ; done

echo "Drop all the rest ipv6 input traffic"
$ip6tables -A INPUT -j DROP

#Add Trusted
mkdir -p /etc/cron.hourly
cat > /etc/cron.hourly/update-ipset <<EOF
#!/bin/env bash

whichalt(){
file=\$1
found=0
for p in \$(echo \$PATH | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "\${p}/\${file}" ]; then found=1; echo "\${p}/\${file}"; return 0; fi
 done
if [ "\$found"==0 ]; then echo "\$file not found"; return 1; fi
}

dohparse(){
domain=\$1
type=\$2
if [ -z "\$type" ]; then type='A'; fi
curl=\$(whichalt curl)
data=\$(\$curl "https://8.8.8.8/resolve?type=\$type&name=\$domain" 2>/dev/null)
parse=\$(echo \$data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print \$1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",\$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[\$0]++')
if [ "\$type"=='A' ]; then echo \$parse | awk '{printf "%s ", \$0}' | awk 'gsub(/ /," \n") { print }' | awk '{match(\$0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr(\$0,RSTART,RLENGTH); print ip}' | awk '!seen[\$0]++' | awk '/./'; fi
if [ "\$type"=='AAAA' ]; then echo \$parse | awk '{printf "%s ", \$0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[\$0]++' | awk '/./'; fi
#if [ ! -z "\$parse" ]; then echo \$parse; fi
}

ipset=\$(whichalt ipset)

echo "Querying Trusted IPs"
TrustedIPv4file='TrustedIPv4.txt'
TrustedIPv6file='TrustedIPv6.txt'
prisrvsuffix="a.\$(hostname -d)"
cat /dev/null > /\$TrustedIPv4file
cat /dev/null > /\$TrustedIPv6file
for seqnum in \`seq 1 40\`
do
        lookupsrv="\${seqnum}\${prisrvsuffix}"
        #dig A \$lookupsrv +short | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | tail -1 | grep -v '0.0.0.0' | tee -a /\$TrustedIPv4file
        #dig AAAA \$lookupsrv +short | grep -E "^([0-9a-fA-F]{0,4}:){1,7}([0-9a-fA-F]){0,4}$" | tail -1 | grep -v '^::' | tee -a /\$TrustedIPv6file
        dohparse \$lookupsrv A | awk 'NR<2{ print }' | awk '!/^0.0.0.0$/ { print }' | awk '/./' >> /\$TrustedIPv4file
        dohparse \$lookupsrv AAAA | awk 'NR<2{ print }' | awk '!/^::$/ { print }' | awk '/./' >> /\$TrustedIPv6file
        sleep 1
done
#Add Local Subnet
echo "Adding Local Subnet"
ip -o -f inet addr show |  awk '!/docker0/ { print }' | awk '/scope global/ {print \$4}' | awk '/^10\.|^172\.(1[6-9]|2[0-9]|3[0-1])\.|^192\.168\./ { print }' >> /\$TrustedIPv4file
ip -o -f inet6 addr show |  awk '!/docker0/ { print }' | awk '/scope global/ {print \$4}' >> /\$TrustedIPv6file
#Remove empty spaces
#sed -i '/^$/d' /\$TrustedIPv4file
#sed -i '/^$/d' /\$TrustedIPv6file
#Remove duplicates
#cat /\$TrustedIPv4file | sort -u > /\$TrustedIPv4file.temp
#cat /\$TrustedIPv6file | sort -u > /\$TrustedIPv6file.temp
# Using Awk to remove empty spaces and duplicates
awk '!seen[\$0]++' /\$TrustedIPv4file | awk '/./' > /\$TrustedIPv4file.temp
awk '!seen[\$0]++' /\$TrustedIPv6file | awk '/./' > /\$TrustedIPv6file.temp
#Move to Trused
mv /\$TrustedIPv4file.temp /\$TrustedIPv4file
mv /\$TrustedIPv6file.temp /\$TrustedIPv6file
cat /\$TrustedIPv4file | while read line; do \$ipset -D -! dynamic_v4_ip_allowed \$line; \$ipset -A dynamic_v4_ip_allowed \$line timeout 5100; done
cat /\$TrustedIPv6file | while read line; do \$ipset -D -! dynamic_v6_ip_allowed \$line; \$ipset -A dynamic_v6_ip_allowed \$line timeout 5100; done
EOF
chmod +x /etc/cron.hourly/update-ipset
#sed -i 's/\\\$/$/g' /etc/cron.hourly/update-ipset
sh /etc/cron.hourly/update-ipset

#run daily
mkdir -p /etc/cron.daily
cfile=`realpath $0`
cat > /etc/cron.daily/update-iptables <<EOF
#!/bin/env bash
echo "3" | sh $cfile
EOF
chmod +x /etc/cron.daily/update-iptables

#put in startup
mkdir -p /etc/cron.startup
cfile=`realpath $0`
cat > /etc/cron.startup/update-iptables <<EOF
#!/bin/env bash
echo "3" | sh $cfile
EOF
chmod +x /etc/cron.startup/update-iptables

#cleanup tmp
mkdir -p /etc/cron.weekly
cfile=`realpath $0`
cat > /etc/cron.weekly/cleanup-tmp <<EOF
#!/bin/env bash
cd /tmp
rm *.txt
EOF
chmod +x /etc/cron.weekly/cleanup-tmp

#create startup directory
#startupexists=`grep '@reboot' /etc/crontab`
if [ -z "$(awk '/@reboot/' /etc/crontab)" ]; then echo '@reboot root cd / && run-parts --report /etc/cron.startup' >> /etc/crontab; fi
