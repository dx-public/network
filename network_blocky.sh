#!/bin/env bash

whichalt(){
file=$1
found=0
#pathdir="$PATH"
#if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"; fi
pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"
for p in $(echo "$pathdir" | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found" = 0 ]; then echo "$file not found"; return 1; fi
}


download_prog() {
dinstalled="$(whichalt gocurl)"; dparams="";
if [ "$dinstalled" = "gocurl not found" ]; then dinstalled="$(whichalt curl)"; dparams="-Ls"; fi
if [ "$dinstalled" = "curl not found" ]; then dinstalled="$(whichalt wget)"; dparams="-q -O -"; fi
if [ "$dinstalled" = "wget not found" ]; then echo "please install wget, curl, or gocurl to continue installing..."; return 1; exit; fi
echo "$dinstalled $dparams"
return 0
}

install_package() {
if [ ! -z "$1" ]; then arg1="$1"; else echo "Please fix the install_package arg"; return 1; fi #get package id
if [ ! -f /etc/${arg1}/${arg1} ]; then

  os=$(uname | awk '{print tolower($0)}')
  arch=$(uname -m | awk '{print tolower($0)}')
  awkfilter="/htt/ && /$os/ && ! /sha|sig|pem|deb|sbom/"
  if [ "$arch"=='x86_64' ] || [ "$arch"=='amd64' ]; then awkfilter="/amd64|x86_64/ && $awkfilter"; fi

  if [ ! -f /tmp/pkgrepo.json ]; then
  pkgrepo=""
  domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
  pkgrepo=$($(download_prog) "https://8.8.8.8/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
  if [ -z "$pkgrepo" ]; then pkgrepo=$($(download_prog) "https://[2001:4860:4860::8888]/resolve?name=pkgrepo.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}'); fi
  if [ -z "$pkgrepo" ]; then pkgrepo="https://api.github.com/repos/<author>/<repo>/releases/latest"; echo "pkg repo not found in pkgrepo.${domain}... Please update it to ${pkgrepo}"; return 1; fi
  $(download_prog) "$pkgrepo" > /tmp/pkgrepo.json 2>/dev/null
  fi
  _BINARY=`cat /tmp/pkgrepo.json | awk '/'"\"${arg1}\""':/,/description/' | awk ''"$awkfilter"'{ print $2 }''' || echo 'path to binary url'`
  _BINARY=$(echo $_BINARY | awk '{gsub(/[",]/,""); print}')
  if [ -z "$_BINARY" ]; then return 1; fi
  #echo "${arg1} not installed. downloading from $_BINARY"
  #curl -Ls "$_BINARY"
  echo "$_BINARY"
fi
return 0
}

if [ ! -f /etc/gocurl/gocurl ]; then
_BINARY=`install_package gocurl || echo 'https://bafybeifecpjncsgrvdvqqfevq6eqjqogdv4m3hty2zle5ygweqpndz6uuu.ipfs.dweb.link/gocurl-linux-amd64'`
$(download_prog) $_BINARY > /tmp/gocurl
mkdir -p /etc/gocurl
mv /tmp/gocurl /etc/gocurl/.
if [ -f /etc/gocurl/gocurl ]; then chmod +x /etc/gocurl/gocurl; fi
fi

echo 'setting up blocky'

if [ ! -f /etc/blocky/blocky ]; then
#_BINARYALT=`/etc/gocurl/gocurl https://api.github.com/repos/0xERR0R/blocky/releases/latest | awk '/download_url/ && /Linux_x86_64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' || echo 'https://github.com/0xERR0R/blocky/releases/download/v0.23/blocky_0.23_Linux_x86_64.tar.gz'`
_BINARY=$(install_package blocky || $($(download_prog) https://api.github.com/repos/0xERR0R/blocky/releases/latest | awk '/download_url/ && /Linux_x86_64/ && ! /sha|sig|pem|deb|sbom/ { print $2 }' || echo 'https://github.com/0xERR0R/blocky/releases/download/v0.23/blocky_0.23_Linux_x86_64.tar.gz'))
/etc/gocurl/gocurl "$(echo "$_BINARY" | awk '{gsub(/^[ \t"]+|[ \t"]+$/,"");print}')" > /tmp/blocky.tar.gz
mkdir -p /etc/blocky
tar xvf /tmp/blocky.tar.gz
rm README.md
rm LICENSE
mv blocky /etc/blocky/.
touch /etc/blocky/config.yml
touch /etc/blocky/whitelist.txt
fi

if [ ! -f /etc/goip/goip ]; then
_BINARY=`install_package goip || echo 'https://bafybeidzspaknaotcpfgnkmdasgkubrtpzhc6uvrva7qtlovxrg3geomyu.ipfs.dweb.link/goip_v1.0.0_linux_amd64'`
/etc/gocurl/gocurl $_BINARY > /tmp/goip
mkdir -p /etc/goip
mv /tmp/goip /etc/goip/.
if [ -f /etc/goip/goip ]; then chmod +x /etc/goip/goip; fi
fi

if [ ! -f /etc/godig/godig ]; then
_BINARY=`install_package godig || echo 'https://bafybeifvl67mmepkokgxdgqopleug2ktxlq3y2geuzy43cgeq3ix2c5zvi.ipfs.dweb.link/godig-linux-amd64-v0.2'`
/etc/gocurl/gocurl $_BINARY > /tmp/godig
mkdir -p /etc/godig
mv /tmp/godig /etc/godig/.
if [ -f /etc/godig/godig ]; then chmod +x /etc/godig/godig; fi
fi


if [ ! -d /var/log/blocky ]; then mkdir -p /var/log/blocky; chmod +w /var/log/blocky; fi

#if [ ! -f '/etc/blocky/bootstrapconfig.yml' ]; then
cat > /etc/blocky/bootstrapconfig.yml <<_EOF_
upstreams:
  init:
    strategy: fast
  groups:
    default:
      - https://1.1.1.1:443/dns-query
      - https://8.8.8.8:443/dns-query
      - https://9.9.9.9:443/dns-query
      - https://ipv4-anycast.dns1.nextdns.io:443/dns-query
      - https://[2606:4700:4700::1111]:443/dns-query
      - https://[2001:4860:4860::8888]:443/dns-query
      - https://[2620:fe::9]:443/dns-query
      - https://ipv6-anycast.dns1.nextdns.io:443/dns-query
      # - https://custom-1 #custom-1
      # - https://custom-2 #custom-2
      # - https://custom-3 #custom-3
      # - https://custom-4 #custom-4
      # - [2606:4700:4700::1111]:53
      # - 1.1.1.2:53
      # - tcp-tls:1.1.1.2:853
    clientlaptop*:
      - https://1.1.1.2:443/dns-query
      - https://1.0.0.2:443/dns-query
      - https://[2606:4700:4700::1112]:443/dns-query
      - https://[2606:4700:4700::1002]:443/dns-query
      - 1.1.1.2:53
  strategy: strict
  timeout: 2s
#  userAgent:
connectIPVersion: dual
customDNS:
  customTTL: 1h
  filterUnmappedTypes: true
#  rewrite:
#    example.com: printer.lan
  mapping:
#    example.com: ::1,127.0.0.1
    statichosts.com: 127.0.0.1
#conditional:
#  fallbackUpstream: false
#  rewrite:
#    example.com: fritz.box
#  mapping:
#    fritz.box: 192.168.178.1
#    lan.net: 192.168.178.1,192.168.178.2
#blocking:
#  denylists:
#    ads:
#      - https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt
#      - https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
#      - http://sysctl.org/cameleon/hosts
#      - https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt
#      - |
#        someadsdomain.com
#    special:
#      - https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts
#  allowlists:
#    ads:
#      - whitelist.txt
#      - |
#        whitelistdomain.com
#        /^banners?[_.-]/
#  clientGroupsBlock:
#    default:
#      - ads
#      - special
#    laptop*:
#      - ads
#    192.168.178.1/24:
#      - special
#  blockType: zeroIp #nxDomain,0.0.0.0, ::
#  blockTTL: 30m
# optional: Configure how lists, AKA sources, are loaded
#  loading:
#    # optional: list refresh period in duration format.
#    # Set to a value <= 0 to disable.
#    # default: 4h
#    refreshPeriod: 24h
#    # optional: Applies only to lists that are downloaded (HTTP URLs).
#    downloads:
#      # optional: timeout for list download (each url). Use large values for big lists or slow internet connections
#      # default: 5s
#      timeout: 60s
#      # optional: Maximum download attempts
#      # default: 3
#      attempts: 5
#      # optional: Time between the download attempts
#      # default: 500ms
#      cooldown: 10s
#    # optional: Maximum number of lists to process in parallel.
#    # default: 4
#    concurrency: 16
#    # Configure startup behavior.
#    # accepted: blocking, failOnError, fast
#    # default: blocking
#    strategy: failOnError
#    # Number of errors allowed in a list before it is considered invalid.
#    # A value of -1 disables the limit.
#    # default: 5
#    maxErrorsPerSource: 5
#custom-blocking-block
caching:
  minTime: 5m
  maxTime: 30m
  maxItemsCount: 0
  prefetching: false
  prefetchExpires: 2h
  prefetchThreshold: 5
  prefetchMaxItemsCount: 0
  cacheTimeNegative: 30m
#clientLookup:
#  upstream: 192.168.178.1
#  singleNameOrder:
#    - 2
#    - 1
#  clients:
#    laptop:
#      - 192.168.178.29
#prometheus:
#  enable: true
#  path: /metrics
queryLog:
  type: csv #mysql,postgresql,csv-client,empty(to console)
  target: /var/log/blocky
  #postgresql target: postgres://user:password@db_host_or_ip:5432/db_name
  logRetentionDays: 3
  creationAttempts: 1
  creationCooldown: 2s
#redis:
#  address: redis:6379
#  password: passwd
#  database: 2
#  required: false
#  connectionAttempts: 10
#  connectionCooldown: 3s
# optional: ports configuration
ports:
  # optional: DNS listener port(s) and bind ip address(es), default port53 (UDP and TCP). Example:53, :53, "127.0.0.1:5353,[::1]:5353"
  dns: 53
  # optional: Port(s) and bind ip address(es) for DoT (DNS-over-TLS) listener. Example: 853, 127.0.0.1:853
  tls: 8853
  # optional: Port(s) and optional bind ip address(es) to serve HTTPS used for prometheus metrics, pprof, REST API, DoH... If you wish to specify a specific IP, you can do so such as 192.168.0.1:443. Example: 443, :443, 127.0.0.1:443,[::1]:443
  https: 8443
  # optional: Port(s) and optional bind ip address(es) to serve HTTP used for prometheus metrics, pprof, REST API, DoH... If you wish to specify a specific IP, you can do so such as 192.168.0.1:4000. Example: 4000, :4000, 127.0.0.1:4000,[::1]:4000
  http: 8080
minTlsServeVersion: 1.2
certFile: /etc/blocky/server.crt
keyFile: /etc/blocky/server.key
bootstrapDns:
  - tcp+udp:127.0.0.1:453
  - tcp+udp:::1:653
#  - upstream: https://one.one.one.one/dns-query
#    ips:
#      - 1.1.1.1
#  - upstream: https://one.one.one.one/dns-query
#    ips:
#      - 1.0.0.1
#  - upstream: https://one.one.one.one/dns-query
#    ips:
#      - 2606:4700:4700::1111
#  - upstream: https://one.one.one.one/dns-query
#    ips:
#      - 2606:4700:4700::1001
#  - upstream: https://dns.google/dns-query
#    ips:
#      - 8.8.8.8
#  - upstream: https://dns.google/dns-query
#    ips:
#      - 8.8.4.4
#  - upstream: https://dns.google/dns-query
#    ips:
#      - 2001:4860:4860::8888
#  - upstream: https://dns.google/dns-query
#    ips:
#      - 2001:4860:4860::8844
#  - upstream: https://dns9.quad9.net/dns-query
#    ips:
#      - 9.9.9.9
#  - upstream: https://dns9.quad9.net/dns-query
#    ips:
#      - 149.112.112.9
#  - upstream: https://dns9.quad9.net/dns-query
#    ips:
#      - 2620:fe::9
#  - upstream: https://dns9.quad9.net/dns-query
#    ips:
#      - 2620:fe::fe:9
#  - upstream: https://ipv4-anycast.dns1.nextdns.io
#    ips:
#      - 45.90.28.0
#  - upstream: https://ipv4-anycast.dns2.nextdns.io
#    ips:
#      - 45.90.30.0
#  - upstream: https://ipv6-anycast.dns1.nextdns.io
#    ips:
#      - 2a07:a8c0::0
#  - upstream: https://ipv6-anycast.dns2.nextdns.io
#    ips:
#      - 2a07:a8c1::0
#filtering:
#  queryTypes:
#    - AAAA
# optional: if path defined, use this file for query resolution (A, AAAA and rDNS). Default: empty
hostsFile:
  # optional: Hosts files to parse
  sources:
    - /etc/hosts
  #  - https://example.com/hosts
  #  - |
  #    # inline hosts
  #    127.0.0.1 example.com
  # optional: TTL, default: 1h
  hostsTTL: 1h
  # optional: Whether loopback hosts addresses (127.0.0.0/8 and ::1) should be filtered or not
  # default: false
  filterLoopback: false
  # optional: Configure how sources are loaded
  loading:
    # optional: file refresh period in duration format.
    # Set to a value <= 0 to disable.
    # default: 4h
    refreshPeriod: 4h
    # optional: Applies only to files that are downloaded (HTTP URLs).
    downloads:
      # optional: timeout for file download (each url). Use large values for big files or slow internet connections
      # default: 5s
      timeout: 5s
      # optional: Maximum download attempts
      # default: 3
      attempts: 3
      # optional: Time between the download attempts
      # default: 500ms
      cooldown: 10s
    # optional: Maximum number of files to process in parallel.
    # default: 4
    concurrency: 4
    # Configure startup behavior.
    # accepted: blocking, failOnError, fast
    # default: blocking
    strategy: fast
    # Number of errors allowed in a file before it is considered invalid.
    # A value of -1 disables the limit.
    # default: 5
    maxErrorsPerSource: 5
# optional: logging configuration
log:
  # optional: Log level (one from trace, debug, info, warn, error). Default: info
  level: info
  # optional: Log format (text or json). Default: text
  format: text
  # optional: log timestamps. Default: true
  timestamp: true
  # optional: obfuscate log output (replace all alphanumeric characters with *) for user sensitive data like request domains or responses to increase privacy. Default: false
  privacy: false
# optional: add EDE error codes to dns response
ede:
  # enabled if true, Default: false
  enable: false
# optional: configure optional Special Use Domain Names (SUDN)
specialUseDomains:
  # optional: block recomended private TLDs
  # default: true
  rfc6762-appendixG: true
# optional: configure extended client subnet (ECS) support
ecs:
  # optional: if the request ecs option with a max sice mask the address will be used as client ip
  useAsClient: true
  # optional: if the request contains a ecs option it will be forwarded to the upstream resolver
  forward: true
_EOF_
#fi

cp /etc/blocky/bootstrapconfig.yml /etc/blocky/template-yml.txt

#ipv4 only bootstrap resolver server
cat > /etc/blocky/ipv4.yml << '_EOF_'
upstreams:
  init:
    strategy: fast
  groups:
    default:
      - https://one.one.one.one/dns-query
      - https://dns.google/dns-query
      - https://dns9.quad9.net/dns-query
      - https://ipv4-anycast.dns1.nextdns.io
      - https://ipv4-anycast.dns2.nextdns.io
  strategy: strict
connectIPVersion: v4
filtering:
  queryTypes:
    - AAAA
ports:
  dns: 453
bootstrapDns:
  - upstream: https://one.one.one.one/dns-query
    ips:
      - 1.1.1.1
  - upstream: https://one.one.one.one/dns-query
    ips:
      - 1.0.0.1
  - upstream: https://dns.google/dns-query
    ips:
      - 8.8.8.8
  - upstream: https://dns.google/dns-query
    ips:
      - 8.8.4.4
  - upstream: https://dns9.quad9.net/dns-query
    ips:
      - 9.9.9.9
  - upstream: https://dns9.quad9.net/dns-query
    ips:
      - 149.112.112.9
  - upstream: https://ipv4-anycast.dns1.nextdns.io
    ips:
      - 45.90.28.0
  - upstream: https://ipv4-anycast.dns2.nextdns.io
    ips:
      - 45.90.30.0
#log:
#  level: trace
_EOF_

#ipv6 only bootstrap resolver server
cat > /etc/blocky/ipv6.yml << '_EOF_'
upstreams:
  init:
    strategy: fast
  groups:
    default:
      - https://one.one.one.one/dns-query
      - https://dns.google/dns-query
      - https://dns9.quad9.net/dns-query
      - https://ipv6-anycast.dns1.nextdns.io
      - https://ipv6-anycast.dns2.nextdns.io
  strategy: strict
connectIPVersion: v6
filtering:
  queryTypes:
    - A
ports:
  dns: 653
bootstrapDns:
  - upstream: https://one.one.one.one/dns-query
    ips:
      - 2606:4700:4700::1111
  - upstream: https://one.one.one.one/dns-query
    ips:
      - 2606:4700:4700::1001
  - upstream: https://dns.google/dns-query
    ips:
      - 2001:4860:4860::8888
  - upstream: https://dns.google/dns-query
    ips:
      - 2001:4860:4860::8844
  - upstream: https://dns9.quad9.net/dns-query
    ips:
      - 2620:fe::9
  - upstream: https://dns9.quad9.net/dns-query
    ips:
      - 2620:fe::fe:9
  - upstream: https://ipv6-anycast.dns1.nextdns.io
    ips:
      - 2a07:a8c0::0
  - upstream: https://ipv6-anycast.dns2.nextdns.io
    ips:
      - 2a07:a8c1::0
#log:
#  level: trace
_EOF_




domain=$(hostname -f | awk -F'.' '{ $1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')
certname=$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=certname.ssl.${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",$0); print $0}' | awk '// {gsub(/,/, "\n"); print}')
if [ ! -z "$certname" ]; then
 if [ -f "/etc/ssl/$(echo $certname).crt" ]; then
  echo "copying good cert to blocky"
  cp /etc/ssl/$(echo $certname).crt /etc/blocky/server.crt
  cp /etc/ssl/$(echo $certname).key /etc/blocky/server.key
 fi
fi

#dummy cert for the dns server to run if not found (recommend changing it)
if [ ! -f /etc/blocky/server.crt ]; then
cat > /etc/blocky/server.crt <<_EOF_
-----BEGIN CERTIFICATE-----
MIIDAjCCAeoCCQCptj0+TjjIJjANBgkqhkiG9w0BAQsFADBDMREwDwYDVQQKDAhE
TlNDcnlwdDEaMBgGA1UECwwRTG9jYWwgdGVzdCBzZXJ2ZXIxEjAQBgNVBAMMCWxv
Y2FsaG9zdDAeFw0xOTExMTgxNDA2MzBaFw0zMzA3MjcxNDA2MzBaMEMxETAPBgNV
BAoMCEROU0NyeXB0MRowGAYDVQQLDBFMb2NhbCB0ZXN0IHNlcnZlcjESMBAGA1UE
AwwJbG9jYWxob3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2+4O
hEIW328ve5OKDG2U83Ytk1hQbO6iv0MIDrvi+hITRzztWRFNvb9ADxQRSTuc8sgJ
PW2fZPbTwWwrlVSPYxFHyx91AoxEDxulyNsPvrlYdMgC73P7rqwr0R1ZZXv1e3AK
IpJ3XNkoxEUFAC2wAiKsA+YNBe+wWMHLK3geBh9ud/0ekNvwiWeIzRz45KHDiYla
93owNJJKDTF6RlF1nK1VZNtDNgNQnjuxwLpS0XJJRHaBxN7+OqY6dImStBfS8mV9
VNapfuEC94kLbaGeLTIPN5RR0reBvT66SIc16/VuDVvNtn2kr6yMMDfyWyLVJkWu
5/9/jJLiUCBhjQ2slwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQA6Vz5HnGuy8jZz
5i8ipbcDMCZNdpYYnxgD53hEKOfoSv7LaF0ztD8Kmg3s5LHv9EHlkK3+G6FWRGiP
9f6IbtRITaiVQP3M13T78hpN5Qq5jgsqjR7ZcN7Etr6ZFd7G/0+mzqbyBuW/3szt
RdX/YLy1csvjbZoNNuXGWRohXjg0Mjko2tRLmARvxA/gZV5zWycv3BD2BPzyCdS9
MDMYSF0RPiL8+alfwLNqLcqMA5liHlmZa85uapQyoUI3ksKJkEgU53aD8cYhH9Yn
6mVpsrvrcRLBiHlbi24QBolhFkCSRK8bXes8XDIPuD8iYRwlrVBwOakMFQWMqNfI
IMOKJomU
-----END CERTIFICATE-----
_EOF_
cat > /etc/blocky/server.key <<_EOF_
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDb7g6EQhbfby97
k4oMbZTzdi2TWFBs7qK/QwgOu+L6EhNHPO1ZEU29v0APFBFJO5zyyAk9bZ9k9tPB
bCuVVI9jEUfLH3UCjEQPG6XI2w++uVh0yALvc/uurCvRHVlle/V7cAoikndc2SjE
RQUALbACIqwD5g0F77BYwcsreB4GH253/R6Q2/CJZ4jNHPjkocOJiVr3ejA0kkoN
MXpGUXWcrVVk20M2A1CeO7HAulLRcklEdoHE3v46pjp0iZK0F9LyZX1U1ql+4QL3
iQttoZ4tMg83lFHSt4G9PrpIhzXr9W4NW822faSvrIwwN/JbItUmRa7n/3+MkuJQ
IGGNDayXAgMBAAECggEBANs0fmGSocuXvYL1Pi4+9qxnCOwIpTi97Zam0BwnZwcL
Bw4FCyiwV4UdX1LoFIailT9i49rHLYzre4oZL6OKgdQjQCSTuQOOHLPWQbpdpWba
w/C5/jr+pkemMZIfJ6BAGiArPt7Qj4oKpFhj1qUj5H9sYXkNTcOx8Fm25rLv6TT9
O7wg0oCpyG+iBSbCYBp9mDMz8pfo4P3BhcFiyKCKeiAC6KuHU81dvuKeFB4XQK+X
no2NqDqe6MBkmTqjNNy+wi1COR7lu34LPiWU5Hq5PdIEqBBUMjlMI6oYlhlgNTdx
SvsqFz3Xs6kpAhJTrSiAqscPYosgaMQxo+LI26PJnikCgYEA9n0OERkm0wSBHnHY
Kx8jaxNYg93jEzVnEgI/MBTJZqEyCs9fF6Imv737VawEN/BhesZZX7bGZQfDo8AT
aiSa5upkkSGXEqTu5ytyoKFTb+dJ/qmx3+zP6dPVzDnc8WPYMoUg7vvjZkXXJgZX
+oMlMUW1wWiDNI3wP19W9Is6xssCgYEA5GqkUBEns6eTFJV0JKqbEORJJ7lx5NZe
cIx+jPpLkILG4mOKOg1TBx0wkxa9cELtsNsM+bPtu9OqRMhsfPBmsXDHhJwg0Z6G
eDTfYYPkpRhwZvl6jBZn9sLVR9wfg2hE+n0lfV3mceg336KOkwAehDU84SWZ2e0S
esqkpbHJa+UCgYA7PY0O8POSzcdWkNf6bS5vAqRIdSCpMjGGc4HKRYSuJNnJHVPm
czNK7Bcm3QPaiexzvI4oYd5G09niVjyUSx3rl7P56Y/MjFVau+d90agjAfyXtyMo
BVtnAGGnBtUiMvP4GGT06xcZMnnmCqpEbBaZQ/7N8Bdwnxh5sqlMdtX2hwKBgAhL
hyQRO2vezgyVUN50A6WdZLq4lVZGIq/bqkzcWhopZaebDc4F5doASV9OGBsXkyI1
EkePLTcA/NH6pVX0NQaEnfpG4To7k46R/PrBm3ATbyGONdEYjzX65VvytoJDKx4d
pVrkKhZA5KaOdLcJ7hHHDSrv/qJXZbBn44rQ5guxAoGBAJ6oeUsUUETakxlmIhmK
xuQmWqLf97BKt8r6Z8CqHKWK7vpG2OmgFYCQGaR7angQ8hmAOv6jM56XhoagDBoc
UoaoEyo9/uCk6NRUkUMj7Tk/5UQSiWLceVH27w+icMFhf1b7EmmNfk+APsiathO5
j4edf1AinVCPwRVVu1dtLL5P
-----END PRIVATE KEY-----
_EOF_
fi

#ip6=$(curl -s -6 https://ifconfig.co | awk -F: '{ print "fe80:"$1":"$2":"$3":"$4":"$5":"$6":"$7 }')
#ip4=$(curl -s -4 https://ifconfig.co | awk -F. '{ print "127."$2"."$3"."$4 }')

#ip6=$(curl -s -6 https://ifconfig.co)
#ip4=$(curl -s -4 https://ifconfig.co)

ip4=$(/etc/goip/goip | awk '!/pointtopoint|lo|Total|docker/' | awk '{ print $1,$2,$3 }' | awk '/\./' | awk -F '\n' '{if(NR==1) {print $1}}' | awk -F '=' '{print $4}' | awk -F '/' '{print $1}')
ip6=$(/etc/goip/goip | awk '!/pointtopoint|lo|Total|docker/' | awk '{ print $1,$2,$3 }' | awk '/:/' | awk -F '\n' '{if(NR==1) {print $1}}' | awk -F '=' '{print $4}' | awk -F '/' '{print $1}')

if [ ! -z "$ip6" ]; then ipdns="${ip6}"; fi
if [ ! -z "$ip4" ]; then ipdns="${ip4}"; fi
if [ ! -z "$ip6" ] && [ ! -z "ip4" ]; then ipdns="${ip6},${ip4}"; fi
if [ -z "$ip6" ] && [ -z "$ip4" ]; then ipdns="fe80::1"; fi

cat > /etc/blocky/refresh-dns.sh <<_EOF_
#!/bin/sh

getnetstat() {
lport=\$1
ports=""
protos="tcp udp tcp6 udp6"
for proto in \$protos
do
#echo \$proto
port=\$(awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2));
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
}
NR > 1 {{if(NR==2)print "";local=getIP(\$2);remote=getIP(\$3)}{print local"-"remote}}' /proc/net/\$proto)
ports="\$ports \$port"
#echo "ports:\$ports"
#echo "port:\$port"
done
ports=\$(echo "\$ports" | awk 'NF > 0' | awk '!a[\$0]++' | awk '/0:'\$lport'-/')

#| awk 'NF > 0' | awk '{print length(\$0)}')
if [ ! -z "\$ports" ]; then
 #echo "port \$lport is listening...changing default"
 return 0
else
 #echo "port \$lport not found...keeping default"
 return 1
fi

}

if [ ! -p /dev/stdin ]; then
  echo -n "Running network_blocky.sh - Refresh block/allow lists (y/N) "
  read ans;

  case \$ans in
      y|Y)
          refresh="y"; echo "Refreshing";;
      n|N)
          refresh="n"; echo "No Update.";;
      *)
          refresh="n"; echo "";;
  esac
fi

if [ -p /dev/stdin ]; then
  while read LINE; do
      refresh="\$LINE"
  done < /dev/stdin
fi

if [ -z "\$(awk '{ print }' /etc/blocky/config.yml)" ]; then refresh=y; fi

#Get day
day=\$( date +%d )

#if [ "\$day" = "02" ] || [ "\$refresh" = "y" ]; then
if [ "\$refresh" = "y" ]; then

echo "refreshing dns"

cp /etc/blocky/bootstrapconfig.yml /etc/blocky/config.yml

domain=\$(hostname -f | awk -F'.' '{ \$1=""; print}' | awk '// {gsub(/ /, "."); print}' | awk '// {sub(/./, ""); print}')

echo "converting static hosts"

#txt record to file
#dig TXT +short statichosts.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0' > /etc/blocky/statichosts.txt
/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=statichosts.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' > /etc/blocky/statichosts.txt

#combine multiple same lines to one and sort
awk '{ line[NR] = \$0 } END { do { haschanged = 0; for(i=1; i < NR; i++) { if ( line[i] > line[i+1] ) { t = line[i]; line[i] = line[i+1]; line[i+1] = t; haschanged = 1 } } } while ( haschanged == 1 ); for(i=1; i <= NR; i++) { print line[i] } }' /etc/blocky/statichosts.txt | awk '!a[\$0]++' | awk -v ORS=',' 'BEGIN{prev_field = ""}
{
  if (prev_field == \$1) {
    printf ","\$2
  } else {
    printf "\n"\$0
  }
  prev_field = \$1
}' | awk 'NF > 0' > /etc/blocky/statichosts.tmp && mv /etc/blocky/statichosts.tmp /etc/blocky/statichosts.txt

#replace statichosts in config
awk -v fix="\$(awk '{ print "    "\$1":",\$2 }' /etc/blocky/statichosts.txt)" '/statichosts.com: 127.0.0.1/ {\$0 = fix}1' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml

#replace server.dns record with sudo ipv6

awk '// {gsub(/fe80::1/, "$ipdns"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml

#grabbing custom servers
#dnss=\$(dig TXT +short dns.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"|\\|\[|\]|\{|\}/, ""); print}' | awk 'NF > 0' |  awk '/doh/ {print \$2}' | awk '// {gsub(/\\\/, ""); print}' )
dnss=\$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=dns.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' | awk '/doh/ {print \$2}' | awk '// {gsub(/\\\/, ""); print}')


if [ ! -z "\$dnss" ]; then

#comment out upstream:
#awk '// {sub(/upstream:/, "#upstream:"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml

#comment out default:
#awk '// {sub(/default:/, "#default:"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml

#comment out current default servers
awk '// {sub(/^      - https/, "      # - https"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml

i=0
for dns in \$(printf '%s\n' "\$dnss")
do
i=\$((i+1))

dns=\$(echo \$dns | awk '{gsub(/\\\/,"");print}')
sdns="      - https://\${dns} #custom-\${i}"
#echo "dns \$i: \$dns"

echo "current setting of custom \$i"
currentsdns=\$(awk '/custom-'\$i'/{print}' /etc/blocky/config.yml)
echo "\$currentsdns"

echo "replacing current setting: \$currentsdns with new setting: \$sdns"
awk '/custom-'\$i'/{ rl = NR + 0 } NR == rl { gsub(/.*/,"'"\$sdns"'");}1' /etc/blocky/config.yml > /etc/blocky/config.yml.tmp && mv /etc/blocky/config.yml.tmp /etc/blocky/config.yml

#echo "enabling server"
#awk '/static.'"'"'custom-'\$i'/{ rl = NR + 0 } NR == rl { gsub( /#/,"") } 1' /etc/blocky/bootstrapconfig.yml

done

if [ -f "/etc/blocky/config.yml.tmp" ]; then rm /etc/blocky/config.yml.tmp; fi

fi

echo "querying for blocklist"
#lists=\$(dig TXT +short blocklists.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')
blists=\$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=blocklists.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/\\\/, ""); print}')

if [ ! -z "\$blists" ]; then
 echo "blocklists found adding to config"
 if [ ! -d "/etc/blocky/lists" ]; then mkdir -p /etc/blocky/lists; fi
 #cleaning lists
 #rm /etc/blocky/lists/*.txt

echo "\$(awk '/^upstreams:/,/^blocking:/' /etc/blocky/config.yml | awk '{sub(/blocking:/,"");print}' | awk 'NF > 0')" > /etc/blocky/config.yml
 echo "blocking:" >> /etc/blocky/config.yml
 echo "  denylists:" >> /etc/blocky/config.yml

for blist in \$(printf '%s\n' \$blists)
 do
  echo "getting urls for \$blist"
  echo "    \${blist}:" >> /etc/blocky/config.yml
  #urls=\$(dig TXT +short \${list}.blocklists.\$(echo \${domain}) | awk '// {gsub(/" "/, ""); print}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/"/, ""); print}' | awk 'NF > 0')
  burls=\$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=\${blist}.blocklists.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/\\\/, ""); print}')
  #i=0
   for burl in \$(printf '%s\n' \$burls)
    do
    if [ "\$(echo \$burl | awk '/^file:/ { print "found" }')" = "found" ]; then
      list=\$(echo \$burl | awk '// {gsub (/file:/, ""); print}')
      #echo "downloading \$list"
      #echo "found file"
      fileurl=\$(echo \$list | awk '// {gsub (/\/|\.|:|&|=|;/, "_"); print}')
      #curl -sL \$list > /etc/blocky/lists/\$fileurl
      /etc/gocurl/gocurl \$list > /etc/blocky/lists/\$fileurl
      #echo "fixing file"
      awk '{print}' /etc/blocky/lists/\$fileurl | awk '// {gsub(/127.0.0.1|0.0.0.0|::1|\t| |\*./, ""); print}' | awk '// {gsub(/^localhost$|^ip6-localhost$|^localhost.localdomain$/, "");print}' | awk '!/^#/{gsub(/#.*/,"",\$0);print}' | awk '!/^<|^\||>$|\]|\[|^\-/' | awk '/\./' | awk 'NF > 0' > /etc/blocky/lists/\$fileurl.txt.tmp && mv /etc/blocky/lists/\$fileurl.txt.tmp /etc/blocky/lists/\$fileurl
      echo "adding /etc/blocky/lists/\$fileurl to conf under \${blist} from \$list"
      echo "      - /etc/blocky/lists/\$fileurl" >> /etc/blocky/config.yml
    else
     echo "adding \${burl} to conf under \${blist}"
     echo "      - \${burl}" >> /etc/blocky/config.yml
     #i=\$((i+1))
     #echo "curling \$burl to /etc/blocky/lists/\$blist-\$i.txt"
     #if [ ! -f /etc/blocky/lists/\$blist-\$i.txt ]; then curl -s \$burl > /etc/blocky/lists/\$blist-\$i.txt; fi
     #awk '{print}' /etc/blocky/lists/blacklists-\$blist-\$i.txt | awk '// {gsub(/127.0.0.1|0.0.0.0|::1|\t| |\*./, ""); print}' | awk '// {gsub(/^localhost$|^ip6-localhost$|^localhost.localdomain$/, ""); print}' | awk '!/^#/{gsub(/#.*/,"",\$0);print}' | awk 'NF > 0' > /etc/blocky/lists/\$list-\$i.txt.tmp && mv /etc/blocky/lists/\$list-\$i.txt.tmp /etc/blocky/lists/\$list-\$i.txt
    fi
    done
done

echo "querying for allowlist"
alists=\$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=allowlists.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/\\\/, ""); print}')

if [ ! -z "\$alists" ]; then
 echo "allowlists found adding to config"
 if [ ! -d "/etc/blocky/lists" ]; then mkdir -p /etc/blocky/lists; fi
 echo "  allowlists:" >> /etc/blocky/config.yml

 for alist in \$(printf '%s\n' \$alists)
  do
   echo "getting urls for \$alist"
   echo "    \${alist}:" >> /etc/blocky/config.yml
   aurls=\$(/etc/gocurl/gocurl "https://8.8.8.8/resolve?name=\${alist}.allowlists.\${domain}&type=txt" 2>/dev/null | awk '/Answer/,/Comment/' | awk '// {gsub(/,"/, "\n"); print}'  | awk '{gsub(/"|\]|\[/,"");print}'  | awk '{gsub(/^[ {]+|[ }]+$/,"");print}' | awk '/data:/{gsub("data:","",\$0); print \$0}' | awk '// {gsub(/,/, "\n"); print}' | awk '// {gsub(/\\\/, ""); print}')
     for aurl in \$(printf '%s\n' \$aurls)
      do
        if [ "\$(echo \$aurl | awk '/^file:/ { print "found" }')" = "found" ]; then
          list=\$(echo \$aurl | awk '// {gsub (/file:/, ""); print}')
          #echo "downloading \$list"
          #echo "found file"
          fileurl=\$(echo \$list | awk '// {gsub (/\/|\.|:|&|=|;/, "_"); print}')
          /etc/gocurl/gocurl \$list > /etc/blocky/lists/\$fileurl
          #echo "fixing file"
          awk '{print}' /etc/blocky/lists/\$fileurl | awk '// {gsub(/127.0.0.1|0.0.0.0|::1|\t| |\*./, ""); print}' | awk '// {gsub(/^localhost$|^ip6-localhost$|^localhost.localdomain$/, "");print}' | awk '!/^#/{gsub(/#.*/,"",\$0);print}' | awk '!/^<|^\||>$|\]|\[|^\-/' | awk '/\./' | awk 'NF > 0' > /etc/blocky/lists/\$fileurl.txt.tmp && mv /etc/blocky/lists/\$fileurl.txt.tmp /etc/blocky/lists/\$fileurl
          echo "adding /etc/blocky/lists/\$fileurl to conf under \${alist} from \$list"
          echo "      - /etc/blocky/lists/\$fileurl" >> /etc/blocky/config.yml
        else
          echo "adding \${aurl} to conf under \${alist}"
          echo "      - \${aurl}" >> /etc/blocky/config.yml
        fi
     done
  done
fi

echo "  clientGroupsBlock:" >> /etc/blocky/config.yml
echo "    default:" >> /etc/blocky/config.yml

for list in \$(printf '%s\n' \$blists)
 do
  echo "      - \${list}" >> /etc/blocky/config.yml
done


echo "  blockType: zeroIp #nxDomain,0.0.0.0, ::" >> /etc/blocky/config.yml
echo "  blockTTL: 30m" >> /etc/blocky/config.yml
echo "  loading:" >> /etc/blocky/config.yml
echo "    refreshPeriod: 4h" >> /etc/blocky/config.yml
echo "    downloads:" >> /etc/blocky/config.yml
echo "      timeout: 4m" >> /etc/blocky/config.yml
echo "      attempts: 2" >> /etc/blocky/config.yml
echo "      cooldown: 10s" >> /etc/blocky/config.yml
echo "    concurrency: 4" >> /etc/blocky/config.yml
echo "    strategy: fast" >> /etc/blocky/config.yml
echo "    maxErrorsPerSource: 5" >> /etc/blocky/config.yml

#echo "combining files"
#awk '{print}' /etc/blocky/lists/*.txt > /etc/blocky/blocked-names.txt

##echo "sorting"
##awk '{ line[NR] = \$0 } END { do { haschanged = 0; for(i=1; i < NR; i++) { if ( line[i] > line[i+1] ) { t = line[i]; line[i] = line[i+1]; line[i+1] = t; haschanged = 1 } } } while ( haschanged == 1 ) for(i=1; i <= NR; i++) { print line[i] } }' /etc/blocky/blocked-names.txt > /etc/blocky/blocked-names.txt.sorted 

#echo "remove dups"
#awk '!(\$0 in a) {a[\$0];print}' /etc/blocky/blocked-names.txt > /etc/blocky/blocked-names.txt.dups && mv /etc/blocky/blocked-names.txt.dups /etc/blocky/blocked-names.txt

fi


echo "Ports"


if [ ! -z "\$(pidof blocky)" ]; then
kill \$(pidof blocky)
sleep 10
fi

#"\$(netstat -anp | grep 'LISTEN' | grep ':53 ')"
if getnetstat 53; then
  echo "default dns server running on default unsecured port (53) to new port (8053)"
  awk '// {sub(/ 53/, " 8053"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi

#"\$(netstat -anp | grep ':53 ' | grep 'blocky' )"
#if getnetstat 53; then
#  echo "replacing the current running blocky with unsecure alt port"
#  awk '// {sub(/ 53/, " 8053"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
#fi

#"\$(netstat -anp | grep 'LISTEN' | grep ':8853 ')"
if getnetstat 8853; then
echo "replacing the current running blocky with default tls (853) port"
awk '// {sub(/ 8853/, " 853"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi

if ! getnetstat 53 && [ -f /.dockerenv ]; then
echo "replacing the current port with the default tls port (853) since there is no default dns server on the host"
awk '// {sub(/ 8853/, " 853"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi

if ! getnetstat 2096 && [ -f /.dockerenv ]; then
echo "replacing the current port with the default https port (2096) since there is no default dns server on the host"
awk '// {sub(/ 8443/, " 2096"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi


#"\$(netstat -anp | grep ':853 ' | grep 'blocky' )"
#if getnetstat 853; then
#  echo "replacing the current running blocky with default tls port"
#  awk '// {sub(/ 8853/, " 853"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
#fi

#"\$(netstat -anp | grep 'LISTEN' | grep ':8443 ')"
if getnetstat 8443; then
echo "replacing the current running blocky with new https (2096) port"
awk '// {sub(/ 8443/, " 2096"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi

#"\$(netstat -anp | grep ':8443 ' | grep 'blocky' )"
#if getnetstat 8443; then
#  echo "replacing the current running blocky with https port"
#  awk '// {sub(/ 8443/, " 2096"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
#fi

#"\$(netstat -anp | grep 'LISTEN' | grep ':8080 ')"
if getnetstat 8080; then
 echo "replacing the current running blocky with new http (8880) port"
 awk '// {sub(/ 8080/, " 8880"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
fi

#"\$(netstat -anp | grep ':8080 ' | grep 'blocky' )"
#if getnetstat 8080; then
# echo "replacing the current running blocky with http port"
#  awk '// {sub(/ 8080/, " 8880"); print}' /etc/blocky/config.yml > /etc/blocky/config.tmp && mv /etc/blocky/config.tmp /etc/blocky/config.yml
#fi

fi #end of day run or manual refresh


checkresolv() {
resolvers() {
    awk '\$1 == "nameserver" { print \$2 }' /etc/resolv.conf
}

is_resolver_responding() {
    #dig +short google.com in ns @$1 >/dev/null
    #host -v test.invalid | awk -F "[ #]" '/Received /{print$5}'
    /etc/godig/godig -s \$1 -t PTR --timeout=1s \$1  > /dev/null 2>&1
}

# print whether each configured resolver is responding (<ip> <"OK" or "BAD">)
for resolver in \$(resolvers); do
    resolvercheck=\$(is_resolver_responding \$resolver && echo "\$resolver is OK" >/dev/null || echo "\$resolver is BAD. Please correct")
    if [ ! -z "\$resolvercheck" ]; then echo \$resolvercheck; fi
done
}


#fix resolv.conf if local dns server is running
if [ -f /etc/resolv.conf ]; then
  if getnetstat 53; then
    #lgint=\$(ip addr | awk -F: '/state/ && !/lo|docker|dummy/ { gsub(/[ \t]/,"",\$2); print \$2 }')
    lgint=\$(/etc/goip/goip | awk '!/lo|docker|dummy/ { gsub(/[ \t\r]|name=|interfaces:/,"",\$2); print \$2 }' | awk '!a[\$0]++' | awk 'NF > 0')
    lgips=""
    lgips="\$(echo "# Created by blocky refresh-dns.sh script (secure)")" #> /etc/resolv.conf
    for lgip in \$(echo "\$lgint")
    do
      #lgips=\$(echo \$lgips && echo "\$( ip addr show dev \$lgip | awk '/inet/ {print \$2}' | awk -F/ '!/^fe80|^127/ && /[a-zA-Z0-9]/ { print "newlinenameserver "\$1 }')")
      lgips=\$(echo \$lgips && echo \$( /etc/goip/goip | awk '/'\$lgip'/' | awk '!/lo|docker|dummy|Total/ { gsub(/[ \t\r]|addr=|interfaces:/,"",\$3); print \$3 }' | awk '!a[\$0]++' | awk 'NF > 0' | awk -F/ '!/^fe80|^127/ && /[a-zA-Z0-9]/ { print "newlinenameserver "\$1 }'))
    done
    if [ -z "\$lgint" ]; then
      ip4=\$(/etc/goip/goip | awk '!/pointtopoint|lo|Total|docker/' | awk '{ print \$1,\$2,\$3 }' | awk '/\./' | awk -F '\n' '{if(NR==1) {print \$1}}' | awk -F '=' '{print \$4}' | awk -F '/' '{print \$1}')
      ip6=\$(/etc/goip/goip | awk '!/pointtopoint|lo|Total|docker/' | awk '{ print \$1,\$2,\$3 }' | awk '/:/' | awk -F '\n' '{if(NR==1) {print \$1}}' | awk -F '=' '{print \$4}' | awk -F '/' '{print \$1}')
      loips=\$(echo "newlinenameserver \$ip4" && echo "newlinenameserver \$ip6")
      lgips=\$(echo "\$lgips" && echo "\$loips") #>> /etc/resolv.conf
    fi
    lgips=\$(echo "\$lgips" && echo "options timeout:3 attempts:3 edns0") #>> /etc/resolv.conf
  else
    lgips=""
    lgips="\$(echo "# Created by blocky refresh-dns.sh script (fallback/lan)")" #> /etc/resolv.conf
    if [ ! -f /etc/blocky/fallback ]; then awk 'BEGIN { print "1.1.1.2\n9.9.9.11\n2606:4700:4700::1112\n2620:fe::11" }' > /etc/blocky/fallback; fi
    for lgip in \$(awk '{ print }' /etc/blocky/fallback)
    do
      lgips=\$(echo \$lgips && echo "newlinenameserver \$lgip")
    done
    lgips=\$(echo "\$lgips" && echo "options timeout:3 attempts:3 edns0") #>> /etc/resolv.conf
  fi


  if [ "\$(awk '{print}' /etc/resolv.conf)" != "\$(echo "\$lgips" | awk '{gsub(/newline/, "\n"); print}' | awk 'NF > 0')" ]; then
    echo "Updating resolv.conf"
    echo "\$lgips" | awk '{gsub(/newline/, "\n"); print}' | awk 'NF > 0' > /etc/resolv.conf
    checkresolv
  fi
fi



_EOF_
chmod u+x /etc/blocky/refresh-dns.sh

if [ ! -d /etc/cron.minutes ]; then mkdir -p /etc/cron.minutes; fi
if [ ! -f /etc/cron.minutes/refresh-dns ]; then echo "#!/bin/env bash" > /etc/cron.minutes/refresh-dns; echo "echo | /etc/blocky/refresh-dns.sh" >> /etc/cron.minutes/refresh-dns; chmod 755 /etc/cron.minutes/refresh-dns; fi

if [ ! -d /etc/cron.startup ]; then mkdir -p /etc/cron.startup; fi
if [ ! -f /etc/cron.startup/refresh-dns ]; then echo "#!/bin/env bash" > /etc/cron.startup/refresh-dns; echo "echo | /etc/blocky/refresh-dns.sh" >> /etc/cron.startup/refresh-dns; chmod 755 /etc/cron.startup/refresh-dns; fi
if [ ! -d /var/log/cron ]; then mkdir -p /var/log/cron; fi
if [ ! -f /etc/crontab ]; then
 if [ -z "$(awk '/@reboot/' /etc/crontab)" ]; then echo "--adding reboot value"; echo '@reboot root cd / && run-parts --report /etc/cron.startup > /var/log/cron/startup.log' >> /etc/crontab; fi
fi

# Create systemd unit
create_bootstrap_systemd_config() {
if [ ! -d /etc/systemd/system ]; then mkdir -p /etc/systemd/system; fi
# Systemd unit
cat > /etc/systemd/system/blocky-v4bootstrap.service <<_EOF_
[Unit]
Description=Blocky is a DNS proxy server and ad-blocker (ipv4 bootstrap)
ConditionPathExists=/etc/blocky
After=local-fs.target
[Service]
#User=${_APP_USER_NAME}
#Group=${_APP_USER_NAME}
Type=simple
WorkingDirectory=/etc/blocky
ExecStart=/etc/blocky/blocky --config /etc/blocky/ipv4.yml
Restart=on-failure
RestartSec=10
#StandardOutput=syslog
#StandardError=syslog
SyslogIdentifier=blocky-v4bootstrap
[Install]
WantedBy=multi-user.target
_EOF_

cat > /etc/systemd/system/blocky-v6bootstrap.service <<_EOF_
[Unit]
Description=Blocky is a DNS proxy server and ad-blocker (ipv6 bootstrap)
ConditionPathExists=/etc/blocky
After=local-fs.target
[Service]
#User=${_APP_USER_NAME}
#Group=${_APP_USER_NAME}
Type=simple
WorkingDirectory=/etc/blocky
ExecStart=/etc/blocky/blocky --config /etc/blocky/ipv6.yml
Restart=on-failure
RestartSec=10
#StandardOutput=syslog
#StandardError=syslog
SyslogIdentifier=blocky-v6bootstrap
[Install]
WantedBy=multi-user.target
_EOF_
}

#create Bootstrap for ipv4 only and/or ipv6 only networks
create_bootstrap_systemd_config


# Systemd unit for main blocky
cat > /etc/systemd/system/blocky.service <<_EOF_
[Unit]
Description=Blocky is a DNS proxy and ad-blocker
ConditionPathExists=/etc/blocky
After=local-fs.target
[Service]
#User=${_APP_USER_NAME}
#Group=${_APP_USER_NAME}
Type=simple
WorkingDirectory=/etc/blocky
ExecStart=/etc/blocky/blocky --config /etc/blocky/config.yml
Restart=on-failure
RestartSec=10
#StandardOutput=syslog
#StandardError=syslog
SyslogIdentifier=blocky
[Install]
WantedBy=multi-user.target
_EOF_

echo "reloading blocky"
echo | /etc/blocky/refresh-dns.sh
#for file in /etc/blocky/*.yml;
#do
#    if [ "$file" != "/etc/blocky/config.yml" ] && [ "$file" != "/etc/blocky/ipv4.yml" ] && [ "$file" != "/etc/blocky/ipv6.yml" ]; then
#    echo "overwriting $file with new config"
#    awk '{ print }' "/etc/blocky/config.yml" > "$file"
#    fi
#done

if [ ! -z "$(pidof blocky)" ]; then
kill -9 $(pidof blocky)
fi

if [ $(which systemctl) ]; then 
 systemctl daemon-reload
 systemctl enable --now blocky-v4bootstrap.service
 systemctl enable --now blocky-v6bootstrap.service
 sleep 3
fi

if [ ! -d /etc/supervisor ]; then mkdir -p /etc/supervisor; fi
if [ ! -f /etc/supervisor/program:blocky.args ]; then
echo "command = /etc/blocky/blocky --config /etc/blocky/config.yml" > /etc/supervisor/program:blocky.args
fi

if ! $(/etc/godig/godig -s 127.0.0.1:453 --short -t NS >/dev/null 2>&1); then
  if [ ! -f /etc/supervisor/program:blockyv4.args ]; then
    echo "command = /etc/blocky/blocky --config /etc/blocky/ipv4.yml" > /etc/supervisor/program:blockyv4.args
  fi
fi

if ! $(/etc/godig/godig -s [::1]:653 --short -t NS >/dev/null 2>&1); then
  if [ ! -f /etc/supervisor/program:blockyv6.args ]; then
    echo "command = /etc/blocky/blocky --config /etc/blocky/ipv6.yml" > /etc/supervisor/program:blockyv6.args
  fi
fi

#if $(/etc/godig/godig -s 127.0.0.1:453 --short -t NS 2>1 /dev/null); then
#  if [ -f /etc/supervisor/program:blockyv4.args ]; then
#    rm /etc/supervisor/program:blockyv4.args
#  fi
#fi

#if $(/etc/godig/godig -s [::1]:653 --short -t NS 2>1 /dev/null); then
#  if [ -f /etc/supervisor/program:blockyv6.args ]; then
#    rm /etc/supervisor/program:blockyv6.args
#  fi
#fi

if [ -f /etc/supervisord/supervisord ]; then
  if [ ! -z "$(pidof supervisord)" ]; then
  echo "Supervisor running, so restarting it to reload blocky"
  kill -9 $(pidof supervisord)
  sleep 5
  echo "checking if supervisor restarted.. if not... starting back up"
    if [ -z "$(pidof supervisord)" ]; then
      echo "running supervisor again.. recommend restarting computer or container"
      /etc/supervisord/supervisord -d
    fi
  fi
fi

echo 'to run: ./etc/blocky/blocky or systemctl enable --now blocky.service'
