#!/bin/env bash

echo

#get current dir
cdir=`dirname $0`

#check status
[ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
if [ $changed = 1 ]; then
 #update git-repo first
 #git reset -- hard # for resetting
 echo "Updating script.."
 git -C $cdir pull
 echo "Please rerun script"
 exit
fi

#run as root
ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

#check if installed
if which birdc >/dev/null; then
 #exists
 exists=yes
else
# update and install bird
 sudo apt update && sudo apt install -y bird2 keepalived jq knot-dnsutils openssl gzip
fi

if which jq >/dev/null; then
 #exists
 exists=yes
else
 sudo apt update && sudo apt install -y jq
fi

#move default configs
if [ ! -f /etc/bird/bird.orig ]; then mv /etc/bird/bird.conf /etc/bird/bird.orig; fi
#if [ ! -f /etc/keepalived/keepalived.orig ]; then mv /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.orig; fi

#get config
MODEL=`cat /sys/class/dmi/id/product_name | tr -dc '[:alnum:]\n\r'`
PETH=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev | tail -n -1`
MAC=`cat /sys/class/net/$PETH/address | sort -r | head -n 1 | cut -d':' -f4- | tr -d ':'`
USRAGT=`echo ${MODEL}-${MAC}`

if [ -f /root/.user_info ]; then
CFG_TOKEN=`cat /root/.user_info | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT | jq .\"https\:\/\/example\.invalid\/app_metadata\".CFG_TOKEN | tr -d '"'`
fi
if [ "$CFG_TOKEN" = 'null' ] || [ -z "$CFG_TOKEN" ]; then echo "** NO ANYCAST CONFIG **"; echo; exit; fi

#get anycast ip
dnsrecord="anycast.$(hostname -f | cut -d'.' -f2-)."
echo "Quering DNS Record: $dnsrecord"
anycastip46=`kdig @1.1.1.1 +tls +short A ${dnsrecord} AAAA ${dnsrecord} | grep -v -e '^\:\:' | grep -v -e '^0\.0\.0\.0'`
if [ ! -z "$anycastip46" ]; then
echo "DNS data found: $anycastip46"
anycastip4=`echo $anycastip46 | tr ' ' '\n' | grep '\.' | head -n 1`
anycastip6=`echo $anycastip46 | tr ' ' '\n' | grep '\:' | head -n 1`
if ! ip addr show | grep -q 'dummy0'; then ip link add dev dummy0 type dummy; fi
ip link set dummy0 up
if ! ip addr show | grep -q "$anycastip4"; then ip addr add dev dummy0 ${anycastip4}/32; fi
if ! ip addr show | grep -q "$anycastip6"; then ip addr add dev dummy0 ${anycastip6}/128; fi
else
echo "DNS record does not exist"
fi

echo
echo "getting cfg from DNS for keepalived (VRRP) L2"
dnscfg=`kdig @1.1.1.1 +tls +short TXT cfg-vrrp.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g'`
if [ ! -z "$dnscfg" ]; then
 echo "Config found"
 echo "$dnscfg" | openssl enc -d -base64 -aes-256-cbc -pbkdf2 -k $CFG_TOKEN | gzip -d > /etc/keepalived/keepalived.conf
 vrrpip4=`grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" /etc/keepalived/keepalived.conf | tail -n 1`
 systemctl stop keepalived
 echo "sleeping for 10 secs... " && sleep 10
 intvrrp=`ip -4 route get ${vrrpip4} | awk {'print $3'} | tr -d '\n'`
 cat /etc/keepalived/keepalived.conf | awk -v srch=" intvrrp" -v repl=" $intvrrp" '{ sub(srch,repl,$0); print $0 }' > /tmp/keepalived.conf && mv /tmp/keepalived.conf /etc/keepalived/keepalived.conf
 touch /srv/services.txt
 cat /srv/services.txt | grep -q "${vrrpip4} ip"
 if [ $? -eq 1 ]; then echo "${vrrpip4} ip" >> /srv/services.txt; fi
 systemctl enable --now keepalived
else
 echo "TXT: cfg-vrrp.$(hostname -f | cut -d'.' -f2-)."
 touch /etc/keepalived/keepalived.conf
 cat /etc/keepalived/keepalived.conf | gzip -f | openssl enc -e -base64 -aes-256-cbc -pbkdf2 -k $CFG_TOKEN
 echo "** NO VRRP CONFIG **"; echo;
fi

echo "setting script for ports"
cat > /etc/keepalived/ports <<'EOF'
#!/bin/bash
getnetstat() {
lport=$1
ports=""
protos="tcp udp tcp6 udp6"
for proto in $protos
do
port=$(awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2));
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
}
NR > 1 {{if(NR==2)print "";local=getIP($2);remote=getIP($3)}{print local"-"remote}}' /proc/net/$proto)
ports="$ports $port"
done
ports=$(echo "$ports" | awk 'NF > 0' | awk '!a[$0]++' | awk '/0:'$lport'-/')

if [ ! -z "$ports" ]; then
 echo "port $lport is listening..."
 return 0
else
 echo "port $lport not found..."
 return 1
fi
}
getnetstat $1
EOF
chmod +x /etc/keepalived/ports


echo
echo "getting cfg from DNS for bird (BGP) L3"
dnscfg=`kdig @1.1.1.1 +tls +short TXT cfg-bgp.$(hostname -f | cut -d'.' -f2-). | sed -e 's/"//g' -e 's/ //g'`
if [ ! -z "$dnscfg" ]; then
 echo "$dnscfg" | openssl enc -d -base64 -aes-256-cbc -pbkdf2 -k $CFG_TOKEN | gzip -d > /etc/bird/bird.conf
else
 echo "TXT: cfg-bgp.$(hostname -f | cut -d'.' -f2-)."
 touch /etc/bird/bird.conf
 cat /etc/bird/bird.conf | gzip -f | openssl enc -e -base64 -aes-256-cbc -pbkdf2 -k $CFG_TOKEN #| openssl enc -d -base64 -aes-256-cbc -pbkdf2 -k $CFG_TOKEN | gzip -d
 echo "** NO CONFIG **"; echo; exit;
fi

#get ip and put it in bird config
intgtw=`ip route | awk '/^default/ { print $5 }'`
#intmac=`ip -o link show |cut -d' ' -f 2,20 | grep $intgtw | rev | cut -d':' -f1 | rev`
#intip6=`ip addr show dev $intgtw | grep -v 'temp' | grep 'inet6 ' | grep 'global' | awk '{ print $2 }' | cut -d'/' -f1`
#intip4=`ip addr show dev $intgtw | grep -v 'temp' | grep 'inet ' | grep 'global' | awk '{ print $2 }' | cut -d'/' -f1`
intip6=$(ip addr show dev $intgtw | awk -F'/' '!/temp/ && /inet6/ && /global/ { print substr($1,11,99) }')
intip4=$(ip addr show dev $intgtw | awk -F'/' '!/temp/ && /inet / && /global/ { print substr($1,10,99) }')
inpass=$(awk '/password/ {gsub(/#|"|;/, ""); print $2 }' /etc/bird/bird.conf | awk '!seen[$0]++')
loclas=$(awk '/local as/ {gsub(/#|"|;/, ""); print $3 }' /etc/bird/bird.conf | awk '!seen[$0]++')
nboras=$(awk '/neighbor/ {gsub(/#|"|;/, ""); print $4 }' /etc/bird/bird.conf | awk '!seen[$0]++')
neibr4=$(awk '/neighbor/ && /\./ {gsub(/#|"|;/, ""); print $2 }' /etc/bird/bird.conf | awk '!seen[$0]++')
neibr6=$(awk '/neighbor/ && /:/ {gsub(/#|"|;/, ""); print $2 }' /etc/bird/bird.conf | awk '!seen[$0]++')
cat /etc/bird/bird.conf | awk -v srch="intip4" -v repl="$intip4" '{ sub(srch,repl,$0); print $0 }' > /tmp/bird.conf && mv /tmp/bird.conf /etc/bird/bird.conf
cat /etc/bird/bird.conf | awk -v srch="intip6" -v repl="$intip6" '{ sub(srch,repl,$0); print $0 }' > /tmp/bird.conf && mv /tmp/bird.conf /etc/bird/bird.conf

cat >/etc/bird/conf.conf<<EOF
router id $intip4;

protocol device
{
    scan time 5;
}

function avoid_martiansv4()
prefix set v4bogon_prefixes;
{
     v4bogon_prefixes = [ 0.0.0.0/8+,         # RFC 1122 'this' network
                          10.0.0.0/8+,        # RFC 1918 private space
                          100.64.0.0/10+,     # RFC 6598 Carrier grade nat space
                          127.0.0.0/8+,       # RFC 1122 localhost
                          169.254.0.0/16+,    # RFC 3927 link local
                          172.16.0.0/12+,     # RFC 1918 private space
                          192.0.2.0/24+,      # RFC 5737 TEST-NET-1
                          192.88.99.0/24+,    # RFC 7526 6to4 anycast relay
                          192.168.0.0/16+,    # RFC 1918 private space
                          198.18.0.0/15+,     # RFC 2544 benchmarking
                          198.51.100.0/24+,   # RFC 5737 TEST-NET-2
                          203.0.113.0/24+,    # RFC 5737 TEST-NET-3
                          224.0.0.0/4+,       # multicast
                          240.0.0.0/4+ ];     # reserved

    if (net ~ v4bogon_prefixes) then {
        print "Reject: Bogon prefix: ", net, " ", bgp_path;
        #reject;
        return false;
    }
    return true;
}

function avoid_bad_prefixesv4()
{
if net.len < 8 then return false;
#if net.len > 24 then return false;
return true;
}

filter bgp4_out {
 if avoid_martiansv4() && avoid_bad_prefixesv4() then accept;
 else reject;
}

function avoid_martiansv6()
prefix set v6bogon_prefixes;
{
     v6bogon_prefixes = [ ::/8+,                         # RFC 4291 IPv4-compatible, loopback, et al
                          0100::/64+,                    # RFC 6666 Discard-Only
                          2001:2::/48+,                  # RFC 5180 BMWG
                          2001:10::/28+,                 # RFC 4843 ORCHID
                          2001:db8::/32+,                # RFC 3849 documentation
                          2002::/16+,                    # RFC 7526 6to4 anycast relay
                          3ffe::/16+,                    # RFC 3701 old 6bone
                          fc00::/7+,                     # RFC 4193 unique local unicast
                          fe80::/10+,                    # RFC 4291 link local unicast
                          fec0::/10+,                    # RFC 3879 old site local unicast
                          ff00::/8+                      # RFC 4291 multicast
       ];
    if (net ~ v6bogon_prefixes) then {
        print "Reject: Bogon prefix: ", net, " ", bgp_path;
        #reject;
        return false;
    }
    return true;
}

filter bgp6_out {
 if avoid_martiansv6() then accept;
 else reject;
}


protocol direct anycast
{
    interface "dummy*";
    interface "lo";
    ipv4;
    ipv6;
}

protocol bgp BGP4
{
    # substitute with your AS or private AS
    local as $loclas;
    source address $intip4;
    ipv4 {
     import none;
     export filter bgp4_out;
    };
    hold time 10;
    graceful restart on;
    multihop 2;
    neighbor $neibr4 as $nboras;
    password "$inpass";
}

protocol bgp BGP6
{
    # substitute with your AS or private AS
    local as $loclas;
    source address $intip6;
    ipv6 {
     import none;
     export filter bgp6_out;
    };
    hold time 10;
    graceful restart on;
    multihop 2;
    neighbor $neibr6 as $nboras;
    password "$inpass";
}

EOF

cp /etc/bird/conf.conf /etc/bird/bird.conf


#run service and reload
echo "reloading bird"
systemctl enable --now bird
systemctl restart bird

#show stats
echo "sleeping for 3 secs..." && sleep 3
birdc show route
birdc show proto all




mkdir -p /etc/cron.hourly
cfile=`realpath $0`
cat > /etc/cron.hourly/check-anycast <<EOF
#!/bin/env bash
#echo "2" | sh $cfile
ifconfig dummy0
if [ \$? -eq 0 ]; then echo "interface exists";
else echo "2" | sh $cfile; fi

getnetstat() {
lport=\$1
ports=""
protos="tcp udp tcp6 udp6"
for proto in \$protos
do
port=\$(awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2));
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
}
NR > 1 {{if(NR==2)print "";local=getIP(\$2);remote=getIP(\$3)}{print local"-"remote}}' /proc/net/\$proto)
ports="\$ports \$port"
done
ports=\$(echo "\$ports" | awk 'NF > 0' | awk '!a[\$0]++' | awk '/0:'\$lport'-/')

if [ ! -z "\$ports" ]; then
 echo "port \$lport is listening..."
 return 0
else
 echo "port \$lport not found..."
 return 1
fi

}
#check if port 53 is listening and enable anycastip else dont enable it
getnetstat 53
if [ \$? -eq 0 ]; then birdc enable anycast;
else birdc disable anycast; fi
EOF
chmod +x /etc/cron.hourly/check-anycast

sh /etc/cron.hourly/check-anycast

echo "DONE"
echo
