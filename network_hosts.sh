#!/usr/bin/env bash

HOST=`hostname | cut -d'.' -f1`
FQDN=`hostname -f | grep '\.'`

if [ ! -f /etc/hosts.bak ]; then
echo "Creating Backup of Hosts file"
cat /etc/hosts > /etc/hosts.bak
fi

hex2dec()
{
    [ "$1" != "" ] && printf "%d" "$(( 0x$1 ))"
}

tabtxt()
{
    if [ `echo $1 | wc -c` -le 8 ]; then printf "${1}\t\t\t\t\t"; fi
    if [ `echo $1 | wc -c` -gt 8 ]; then printf "${1}\t\t\t\t"; fi
}

format_v4address()
{
    printf "%03d.%03d.%03d.%03d" $(echo $1 | cut -d'.' --output-delimiter=' ' -f1-4)
}

format_v6address()
{
    ip=$1

    # prepend 0 if we start with :
    echo $ip | grep -qs "^:" && ip="0${ip}"

    # expand ::
    if echo $ip | grep -qs "::"; then
        colons=$(echo $ip | sed 's/[^:]//g')
        missing=$(echo ":::::::::" | sed "s/$colons//")
        expanded=$(echo $missing | sed 's/:/:0/g')
        ip=$(echo $ip | sed "s/::/$expanded/")
    fi

    blocks=$(echo $ip | grep -o "[0-9a-f]\+")
    set $blocks

    printf "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x\t\n" \
        $(hex2dec $1) \
        $(hex2dec $2) \
        $(hex2dec $3) \
        $(hex2dec $4) \
        $(hex2dec $5) \
        $(hex2dec $6) \
        $(hex2dec $7) \
        $(hex2dec $8)
}

# a valid ipv6 is either the expanded form or the compressed one
is_ipv6()
{
    expanded="$(format_v6address $1)"
    [ "$1" = "$expanded" ] && return 0
    compressed="$(compress_ipv6 $expanded)"
    [ "$1" = "$compressed" ] && return 0
    return 1
}

# returns a compressed ipv6 address under the form recommended by RFC5952
compress_ipv6()
{
    ip=$1

    blocks=$(echo $ip | grep -o "[0-9a-f]\+")
    set $blocks

    # compress leading zeros
    ip=$(printf "%x:%x:%x:%x:%x:%x:%x:%x\n" \
        $(hex2dec $1) \
        $(hex2dec $2) \
        $(hex2dec $3) \
        $(hex2dec $4) \
        $(hex2dec $5) \
        $(hex2dec $6) \
        $(hex2dec $7) \
        $(hex2dec $8)
    )

    # prepend : for easier matching
    ip=:$ip

    # :: must compress the longest chain
    for pattern in :0:0:0:0:0:0:0:0 \
            :0:0:0:0:0:0:0 \
            :0:0:0:0:0:0 \
            :0:0:0:0:0 \
            :0:0:0:0 \
            :0:0; do
        if echo $ip | grep -qs $pattern; then
            ip=$(echo $ip | sed "s/$pattern/::/")
            # if the substitution occured before the end, we have :::
            ip=$(echo $ip | sed 's/:::/::/')
            break # only one substitution
        fi
    done

    # remove prepending : if necessary
    echo $ip | grep -qs "^:[^:]" && ip=$(echo $ip | sed 's/://')

    echo $ip
}

parse_yaml()
{
 local prefix=$2
 local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
 sed -ne "s|,$s\]$s\$|]|" \
    -e ":1;s|^\($s\)\($w\)$s:$s\[$s\(.*\)$s,$s\(.*\)$s\]|\1\2: [\3]\n\1  - \4|;t1" \
    -e "s|^\($s\)\($w\)$s:$s\[$s\(.*\)$s\]|\1\2:\n\1  - \3|;p" $1 | \
 sed -ne "s|,$s}$s\$|}|" \
    -e ":1;s|^\($s\)-$s{$s\(.*\)$s,$s\($w\)$s:$s\(.*\)$s}|\1- {\2}\n\1  \3: \4|;t1" \
    -e    "s|^\($s\)-$s{$s\(.*\)$s}|\1-\n\1  \2|;p" | \
 sed -ne "s|^\($s\):|\1|" \
    -e "s|^\($s\)-$s[\"']\(.*\)[\"']$s\$|\1$fs$fs\2|p" \
    -e "s|^\($s\)-$s\(.*\)$s\$|\1$fs$fs\2|p" \
    -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
    -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p" | \
 awk -F$fs '{ indent = length($1)/2; vname[indent] = $2; for (i in vname) {if (i > indent) {delete vname[i]; idx[i]=0}} if(length($2)== 0){  vname[indent]= ++idx[indent] }; if (length($3) > 0) { vn=""; for (i=0; i<indent; i++) { vn=(vn)(vname[i])("_")} printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, vname[indent], $3); } }'
}

cat /dev/null > /etc/hosts
printf "#Localhost\n$(tabtxt 127.0.0.1)localhost.localdomain localhost\n$(format_v6address ::1)localhost.localdomain localhost ip6-localhost ip6-loopback" > /etc/hosts
printf "\n" >> /etc/hosts
printf "#IPv6 capable hosts\n$(format_v6address ff02::1)ip6-allnodes\n$(format_v6address ff02::2)ip6-allrouters" >> /etc/hosts
printf "\n" >> /etc/hosts
printf "#MCAST ALL NODES\n$(tabtxt 224.0.1.1)all-nodes.nodes-local.mcast.arpa\n$(format_v6address ff01::1)all-nodes.nodes-local.mcast6.arpa" >> /etc/hosts
printf "\n" >> /etc/hosts
printf "#MCAST MDNS\n$(tabtxt 224.0.0.251)mdns.mcast.arpa\n$(format_v6address ff01::1)all-nodes.nodes-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff01::fb)mdnsv6.node-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff02::fb)mdnsv6.link-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff04::fb)mdnsv6.admin-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff05::fb)mdnsv6.site-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff08::fb)mdnsv6.org-local.mcast6.arpa" >> /etc/hosts
printf "\n$(format_v6address ff0e::fb)mdnsv6.global.mcast6.arpa" >> /etc/hosts
printf "\n" >> /etc/hosts

#public dns servers
printf "\n#Public DNS Server - Cloudflare\n" >> /etc/hosts
printf "$(tabtxt 1.1.1.1)dns.cloudflare-dns.com\n" >> /etc/hosts
printf "$(tabtxt 1.0.0.1)dns.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1111)dns.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1001)dns.cloudflare-dns.com\n" >> /etc/hosts
printf "$(tabtxt 1.1.1.2)security.cloudflare-dns.com\n" >> /etc/hosts
printf "$(tabtxt 1.0.0.2)security.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1112)security.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1002)security.cloudflare-dns.com\n" >> /etc/hosts
printf "$(tabtxt 1.1.1.3)family.cloudflare-dns.com\n" >> /etc/hosts
printf "$(tabtxt 1.0.0.3)family.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1113)family.cloudflare-dns.com\n" >> /etc/hosts
printf "$(format_v6address 2606:4700:4700::1003)family.cloudflare-dns.com\n" >> /etc/hosts
printf "\n#Public DNS Server - OpenDNS/Cisco\n" >> /etc/hosts
printf "$(tabtxt 146.112.41.2)doh.opendns.com\n" >> /etc/hosts
printf "$(format_v6address 2620:119:fc::2)doh.opendns.com\n" >> /etc/hosts
printf "\n#Public DNS Server - Quad9\n" >> /etc/hosts
printf "$(tabtxt 149.112.112.11)dns11.quad9.net\n" >> /etc/hosts
printf "$(format_v6address 2620:fe::11)dns11.quad9.net\n" >> /etc/hosts
printf "\n#Public DNS Server - Google\n" >> /etc/hosts
printf "$(tabtxt 8.8.8.8)dns.google\n" >> /etc/hosts
printf "$(format_v6address 2001:4860:4860::8888)dns.google\n" >> /etc/hosts
printf "\n#Public DNS Server - HE\n" >> /etc/hosts
printf "$(tabtxt 74.82.42.42)ordns.he.net\n" >> /etc/hosts
printf "$(format_v6address 2001:470:20::2)ordns.he.net\n" >> /etc/hosts
printf "\n#Public DNS Server - Cleanbrowsing\n" >> /etc/hosts
printf "$(tabtxt 185.228.168.9)doh.cleanbrowsing.org\n" >> /etc/hosts
printf "\n" >> /etc/hosts

MINT=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev`
ROUT=`ip route show | grep 'default' | awk '{ print $5 }'`
PINT=`for primaryint in "$MINT"; do echo "$primaryint" | grep "${ROUT}"; done`
if [ -z $HOST ]; then  HOST=`echo $(cat /etc/hostname)`; fi
if [ -z $FQDN ]; then  FQDN=`echo $(cat /etc/hostname).$(cat /etc/domainname)`; fi
PIP4=`ip -4 address show ${PINT} | grep inet | grep -v 'secondary' | grep 'global' | awk '{ print $2 }' | cut -d'/' -f1`
if [ ! -z ${PIP4} ]; then printf "\n#IPV4 Local Host\n$(tabtxt ${PIP4})${FQDN} ${HOST}\n" >> /etc/hosts; fi
PIP6=`ip -6 address show ${PINT} | grep inet6 | grep -v 'secondary' | grep 'global' | awk '{ print $2 }' | cut -d'/' -f1`
if [ ! -z ${PIP6} ]; then printf "\n#IPV6 Local Host\n$(format_v6address ${PIP6})\t${FQDN} ${HOST}\n" >> /etc/hosts; fi
printf "\n" >> /etc/hosts

#if [ -f /etc/statichosts ]; then
#printf "#Static Host(s)" > /srv/hosts-static.txt
#for statichosts in `cat /etc/statichosts`; do
# echo "adding static host: $statichosts"
# if [ ! -z ${PIP4} ]; then statichostipv4=`dig A +short ${statichosts} | head -n 1`; if [ ! -z ${statichostipv4} ]; then printf "\n$(tabtxt ${statichostipv4})${statichosts}" >> /srv/hosts-static.txt; fi fi
# if [ ! -z ${PIP6} ]; then statichostipv6=`dig AAAA +short ${statichosts} | head -n 1`; if [ ! -z ${statichostipv6} ]; then printf "\n$(format_v6address ${statichostipv6})\t${statichosts}" >> /srv/hosts-static.txt; fi fi
#done
#fi

if [ -f /tmp/customize.yaml ]; then rm /tmp/customize.yaml; fi
curl -f -k -s -w "\n" --resolve device-local-config.server.localdomain:443:127.0.0.1 https://device-local-config.server.localdomain/local/customize_devices.yaml -o /tmp/customize.yaml

if [ -f /tmp/customize.yaml ]; then

 tr -d '\r' < /tmp/customize.yaml  > /tmp/customize.yaml.temp
 cat /tmp/customize.yaml.temp > /tmp/customize.yaml
 sed -i s/device_tracker\.//g /tmp/customize.yaml

 parse_yaml /tmp/customize.yaml > /tmp/customize.yaml.temp
 sed '/^_/d' /tmp/customize.yaml.temp > /tmp/customize.yaml
 rm /tmp/customize.yaml.temp
 
 cat /tmp/customize.yaml | cut -d'_' -f1-6 | sort -u > /tmp/mac.txt
 printf "#IPV4 Remote Hosts\n" > /srv/hosts-reservedv4.txt
 printf "#IPV6 Remote Hosts\n" > /srv/hosts-reservedv6.txt
 printf "#Cluster Hosts\n" > /srv/hosts-clusterdns.txt
 #cat /dev/null > /srv/hosts-reservedv4.txt
 #cat /dev/null > /srv/hosts-reservedv6.txt
 #cat /dev/null > /srv/hosts-clusterdns.txt

 for macs in `cat /tmp/mac.txt`; do
   hostipv4=`cat /tmp/customize.yaml | grep "${macs}" | grep -i 'ipv4' | grep -v 'dhcp' | head -n 1 | cut -d'=' -f2 | sed "s/\"//g"`
   hostipv6=`cat /tmp/customize.yaml | grep "${macs}" | grep -i 'ipv6' | grep -v 'dhcp' | head -n 1 | cut -d'=' -f2 | sed "s/\"//g"`
   hostsnme=`cat /tmp/customize.yaml | grep "${macs}" | grep -i 'hostname' | head -n 1 | cut -d'=' -f2 | sed "s/\"//g"`
   domainme=`cat /tmp/customize.yaml | grep "${macs}" | grep -i 'domain' | head -n 1 | cut -d'=' -f2 | sed "s/\"//g"`
   clusterd=`cat /tmp/customize.yaml | grep "${macs}" | grep -i 'clusterdns' | head -n 1 | cut -d'=' -f2 | sed "s/\"//g"`
   if [ ! -z $domainme ]; then hostsnme=$(printf "${hostsnme}.${domainme} ${hostsnme}"); fi
   if [ ! -z $hostipv4 ]; then printf "$(tabtxt ${hostipv4})${hostsnme}\n" >> /srv/hosts-reservedv4.txt; fi
   if [ ! -z $hostipv6 ]; then printf "$(format_v6address ${hostipv6})\t${hostsnme}\n" >> /srv/hosts-reservedv6.txt; fi
   if [ ! -z $clusterd ]; then
    if [ ! -z $hostipv4 ]; then printf "$(tabtxt \#${hostipv4})${clusterd}\n" >> /srv/hosts-clusterdns.txt; fi
    if [ ! -z $hostipv6 ]; then printf "$(format_v6address \#${hostipv6})\t${clusterd}\n" >> /srv/hosts-clusterdns.txt; fi
   fi
 done
 
 rm /tmp/mac.txt

 cat /srv/hosts-*.txt >> /etc/hosts

 clusterips=`cat /tmp/customize.yaml | grep -i 'clusterips' | cut -d'=' -f2 | sed "s/\"//g" | tr ' ' '\n' | sort | uniq`
 if [ ! -z "$clusterips" ]; then printf "\n#Cluster IPs\n" >> /etc/hosts; fi
 for clusterip in `echo $clusterips | tr ' ' '\n'`; do
 chost=`echo $clusterip | tr '.' '-' | tr ':' '-'`
 echo "$clusterip ${chost}.lan ${chost}" >> /etc/hosts
 done

fi






