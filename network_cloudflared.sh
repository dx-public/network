#!/bin/env bash

echo "Getting Cloudflare/WGCF working"

if [ ! -f /etc/wireguard/wgcf ]; then 
 _BINARY=`curl -s https://api.github.com/repos/ViRb3/wgcf/releases/latest | grep browser_download_url | grep "linux_amd64" | awk '{print $2}' | tr -d '\"' || echo 'https://github.com/ViRb3/wgcf/releases/download/v2.2.15/wgcf_2.2.15_linux_amd64'`

 #wget https://github.com/ViRb3/wgcf/releases/download/v2.2.15/wgcf_2.2.15_linux_amd64 -O /etc/wireguard/wgcf
 mkdir -p /etc/wireguard
 wget $_BINARY -O /etc/wireguard/wgcf
 chmod +x /etc/wireguard/wgcf

 if [ ! -f /etc/wireguard/wgcf-account.toml ]; then
  /etc/wireguard/wgcf register
  mv wgcf-account.toml /etc/wireguard/.
 fi

 if [ ! -f /etc/wireguard/wgcf-profile.conf ]; then
  /etc/wireguard/wgcf generate --config /etc/wireguard/wgcf-account.toml
  mv wgcf-profile.conf /etc/wireguard/.
 fi

 echo "to use key run this cmd: WGCF_LICENSE_KEY='123412341234' /etc/wireguard/wgcf update"

 sleep 5
 echo "WGCF Status:"
 /etc/wireguard/wgcf status

 echo "WGCF Trace:"
 /etc/wireguard/wgcf trace

fi

if ! which wireguard >/dev/null || ! which wireguard-go >/dev/null; then
    apt -y install wireguard-go wireguard-tools resolvconf
    if ! which wireguard >/dev/null; then
     ln -s `which wireguard-go` /usr/bin/wireguard
    fi
    if ! which wireguard-go >/dev/null; then
     ln -s `which wireguard` /usr/bin/wireguard-go
    fi
fi


if [ ! -f /tmp/cloudflare-ipv6 ]; then curl -s https://www.cloudflare.com/ips-v4 > /tmp/cloudflare-ipv4; fi
sed '/162\.158\.0\.0/d' /tmp/cloudflare-ipv4 > /tmp/cloudflare-ipv4.temp && mv /tmp/cloudflare-ipv4.temp /tmp/cloudflare-ipv4
#198.41.192.0/24 and 198.41.200.0/24 are for argo tunnel
sleep 1
if [ ! -f /tmp/cloudflare-ipv6 ]; then curl -s https://www.cloudflare.com/ips-v6 > /tmp/cloudflare-ipv6; fi
profile=/etc/wireguard/cf-tun.conf
sed '/AllowedIPs\|DNS/d' /etc/wireguard/wgcf-profile.conf > $profile
addressv4=`cat $profile | grep 'Address' | grep '\.' | cut -d'=' -f2- | sed 's/ //g'`
addressv6=`cat $profile | grep 'Address' | grep ':' | cut -d'=' -f2- | sed 's/ //g'`
allowedipsv4=`cat /tmp/cloudflare-ipv4 | tr '\n' '\,'`
allowedipsv6=`cat /tmp/cloudflare-ipv6 | tr '\n' '\,'`
echo "AllowedIPs = ${allowedipsv4},${addressv4}" >> $profile
echo "AllowedIPs = ${allowedipsv6},${addressv6}" >> $profile

echo 'to start manually: wg-quick up cf-tun'
echo "to turn off: wg-quick down cf-tun"

tee /etc/systemd/system/wg-quick@.service>/dev/null<<_EOF_
[Unit]
Description=WireGuard via wg-quick(8) for %I
After=network-online.target nss-lookup.target
Wants=network-online.target nss-lookup.target
PartOf=wg-quick.target
Documentation=man:wg-quick(8)
Documentation=man:wg(8)
Documentation=https://www.wireguard.com/
Documentation=https://www.wireguard.com/quickstart/
Documentation=https://git.zx2c4.com/wireguard-tools/about/src/man/wg-quick.8
Documentation=https://git.zx2c4.com/wireguard-tools/about/src/man/wg.8

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/etc/wireguard/wg-quick up %i
ExecStop=/etc/wireguard/wg-quick down %i
ExecReload=/bin/bash -c 'exec /usr/bin/wg syncconf %i <(exec /usr/bin/wg-quick strip %i)'
Environment=WG_ENDPOINT_RESOLUTION_RETRIES=infinity

[Install]
WantedBy=multi-user.target
_EOF_


tee /etc/wireguard/wg-quick>/dev/null<<_EOF_
#!/usr/bin/env bash
#clear hosts file
sed '/engage\.cloudflareclient\.com/d' /etc/hosts > /etc/hosts.temp && mv /etc/hosts.temp /etc/hosts
/usr/bin/wg-quick down \$2 &>/dev/null

#check ipv4
ipv4endpoint=\$(dig @1.1.1.1 +tls +short A engage.cloudflareclient.com. | grep -v 'failed' | grep -v 'timed-out' | grep -v 'no servers')
echo "IPv4 Endpoint: \$ipv4endpoint"
ip -4 route del \${ipv4endpoint}/32 &>/dev/null
ipv4endpointstatus=\$(ping -4 -c 2 \${ipv4endpoint} &>/dev/null)
if [ \$? -eq 0 ]; then
 echo "IPv4 Endpoint: Pingable"
 echo \${ipv4endpoint} engage.cloudflareclient.com >> /etc/hosts
 ip -4 route show | grep -q "\${ipv4endpoint}"
 if [ \$? -ne 0 ]; then ip -4 route add \${ipv4endpoint}/32 dev \$(ip -4 route show | grep 'default' | awk '{ print \$5 }') via \$(ip -4 route show | grep 'default' | awk '{ print \$3 }') metric 1; fi
else
 echo "IPv4 Endpoint: Not Pingable"
fi

#check ipv6
ipv6endpoint=\$(dig @1.1.1.1 +tls +short AAAA engage.cloudflareclient.com. | grep -v 'failed' | grep -v 'timed-out' | grep -v 'no servers')
echo "IPv6 Endpoint: \$ipv6endpoint"
ip -6 route del \${ipv6endpoint}/128 &>/dev/null
ipv6endpointstatus=\$(ping -6 -c 2 \${ipv6endpoint} &>/dev/null)
if [ \$? -eq 0 ]; then
 echo "IPv6 Endpoint: Pingable"
 echo \${ipv6endpoint} engage.cloudflareclient.com >> /etc/hosts
 ip -6 route show | grep -q "\${ipv6endpoint}"
 if [ \$? -ne 0 ]; then ip -6 route add \${ipv6endpoint}/128 dev \$(ip -6 route show | grep 'default' | awk '{ print \$5 }') via \$(ip -6 route show | grep 'default' | awk '{ print \$3 }') metric 1; fi
else
 echo "IPv6 Endpoint: Not Pingable"
fi

#run wireguard from config
if [ "\$1" = "up" ]; then
 cftunnelpingable=\$(grep -q 'engage\.cloudflareclient\.com' /etc/hosts)
 if [ \$? -eq 0 ]; then
  /usr/bin/wg-quick \$1 \$2
  echo "successfully turned on \$2 tunnel"
 else
  echo "unable to turn on \$2 tunnel"
 fi
fi
if [ "\$1" = "down" ]; then
 /usr/bin/wg-quick \$1 \$2 1&>/dev/null;
 ip -4 route del \${ipv4endpoint}/32 1&>/dev/null
 ip -6 route del \${ipv6endpoint}/128 1&>/dev/null
 echo "successfully turned off \$2 tunnel"
fi
_EOF_
chmod +x /etc/wireguard/wg-quick

systemctl daemon-reload
systemctl stop wg-quick@cf-tun
systemctl enable --now wg-quick@cf-tun


#privatekey=`cat $profile | grep 'PrivateKey' | cut -d'=' -f2- | sed 's/ //g'`
#echo $privatekey > /etc/wireguard/wgcf-private.key
#publickey=`cat $profile | grep 'PublicKey' | cut -d'=' -f2- | sed 's/ //g'`
#endpoint=`cat $profile | grep 'Endpoint' | cut -d'=' -f2- | sed 's/ //g'`
#addressv4=`cat $profile | grep 'Address' | grep '\.' | cut -d'=' -f2- | sed 's/ //g'`
#addressv6=`cat $profile | grep 'Address' | grep ':' | cut -d'=' -f2- | sed 's/ //g'`

#killall -r wireguard* 2>/dev/null || true
#tunx=wg0
#wg set "${tunx}" private-key /etc/wgcf/wgcf-private.key peer "${publickey}" endpoint "${endpoint}"
#wg setconf "${tunx}" /etc/wgcf/wgcf-profile.conf
#ip link add dev wg0 type wireguard # or wireguard-go "${tunx}"
#ip address add dev wg0 ${addressv4}
#ip address add dev wg0 ${addressv6}
#ip link set up dev "${tunx}" 
#route add 1.0.0.1/32 "${tunx}"
#route add 4.2.2.2/32 "${tun}"

